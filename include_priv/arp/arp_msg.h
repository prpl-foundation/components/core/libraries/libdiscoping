/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__ARP_MSG_H__)
#define __ARP_MSG_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @file This file is responsible for parsing ethernet frames containing an ARP packet.
 */

#include "arp/arp_eth_socket.h"
#include "arp/arp_types.h"

typedef enum {
    ARP_MSG_OPERATION_TYPE_INVALID = 0,
    ARP_MSG_OPERATION_TYPE_REQUEST = 1,
    ARP_MSG_OPERATION_TYPE_REPLY = 2,
    ARP_MSG_OPERATION_TYPE_MAX
} arp_msg_operation_type_e;

struct arp_msg {
    char ip_sender[INET_ADDRSTRLEN];
    char ip_target[INET_ADDRSTRLEN];
    char mac_sender[18];
    char mac_target[18];
    arp_msg_operation_type_e operation_type;
};

typedef struct {
    uint16_t hw_addr_format;           /* format of hardware address */
    uint16_t protocol_addr_format;     /* format of protocol address */
    uint8_t hw_addr_length;            /* length of hardware address */
    uint8_t protocol_addr_length;      /* length of protocol addres */
    uint16_t operation_type;           /* operation type */
} __attribute__((__packed__)) arp_msg_bin_hdr_t;

typedef struct {
    arp_msg_bin_hdr_t arp_header;
    unsigned char sender_hw_addr[6];
    uint32_t sender_ip;
    unsigned char target_hw_addr[6];
    uint32_t target_ip;
} __attribute__((__packed__)) arp_msg_bin_t;

bool PRIVATE arp_msg_filter_and_parse(arp_msg_t* tgt_simple_arp_msg, const arp_eth_socket_frame_t* eth_frame);

bool PRIVATE arp_msg_serialize(arp_msg_bin_t* tgt_arp_bin, const arp_msg_t* arp_msg);

#ifdef __cplusplus
}
#endif

#endif
