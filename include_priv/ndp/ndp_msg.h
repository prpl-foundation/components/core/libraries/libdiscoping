/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__NDP_MSG_H__)
#define __NDP_MSG_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @file This file is responsible for parsing and serializing packets of the Neighbor Discovery Protocol.
 *
 * Actually doing anything with the parsed packets, or actually sending them, is *not* the
 * responsibility of this file.
 */

#include "util.h"
#include <netinet/icmp6.h>

typedef struct {
    struct nd_neighbor_solicit hdr;
    struct nd_opt_hdr opt;
    uint8_t hw_addr[6];
} __attribute__ ((__packed__)) ndp_msg_solicit_packet_t;

typedef struct {
    uint8_t icmp6_type;
    uint8_t icmp6_code;
    uint16_t icmp6_checksum;
    uint16_t icmp6_echo_id;
    uint16_t icmp6_echo_seq;
    uint8_t data[56];
} __attribute__ ((__packed__)) ndp_msg_echo_packet_t;

/**
 * Searches an option in a list of options.
 *
 * Some NDP packets have a list of options at the end. This function searches such an option.
 *
 * @param option_type number/identifier of the option that is searched
 * @param tgt_option_length_in_bytes
 *   output argument, length of option that returnvalue points to.
 *   This is in bytes and not in NDP's unit of 8-octets.
 * @param optionlist
 * @param optionlist_length_in_bytes
 *   length of given `optionlist`.
 *   This is in bytes and not in NDP's unit of 8-octets.
 * @return pointer inside `optionlist` that is the first byte of the option data (so excluding its
 *   headers). NULL if the option is not found or on parse error.
 */
const uint8_t* PRIVATE ndp_msg_get_option(uint32_t option_type, uint32_t* tgt_option_length_in_bytes,
                                          const uint8_t* optionlist, uint32_t optionlist_length_in_bytes);

/**
 * Parse neighbor advertisement message.
 */
bool PRIVATE ndp_msg_advert_parse(char target_ipv6_address_str[INET6_ADDRSTRLEN],
                                  char target_hw_addr_str[18], const uint8_t* frame, ssize_t source_frame_len);

bool ndp_msg_solicit_serialize(ndp_msg_solicit_packet_t* target_ns, const char* ipv6_to, const char* mac_from);

void ndp_msg_make_solicited_node_multicast_address(uint8_t ipv6_bin[16]);

void ndp_msg_echo_serialize(ndp_msg_echo_packet_t* echo_req);

#ifdef __cplusplus
}
#endif

#endif
