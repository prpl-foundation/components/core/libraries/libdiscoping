/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__ARP_DISCOPING_H__)
#define __ARP_DISCOPING_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @file An IP that we're watching for information about up/down/collision
 */

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxo/amxo.h>

#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <arpa/inet.h>

#include "discoping/discoping_types.h"
#include "discoping_watcher.h"
#include <stdbool.h>

typedef enum {
    /** Arping's user thinks the MAC is unreachable, so must be notified if it becomes reachable. */
    ARP_IP_MAC_USERVIEW_UNREACHABLE,
    /** Arping's user thinks the MAC is reachable, so must be notified if it becomes unreachable. */
    ARP_IP_MAC_USERVIEW_REACHABLE,
    /** Arping's user asked to be notified if MAC becomes reachable or unreachable */
    ARP_IP_MAC_USERVIEW_UNKNOWN
} discoping_ip_mac_userview_e;

struct discoping_ip {
    amxc_htable_it_t it;
    /**
     * PRIVATE (do not use outside discoping_ip.c)
     *
     * Elements of type @ref discoping_ip_mac_t.
     * Keys are the MAC address in uppercase.
     */
    amxc_htable_t macs;

    /**
     * PRIVATE (only use inside discoping_ip.c)
     *
     * Whether this IP reachability and collision check will be verified repeatedly automatically.
     */
    bool periodic_reverify_enabled;
};

bool PRIVATE discoping_ip_new(discoping_ip_t** ip);

void PRIVATE discoping_ip_delete(discoping_ip_t** ip);

/**
 * Character representation, like "192.168.1.1", or NULL if none.
 *
 * Note: the key of the hashtable is reused as character representation of the IP,
 * and amxc_htable stores its keys in its bookkeeping, so we will only have
 * a non-NULL character representation after the IP is added to a hashtable.
 */
const char* PRIVATE discoping_ip_char(discoping_ip_t* ip);

void PRIVATE discoping_ip_add_or_set_mac(discoping_ip_t* ip, const char* mac,
                                         discoping_ip_mac_userview_e userview);

void PRIVATE discoping_ip_mark_seen_network(discoping_ip_t* ip, const char* mac_char);

bool PRIVATE discoping_ip_has_mac(discoping_ip_t* ip, const char* mac_char);

/**
 * Whether there was in the network at least one MAC that claimed this IP.
 *
 * This is since the last call to @ref discoping_ip_clear_network_seen.
 */
bool PRIVATE discoping_ip_has_network_seen_macs(discoping_ip_t* ip);

/**
 * Conclude whether the (un)confirmed MACs of given entry are up, down, or colliding.
 *
 * Reports this conclusion to the user via callback.
 *
 * If the IP does not have any reachable MACs anymore, deletes the IP.
 *
 * This does *not* call @ref discoping_ip_clear_network_seen.
 */
void PRIVATE discoping_ip_conclude(discoping_ip_t** ip, discoping_watcher_t* watcher);

/**
 * Clear/unset all confirmations that were received on the network.
 *
 * @return: whether this IP needs to be verified, i.e. there are no known MACs or a known MAC of
 *   the IP was not confirmed.
 */
bool PRIVATE discoping_ip_clear_network_seen(discoping_ip_t* ip);

bool PRIVATE discoping_ip_has_macs(discoping_ip_t* ip);

/**
 * Whether this IP reachability and collision check will be verified repeatedly automatically.
 */
bool PRIVATE discoping_ip_get_periodic_reverify_enabled(discoping_ip_t* ip);

/**
 * Set whether this IP reachability and collision check will be verified repeatedly automatically.
 */
void PRIVATE discoping_ip_set_periodic_reverify_enabled(discoping_ip_t* ip, bool periodic_reverify_enabled);

#ifdef __cplusplus
}
#endif

#endif
