/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DISCOPING_WATCHER_H__)
#define __DISCOPING_WATCHER_H__

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include "arp/arp_types.h"
#include "discoping/discoping_types.h"
#include "discoping.h"

/**
 * @file Periodically check an IP<->MAC mapping (by sending ARP/ICMP/... requests) until it's down.
 */

/**
 * Time in milliseconds to verify IP<->MAC pair if no arp reply seen during this time.
 *
 * Each "timeblock" of this length, the watcher listens to incoming ARP/ICMP/... replies (this reply
 * does not have to correspond to an ARP/ICMP/... request sent by discoping).
 * If no ARP reply was received in a timeblock for an previously observed IP-MAC combination,
 * at the beginning of the next timeblock the MAC-IP mapping is verified by send an ARP/ICMP/...
 * request for this IP.
 *
 * @internal
 * The timeblock length is chosen so that when a device is disconnected, this is verified within
 * 180 seconds with a margin of five seconds.
 *
 * Specifically, the time required to verify that an IP<->MAC pair is no longer
 * available, consists of two watcher timeblocks, a number of verifier retries and
 * the aforementioned margin of five seconds.
 *
 * @endinternal
 */
#define ARP_WATCHER_TIMEBLOCK_MS 79000

/**
 * Time in milliseconds to wait before performing the next scan (search IP addresses).
 *
 * This is only done on interfaces where it is considered appropriate.
 */
#define ARP_WATCHER_SCAN_INTERVAL_MS 120000

/**
 * Time in milliseconds to perform the first scan after startup.
 *
 * This is only done on interfaces where it is considered appropriate.
 */
#define ARP_WATCHER_SCAN_INITIAL_WAIT_MS 30000

bool PRIVATE discoping_watcher_new(discoping_watcher_t** watcher, discoping_t* discoping, discoping_socketlist_t* socketlist);

void PRIVATE discoping_watcher_delete(discoping_watcher_t** watcher);

/**
 * Internal function used in mechanism to ensure an IP always has a watcher.
 *
 * You probably want to use @ref discoping_watcher_mark_seen_network instead.
 */
bool PRIVATE discoping_watcher_internal_add_ip(discoping_watcher_t* watcher, discoping_ip_t* ip, const char* ip_char);

/**
 * The IP-MAC became unreachable and the user must be informed about this.
 *
 * @param mac: can be NULL in case the IP was being watched without any associated MAC.
 */
void PRIVATE discoping_watcher_on_down(discoping_watcher_t* watcher, discoping_ip_t* ip, const char* mac);

/**
 * The IP became reachable and the user must be informed about this.
 */
void PRIVATE discoping_watcher_on_up(discoping_watcher_t* watcher, discoping_ip_t* ip, const char* mac);

void PRIVATE discoping_watcher_on_collision(discoping_watcher_t* watcher,
                                            discoping_ip_t* ip, const amxc_var_t* colliding_macs);

/**
 * Call this when network confirmed the IP is reachable on this mac.
 *
 * This is independent of whether it was reachable before.
 */
void PRIVATE discoping_watcher_mark_seen_network(discoping_watcher_t* watcher, const char* ip,
                                                 const char* mac);

/**
 * Call this when user knows somehow the IP is reachable on this mac.
 */
void PRIVATE discoping_watcher_mark_seen_user(discoping_watcher_t* watcher, const char* ip,
                                              const char* mac);

discoping_verifier_t* PRIVATE discoping_watcher_verifier(discoping_watcher_t* watcher);

void PRIVATE discoping_watcher_verify(discoping_watcher_t* watcher, const char* ip_char, const char* mac, bool periodic_reverify_enabled);

bool PRIVATE discoping_watcher_is_watching(discoping_watcher_t* watcher, const char* ip_char, const char* mac);

void PRIVATE discoping_watcher_set_periodic_reverify_enabled(discoping_watcher_t* watcher,
                                                             const char* ip_char,
                                                             bool enabled
                                                             );

#ifdef __cplusplus
}
#endif

#endif
