/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DISCOPING_VERIFIER_H__)
#define __DISCOPING_VERIFIER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "arp/arp_types.h"
#include "discoping/discoping_types.h"
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <stdbool.h>

/**
 * @file Immediately performs the check if an IP<->MAC mapping is down by sending ARP and checking timeout.
 */

/**
 * Retrytime between sending ARP request and sending again.
 *
 * Time in milliseconds after not receiving an ARP reply (to an ARP requests we sent) that we try
 * again by sending another ARP request.
 */
#define ARP_VERIFIER_RETRY_INTERVAL_MS 4000

/** Number of times we send an ARP until we conclude it's down. Includes the first attempt. */
#define ARP_VERIFIER_NB_TRIES 4

/**
 * Time after we assume no collision will be detected anymore.
 *
 * Time in milliseconds after sending an ARP request that we conclude whether there is collision
 * or not (collision is the situation of multiple MACs claiming the same IP).
 * After this time, if only 1 reply was received for this ip, we assume there is no collision and the
 * user's on_up callback can be called. If after this time, 2 or more replies were received (with
 * different MACs), we already report at this point which MACs are colliding (by the on_collision
 * callback).
 */
#define ARP_VERIFIER_COLLISION_TIMEOUT_MS 1500

bool PRIVATE discoping_verifier_new(discoping_verifier_t** verifier, discoping_watcher_t* watcher, discoping_socketlist_t* socketlist);

void PRIVATE discoping_verifier_delete(discoping_verifier_t** verifier);

void PRIVATE discoping_verifier_verify(discoping_verifier_t* verifier, discoping_ip_t* ip);

void PRIVATE discoping_verifier_mark_seen_network(discoping_verifier_t* verifier,
                                                  discoping_ip_t* ip, const char* mac, bool need_collision_check);

#ifdef __cplusplus
}
#endif

#endif
