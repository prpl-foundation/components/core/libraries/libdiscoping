/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "priv.h"
#include "util.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <string.h>
#include <amxc/amxc_macros.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <arpa/inet.h>

bool util_mac_string_to_bytes(unsigned char* tgt_mac_bin, const char* src_mac_str) {
    int result = -1;
    unsigned int int_mac[6] = {0};
    when_null_trace(tgt_mac_bin, error, ERROR, "NULL");
    when_null_trace(src_mac_str, error, ERROR, "NULL");
    when_false(17 == strlen(src_mac_str), error);
    result = sscanf(src_mac_str, "%2x:%2x:%2x:%2x:%2x:%2x",
                    &int_mac[0], &int_mac[1], &int_mac[2], &int_mac[3], &int_mac[4], &int_mac[5]);
    when_false(result == 6, error);
    for(int i = 0; i < 6; i++) {
        tgt_mac_bin[i] = (unsigned char) int_mac[i];
    }

    return true;
error:
    return false;
}

void util_mac_bytes_to_string(char* tgt_mac_str, const unsigned char* src_mac_bin) {
    int result = -1;
    when_null_trace(tgt_mac_str, exit, ERROR, "NULL");
    when_null_trace(src_mac_bin, exit, ERROR, "NULL");
    result = snprintf(tgt_mac_str, 18, "%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X",
                      src_mac_bin[0], src_mac_bin[1], src_mac_bin[2],
                      src_mac_bin[3], src_mac_bin[4], src_mac_bin[5]);
    when_false_trace(result == 17, exit, ERROR, "BUG");

exit:
    return;
}

bool util_cstring_to_upper(char* tgt_str, size_t tgt_str_size, const char* src_str) {
    size_t i = 0;
    when_null_trace(tgt_str, error, ERROR, "NULL argument");
    when_null_trace(src_str, error, ERROR, "NULL argument");

    for(i = 0; src_str[i] != '\0' && i < tgt_str_size - 1; i++) {
        tgt_str[i] = toupper(src_str[i]);
    }
    if(src_str[i] != '\0') {
        tgt_str[i] = '\0';
        return false;
    }
    return true;
error:
    return false;
}

util_ip_family_t util_ip_family(const char* ipaddress) {
    const char* pos = NULL;
    when_null_trace(ipaddress, error, ERROR, "NULL");
    pos = strchr(ipaddress, ':');

    if(pos != NULL) {
        return AF_INET6;
    } else {
        return AF_INET;
    }

error:
    return AF_INET;
}