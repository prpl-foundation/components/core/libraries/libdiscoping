/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "ndp/ndp_msg.h"
#include "priv.h"
#include "util.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <arpa/inet.h>

const uint8_t* ndp_msg_get_option(uint32_t option_type, uint32_t* tgt_option_length_in_bytes,
                                  const uint8_t* optionlist, uint32_t optionlist_length_in_bytes) {

    const uint8_t* ptr = optionlist;
    uint32_t bytes_left = optionlist_length_in_bytes;
    when_null_trace(tgt_option_length_in_bytes, error, ERROR, "NULL argument");
    when_null_trace(optionlist, error, ERROR, "NULL argument");

    while(bytes_left >= 8) {
        uint8_t option_with_headers_length_in_8octets = ptr[1];
        uint32_t option_with_headers_length_bytes = ((uint16_t) option_with_headers_length_in_8octets) << 3;

        when_false_trace(option_with_headers_length_bytes != 0, error, INFO, "Invalid length");

        when_false_trace(bytes_left >= option_with_headers_length_bytes, error,
                         INFO, "Option larger than remaining data");
        bytes_left -= option_with_headers_length_bytes;

        /* skips unrecognized option */
        if((uint32_t) ptr[0] != option_type) {
            ptr += option_with_headers_length_bytes;
            continue;
        }

        /* Found! return option pointer in `optionlist` and set the correct length */
        ptr += 2;
        *tgt_option_length_in_bytes = option_with_headers_length_bytes - 2;

        return ptr;
    }

error:
    if(tgt_option_length_in_bytes != NULL) {
        *tgt_option_length_in_bytes = 0;
    }
    return NULL;
}


bool ndp_msg_advert_parse(char target_ipv6_address_str[INET6_ADDRSTRLEN],
                          char target_hw_addr_str[18], const uint8_t* source_frame, ssize_t source_frame_len) {

    struct nd_neighbor_advert* advert = (struct nd_neighbor_advert*) source_frame;
    uint32_t optionlength_bytes = 0;
    const uint8_t* option = NULL;
    when_false(source_frame_len >= (ssize_t) sizeof(struct nd_neighbor_advert), error);

    when_false_trace(advert->nd_na_hdr.icmp6_type == ND_NEIGHBOR_ADVERT, error, INFO,
                     "Unhandled icmpv6 message (should not be recieved, filter should be set)");

    inet_ntop(AF_INET6, &advert->nd_na_target, target_ipv6_address_str, INET6_ADDRSTRLEN);
    option = ndp_msg_get_option(ND_OPT_TARGET_LINKADDR, &optionlength_bytes,
                                source_frame + sizeof(struct nd_neighbor_advert), source_frame_len - sizeof(struct nd_neighbor_advert));

    // Note: Since 2007-ish the option is not mandatory anymore in some cases (RFC4861).
    //       So don't complain about it in logs because it's now normal to see such packets.
    //       It's still still mandatory for replies to multicast sollicitation (so mandatory for
    //       replies to what we send), so it's still OK to rely on this option in our case.
    when_null(option, error);

    // Note: option length is a multiple of 8 (RFC4861), so no "== 6".
    when_false_trace(optionlength_bytes >= 6, error, INFO, "Option length too short");

    snprintf(target_hw_addr_str, 18, "%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X", option[0], option[1], option[2],
             option[3], option[4], option[5]);

    return true;

error:
    return false;
}

void ndp_msg_make_solicited_node_multicast_address(uint8_t ipv6_bin[16]) {
    // See also: https://en.wikipedia.org/wiki/Solicited-node_multicast_address
    uint8_t prefix[] = {0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff};
    when_null_trace(ipv6_bin, exit, ERROR, "NULL argument");

    memcpy(ipv6_bin, prefix, sizeof(prefix));

exit:
    return;
}

bool ndp_msg_solicit_serialize(ndp_msg_solicit_packet_t* ns, const char* ipv6_to, const char* mac_from) {
    bool ok = false;
    int ret = -1;
    when_null_trace(ns, error, ERROR, "NULL argument");
    when_false_trace(util_ip_family(ipv6_to) == AF_INET6, error, INFO, "NDP is for ipv6 only");

    memset(ns, 0, sizeof(ndp_msg_solicit_packet_t));
    ns->hdr.nd_ns_type = ND_NEIGHBOR_SOLICIT;
    ns->hdr.nd_ns_code = 0;
    ns->hdr.nd_ns_cksum = 0; /* computed by the kernel */
    ns->hdr.nd_ns_reserved = 0;
    ns->opt.nd_opt_type = ND_OPT_SOURCE_LINKADDR;
    ns->opt.nd_opt_len = 1; /* 8 bytes */

    ret = inet_pton(AF_INET6, ipv6_to, &ns->hdr.nd_ns_target);
    when_false_trace(ret == 1, error, ERROR, "Invalid IPv6 IP %s", ipv6_to);
    ok = util_mac_string_to_bytes(ns->hw_addr, mac_from);
    when_false_trace(ok, error, ERROR, "Invalid MAC %s", mac_from);

    ok = true;
error:
    return ok;
}

void ndp_msg_echo_serialize(ndp_msg_echo_packet_t* echo_req) {
    when_null_trace(echo_req, error, ERROR, "NULL argument");

    *echo_req = (ndp_msg_echo_packet_t) {
        .icmp6_type = 128,   // 128 = echo request
        .icmp6_code = 0,
        .icmp6_checksum = 0, // computed by the kernel
        .icmp6_echo_id = 0x337,
        .icmp6_echo_seq = 0,
    };

error:
    return;
}


