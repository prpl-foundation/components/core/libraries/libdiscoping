/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "discoping.h"
#include "ndp/ndp_socket_priv.h"
#include "ndp/ndp_msg.h"
#include "priv.h"
#include "util.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <net/if.h>
#include <string.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>
#include <netinet/icmp6.h>

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>

#include <netdb.h>

// Seems to be missing on some platforms.
#ifndef IPV6_ADDR_PREFERENCES
/* RFC5014.  */
#define IPV6_ADDR_PREFERENCES   72
#endif

#define IPV6_PREFER_SRC_PUBLIC        0x0002

struct ndp_socket {
    amxo_parser_t* odl_parser;
    int socket_fd;
    char* netdev_name;
    socket_common_reply_cb_t ext_cb;
    void* userdata;
};

/**
 * @implements amxo_fd_cb_t
 */
static void s_recvmsg(int sockfd, void* priv) {
    ndp_socket_t* socket = (ndp_socket_t*) priv;
    ssize_t rdlen = 0;
    uint8_t frame[1460] = {};
    char ipv6_address[INET6_ADDRSTRLEN] = {};
    char hw_addr[20] = {};
    bool ok = false;
    when_null_trace(socket, exit, ERROR, "NULL");

    // Read frame:
    rdlen = recv(sockfd, &frame, sizeof(frame), MSG_DONTWAIT);
    when_false(rdlen != -1, exit);

    ok = ndp_msg_advert_parse(ipv6_address, hw_addr, frame, rdlen);
    when_false_trace(ok, exit, INFO, "Error parsing frame");
    SAH_TRACEZ_INFO(ME, "MAC %s claims IP %s ", hw_addr, ipv6_address);

    socket->ext_cb(ipv6_address, hw_addr, socket->userdata);

exit:
    return;
}

static bool s_getIPv6ByIntfName(const char* host, const char* intfname, struct sockaddr_in6* tgt_addr) {
    bool retval = false;
    int val = -1;
    struct addrinfo* res = NULL;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_family = PF_INET6;
    hints.ai_socktype = SOCK_DGRAM; /* dummy */
    hints.ai_flags = AI_NUMERICHOST;

    val = getaddrinfo(host, NULL, &hints, &res);
    when_false_trace(val == 0, exit, ERROR, "Failed to fetch addrinfo for host %s %d %d: %s", host, val, errno, strerror(errno));

    memcpy(tgt_addr, res->ai_addr, sizeof(struct sockaddr_in6));
    freeaddrinfo(res);

    if(intfname != NULL) {
        val = if_nametoindex(intfname);
        when_false_trace(val != 0, exit, ERROR, "Failed to fetch index of interface %s", intfname);
        tgt_addr->sin6_scope_id = val;
    } else {
        tgt_addr->sin6_scope_id = 0;
    }
    retval = true;

exit:
    return retval;
}

static bool s_open_socket(ndp_socket_t* self, const char* ip) {
    struct ifreq ifr;
    int ret = -1;
    int flags = 0;
    uint32_t value = 255;
    struct ipv6_mreq mreq;
    struct icmp6_filter filter;
    struct sockaddr_in6 src;
    memset(&ifr, 0, sizeof(ifr));
    memset(&mreq, 0, sizeof(mreq));
    memset(&filter, 0, sizeof(filter));
    memset(&src, 0, sizeof(src));
    when_null_trace(self, error, ERROR, "NULL argument");
    when_null_trace(self->odl_parser, error, ERROR, "Invalid state");
    when_false_trace(self->socket_fd == -1, error, ERROR, "Invalid state");
    SAH_TRACEZ_INFO(ME, "Opening peer for %s", self->netdev_name);

    // Create the socket:
    self->socket_fd = socket(PF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
    when_false_trace(self->socket_fd != -1, error, ERROR, "Failed to create socket: %d %s", errno, strerror(errno));

    // Bind to interface:
    strncpy(ifr.ifr_name, self->netdev_name, sizeof(ifr.ifr_name) - 1);
    ret = setsockopt(self->socket_fd, SOL_SOCKET, SO_BINDTODEVICE, &ifr, sizeof(ifr));
    when_false_trace(ret != -1, error, ERROR, "Failed to bind (%d: %s) to interface %s", errno, strerror(errno), self->netdev_name);

    // Bind to IP:
    s_getIPv6ByIntfName(ip, NULL, &src);
    if(bind(self->socket_fd, (struct sockaddr*) &src, sizeof(struct sockaddr_in6)) == -1) {
        SAH_TRACEZ_ERROR(ME, "%s: Failed to bind (%d: %s) to IP %s", self->netdev_name, errno, strerror(errno), ip);
    }

    // Fetch index of netdev_name:
    mreq.ipv6mr_interface = if_nametoindex(self->netdev_name);

    // Join multicast:
    inet_pton(AF_INET6, "FF02::1", &mreq.ipv6mr_multiaddr);
    if(setsockopt(self->socket_fd, IPPROTO_IPV6, IPV6_JOIN_GROUP, &mreq, sizeof(mreq)) == -1) {
        SAH_TRACEZ_ERROR(ME, "setsockopt failed with error %d: %s", errno, strerror(errno));
    }

    // Set filter:
    ICMP6_FILTER_SETBLOCKALL(&filter);
    ICMP6_FILTER_SETPASS(ND_NEIGHBOR_ADVERT, &filter);
    if(setsockopt(self->socket_fd, IPPROTO_ICMPV6, ICMP6_FILTER, &filter, sizeof(filter)) == -1) {
        SAH_TRACEZ_ERROR(ME, "setsockopt failed with error %d: %s", errno, strerror(errno));
    }

    // Set max hops:
    if(setsockopt(self->socket_fd, SOL_SOCKET, SO_DONTROUTE, &(int) { 1 }, sizeof(int)) == -1) {
        SAH_TRACEZ_ERROR(ME, "setsockopt failed with error %d: %s", errno, strerror(errno));
    }
    if(setsockopt(self->socket_fd, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &value, sizeof(value)) == -1) {
        SAH_TRACEZ_ERROR(ME, "setsockopt failed with error %d: %s", errno, strerror(errno));
    }
    if(setsockopt(self->socket_fd, IPPROTO_IPV6, IPV6_UNICAST_HOPS, &value, sizeof(value)) == -1) {
        SAH_TRACEZ_ERROR(ME, "setsockopt failed with error %d: %s", errno, strerror(errno));
    }

    // Hardcode preference to public ipv6:
    value = IPV6_PREFER_SRC_PUBLIC;
    if(setsockopt(self->socket_fd, IPPROTO_ICMPV6, IPV6_ADDR_PREFERENCES, &value, sizeof(value)) == -1) {
        SAH_TRACEZ_NOTICE(ME, "setsockopt failed with error %d: %s", errno, strerror(errno));
    }

    // Set non-blocking:
    flags = fcntl(self->socket_fd, F_GETFL, 0);
    when_false_trace(flags >= 0, error, ERROR, "socket fnctl F_GETFL failed: %d %s", errno, strerror(errno));
    ret = fcntl(self->socket_fd, F_SETFL, flags | O_NONBLOCK);
    when_false_trace(ret >= 0, error, ERROR, "socket fnctl F_SETFL failed %d %s", errno, strerror(errno));

    // Add to event loop:
    ret = amxo_connection_add(self->odl_parser, self->socket_fd, s_recvmsg, NULL, AMXO_CUSTOM, self);
    when_failed_trace(ret, error, ERROR, "amxo_connection_add() failed %d", ret);

    return true;

error:
    return false;

}

bool ndp_socket_new(ndp_socket_t** self,
                    amxo_parser_t* odl_parser, const char* netdev_name,
                    const char* ip,
                    socket_common_reply_cb_t cb) {

    bool ok = false;
    when_null_trace(self, error, ERROR, "NULL/Empty argument socket");
    when_null_trace(cb, error, ERROR, "NULL argument");
    when_null_trace(odl_parser, error, ERROR, "NULL");
    when_str_empty_trace(netdev_name, error, ERROR, "NULL/Empty argument");
    when_str_empty_trace(ip, error, ERROR, "NULL/Empty argument");
    when_false_trace(util_ip_family(ip) == AF_INET6, error, INFO, "NDP is for ipv6 only");

    *self = (ndp_socket_t*) calloc(1, sizeof(ndp_socket_t));
    when_null_trace(*self, error, ERROR, "Out of mem");

    (*self)->socket_fd = -1;
    (*self)->ext_cb = cb;
    (*self)->odl_parser = odl_parser;
    (*self)->netdev_name = strdup(netdev_name);
    when_null_trace((*self)->netdev_name, error, ERROR, "Out of mem");

    ok = s_open_socket(*self, ip);
    when_false_trace(ok, error, ERROR, "Error opening socket");

    return true;

error:
    ndp_socket_delete(self);
    return false;
}

void ndp_socket_delete(ndp_socket_t** socket) {
    if((socket == NULL) || (*socket == NULL)) {
        return;
    }

    if((*socket)->socket_fd != -1) {
        amxo_connection_remove((*socket)->odl_parser, (*socket)->socket_fd);
        close((*socket)->socket_fd);
    }

    free((*socket)->netdev_name);

    free(*socket);
    *socket = NULL;
}

bool ndp_socket_send_request(ndp_socket_t* self, UNUSED const char* ip_from,
                             const char* ip_to, const char* mac_from) {
    ndp_msg_solicit_packet_t packet;
    struct sockaddr_in6 dst;
    bool ok = false;
    ssize_t sendto_size = -1;
    memset(&packet, 0, sizeof(packet));
    memset(&dst, 0, sizeof(dst));
    when_null_trace(self, exit, ERROR, "NULL argument");

    ok = ndp_msg_solicit_serialize(&packet, ip_to, mac_from);
    when_false(ok, exit); // Can legitimately happen (e.g. ip_to is ipv4)

    s_getIPv6ByIntfName(ip_to, self->netdev_name, &dst);
    ndp_msg_make_solicited_node_multicast_address(dst.sin6_addr.s6_addr);
    sendto_size = sendto(self->socket_fd, &packet, sizeof(packet), 0, (struct sockaddr*) &dst, sizeof(dst));
    // Failure to send is normal if we're sending on the wrong interface, which happens a lot
    // because we send everything on all interfaces. So do not logspam these failures.
    when_false_trace(sendto_size != -1, exit, INFO, "Failed to send %s %s %s -> %s: %d %s", self->netdev_name, ip_from, mac_from, ip_to, errno, strerror(errno));
    when_false_trace(sendto_size == sizeof(packet), exit, ERROR, "Send succeeded but was incomplete!");

    ok = true;

exit:
    return ok;
}

void ndp_socket_set_userdata(ndp_socket_t** self, void* userdata) {
    when_null_trace(*self, exit, ERROR, "NULL");
    (*self)->userdata = userdata;
exit:
    return;
}

void ndp_socket_scan(ndp_socket_t* self) {
    ssize_t sendto_size = -1;
    ndp_msg_echo_packet_t packet;
    struct sockaddr_in6 dst;
    memset(&packet, 0, sizeof(packet));
    memset(&dst, 0, sizeof(dst));

    when_null_trace(self, exit, ERROR, "NULL");
    ndp_msg_echo_serialize(&packet);

    s_getIPv6ByIntfName("ff02::1", self->netdev_name, &dst);
    sendto_size = sendto(self->socket_fd, &packet, sizeof(packet), 0, (struct sockaddr*) &dst, sizeof(dst));
    when_false_trace(sendto_size != -1, exit, INFO, "Failed to send %s: %d %s", self->netdev_name, errno, strerror(errno));
    when_false_trace(sendto_size == sizeof(packet), exit, ERROR, "Send succeeded but was incomplete!");

exit:
    return;
}
