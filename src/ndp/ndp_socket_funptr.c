/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
 * This file is basically just @ref ndp_socket_impl
 *
 * It is a separate file from ndp_socket.c to allow cmocka to mock the
 * functions @ref ndp_socket_scan etc. Without a separate file, the caller
 * and callee are in the same file, which causes these functioncalls to not be "processed"
 * by the linker, causing the linker to not be able to "re-route" the real function
 * to the mocked function.
 */

#include "ndp/ndp_socket_priv.h"
#include "discoping/discoping_socketlist.h"

/** @implements discoping_socket_t.new_socket() */
static void s_new_socket(void** self, amxo_parser_t* odl_parser, const char* netdev_name,
                         const char* ip_from, const char* mac_from UNUSED, socket_common_reply_cb_t cb) {

    ndp_socket_new((ndp_socket_t**) self, odl_parser, netdev_name, ip_from, cb);
}

/** @implements discoping_socket_t.set_userdata() */
static void s_set_userdata(void** self, void* userdata) {
    ndp_socket_set_userdata((ndp_socket_t**) self, userdata);
}

/** @implements discoping_socket_t.delete_socket() */
static void s_delete_socket(void** self) {
    ndp_socket_delete((ndp_socket_t**) self);
}

/** @implements discoping_socket_t.send_request() */
static bool s_send_request(void* self_untyped, const char* ip_from, const char* ip_to, const char* mac_from) {
    return ndp_socket_send_request((ndp_socket_t*) self_untyped, ip_from, ip_to, mac_from);
}


/** @implements discoping_socket_t.scan() */
static void s_scan(void* self_untyped) {
    ndp_socket_scan((ndp_socket_t*) self_untyped);
}

discoping_socket_t ndp_socket_impl = {
    .new_socket = s_new_socket,
    .set_userdata = s_set_userdata,
    .delete_socket = s_delete_socket,
    .send_request = s_send_request,
    .scan = s_scan,
};