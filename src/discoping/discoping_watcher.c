/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "discoping/discoping_watcher.h"
#include "discoping/discoping_verifier.h"
#include "discoping/discoping_socketlist.h"
#include "discoping/discoping_priv.h"
#include "priv.h"
#include "discoping/discoping_ip.h"
#include "util.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <net/if.h>
#include <string.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxo/amxo.h>

#define ARP_VERIFIER_TIME_MARGIN_MS 5000

#if (ARP_WATCHER_TIMEBLOCK_MS < ARP_VERIFIER_RETRY_INTERVAL_MS * ARP_VERIFIER_NB_TRIES + ARP_VERIFIER_TIME_MARGIN_MS)
#error "The ARP watcher timeblock length must encompass at least the time required to send the verification ARP requests and some extra margin time.");
#endif

struct discoping_watcher {
    /** element of type @ref discoping_ip_t */
    amxc_htable_t ips;
    discoping_t* discoping;
    discoping_verifier_t* verifier;
    discoping_socketlist_t* socketlist;
    amxp_timer_t* periodic_reverification_timer;
    amxp_timer_t* scan_timer;
};

static discoping_ip_t* s_lookup_ip(discoping_watcher_t* watcher, const char* ip_char) {
    amxc_htable_it_t* it = NULL;
    when_null_trace(watcher, error, ERROR, "NULL argument watcher");
    when_null_trace(ip_char, error, ERROR, "NULL argument ip_char");
    it = amxc_htable_get(&watcher->ips, ip_char);
    when_null(it, error);
    return amxc_htable_it_get_data(it, discoping_ip_t, it);

error:
    return NULL;
}

static discoping_ip_t* s_add_or_lookup_ip(discoping_watcher_t* watcher, const char* ip_char) {
    discoping_ip_t* ip = NULL;
    when_null_trace(watcher, error, ERROR, "NULL argument watcher");

    ip = s_lookup_ip(watcher, ip_char);
    if(ip == NULL) {
        bool ok = false;
        discoping_ip_new(&ip);
        when_null_trace(ip, error, ERROR, "Error creating IP");
        ok = discoping_watcher_internal_add_ip(watcher, ip, ip_char);
        when_false_trace(ok, error, ERROR, "Error adding IP");
    }

    return ip;

error:
    discoping_ip_delete(&ip);
    return NULL;
}

bool discoping_watcher_internal_add_ip(discoping_watcher_t* watcher, discoping_ip_t* ip, const char* ip_char) {
    when_null_trace(watcher, error, ERROR, "NULL argument watcher");
    when_null_trace(ip, error, ERROR, "NULL argument ip");
    when_null_trace(ip_char, error, ERROR, "NULL argument ip_char");
    when_false_trace(NULL == discoping_ip_char(ip), error, ERROR, "IP already added to a watcher");
    when_false_trace(s_lookup_ip(watcher, ip_char) == NULL, error, ERROR, "IP already tracked, cannot add");

    amxc_htable_insert(&watcher->ips, ip_char, &ip->it);

    return true;
error:
    return false;
}

/**
 * Verify if IP is reachable by sending ARP requests, if it was not confirmed during time between
 * last call and current call to this function.
 */
static void s_clear_confirmations_or_verify(discoping_watcher_t* watcher) {
    when_null_trace(watcher, error, ERROR, "NULL argument watcher");

    amxc_htable_for_each(it, &watcher->ips) {
        discoping_ip_t* ip = amxc_htable_it_get_data(it, discoping_ip_t, it);
        bool need_verifying = discoping_ip_clear_network_seen(ip);
        bool allowed_verifying = discoping_ip_get_periodic_reverify_enabled(ip);

        if(need_verifying && allowed_verifying) {
            discoping_verifier_verify(watcher->verifier, ip);
        }
    }

error:
    return;
}

/** @implements amxp_timer_cb_t */
static void s_periodic_reverification_timer_cb(amxp_timer_t* timer, void* priv) {
    discoping_watcher_t* watcher = (discoping_watcher_t*) priv;
    when_null_trace(watcher, error, ERROR, "NULL argument");
    when_false_trace(timer == watcher->periodic_reverification_timer, error, ERROR, "Wrong timer");

    s_clear_confirmations_or_verify(watcher);

error:
    return;
}

static void s_scan(discoping_watcher_t* watcher) {
    when_null_trace(watcher, exit, ERROR, "NULL argument");
    discoping_socketlist_scan(watcher->socketlist);

exit:
    return;
}

/** @implements amxp_timer_cb_t */
static void s_scan_timer_cb(amxp_timer_t* timer, void* priv) {
    discoping_watcher_t* watcher = (discoping_watcher_t*) priv;
    when_null_trace(watcher, error, ERROR, "NULL argument");
    when_false_trace(timer == watcher->scan_timer, error, ERROR, "Wrong timer");

    s_scan(watcher);

error:
    return;
}

bool discoping_watcher_new(discoping_watcher_t** watcher, discoping_t* discoping, discoping_socketlist_t* socketlist) {
    int status = -1;
    when_null_trace(watcher, error, ERROR, "NULL argument watcher");
    when_null_trace(discoping, error, ERROR, "NULL argument discoping");

    *watcher = (discoping_watcher_t*) calloc(1, sizeof(discoping_watcher_t));
    when_null_trace(*watcher, error, ERROR, "Out of mem");

    (*watcher)->discoping = discoping;

    status = amxc_htable_init(&(*watcher)->ips, 4);
    when_failed_trace(status, error, ERROR, "Error initializing htable");

    discoping_verifier_new(&(*watcher)->verifier, *watcher, socketlist);
    when_null_trace((*watcher)->verifier, error, ERROR, "Cannot create verifier");

    amxp_timer_new(&(*watcher)->periodic_reverification_timer, s_periodic_reverification_timer_cb, *watcher);
    when_null_trace((*watcher)->periodic_reverification_timer, error, ERROR, "Error creating timer");
    amxp_timer_set_interval((*watcher)->periodic_reverification_timer, ARP_WATCHER_TIMEBLOCK_MS);
    amxp_timer_start((*watcher)->periodic_reverification_timer, ARP_WATCHER_TIMEBLOCK_MS);

    amxp_timer_new(&(*watcher)->scan_timer, s_scan_timer_cb, *watcher);
    when_null_trace((*watcher)->scan_timer, error, ERROR, "Error creating timer");
    amxp_timer_set_interval((*watcher)->scan_timer, ARP_WATCHER_SCAN_INTERVAL_MS);
    amxp_timer_start((*watcher)->scan_timer, ARP_WATCHER_SCAN_INITIAL_WAIT_MS);

    (*watcher)->socketlist = socketlist;
    discoping_socketlist_set_watcher(socketlist, *watcher);

    return true;

error:
    discoping_watcher_delete(watcher);
    return false;
}

/** @implements amxc_htable_it_delete_t */
static void s_delete_ip(const char* key UNUSED, amxc_htable_it_t* it) {
    discoping_ip_t* ip = NULL;
    when_null_trace(it, exit, ERROR, "NULL argument it");

    ip = amxc_htable_it_get_data(it, discoping_ip_t, it);
    discoping_ip_delete(&ip);

exit:
    return;
}

void discoping_watcher_delete(discoping_watcher_t** watcher) {
    discoping_verifier_t* verifier = NULL;
    if((watcher == NULL) || (*watcher == NULL)) {
        return;
    }

    amxp_timer_delete(&(*watcher)->periodic_reverification_timer);
    amxp_timer_delete(&(*watcher)->scan_timer);

    verifier = (*watcher)->verifier;
    (*watcher)->verifier = NULL;
    discoping_verifier_delete(&verifier);

    discoping_socketlist_set_watcher((*watcher)->socketlist, NULL);

    amxc_htable_clean(&(*watcher)->ips, s_delete_ip);

    free(*watcher);
    *watcher = NULL;
}

discoping_verifier_t* discoping_watcher_verifier(discoping_watcher_t* watcher) {
    when_null_trace(watcher, error, ERROR, "NULL argument watcher");

    return watcher->verifier;

error:
    return NULL;
}

static void s_on_updown(discoping_watcher_t* watcher, discoping_ip_t* ip, const char* mac, bool is_up) {
    char* ip_char = NULL;
    char* mac_char = NULL;
    when_null_trace(watcher, exit, ERROR, "NULL argument watcher");
    when_null_trace(ip, exit, ERROR, "NULL argument ip");
    when_null_trace(discoping_ip_char(ip), exit, ERROR, "IP of no watcher");
    when_false_trace(s_lookup_ip(watcher, discoping_ip_char(ip)) == ip, exit, ERROR, "IP of another watcher");

    ip_char = strdup(discoping_ip_char(ip));
    // do not trust our caller does not give mac owned by ip object (which callback can delete)
    mac_char = mac != NULL ? strdup(mac) : NULL;

    if(is_up) {
        ip = NULL; // callback can trigger deletion of `ip`, so consider `ip` to be gone.
        discoping_internal_on_up(watcher->discoping, ip_char, mac_char);
    } else {
        discoping_internal_on_down(watcher->discoping, ip_char, mac_char);
    }

exit:
    free(ip_char);
    free(mac_char);
    return;
}

void discoping_watcher_on_down(discoping_watcher_t* watcher, discoping_ip_t* ip, const char* mac) {
    s_on_updown(watcher, ip, mac, false);
}

void discoping_watcher_on_up(discoping_watcher_t* watcher, discoping_ip_t* ip, const char* mac) {
    s_on_updown(watcher, ip, mac, true);
}

void discoping_watcher_on_collision(discoping_watcher_t* watcher, discoping_ip_t* ip,
                                    const amxc_var_t* colliding_macs) {

    char* ip_char = NULL;
    when_null_trace(watcher, exit, ERROR, "NULL argument watcher");
    when_null_trace(ip, exit, ERROR, "NULL argument ip");
    when_null_trace(discoping_ip_char(ip), exit, ERROR, "IP of no watcher");
    SAH_TRACEZ_INFO(ME, "Collision %s", discoping_ip_char(ip));
    when_false_trace(s_lookup_ip(watcher, discoping_ip_char(ip)) == ip, exit, ERROR, "IP of another watcher");

    ip_char = strdup(discoping_ip_char(ip));

    discoping_internal_on_collision(watcher->discoping, ip_char, colliding_macs);
exit:
    free(ip_char);
    return;
}

void discoping_watcher_mark_seen_network(discoping_watcher_t* watcher, const char* ip_char, const char* mac) {
    discoping_ip_t* ip = NULL;
    bool need_collision_check = false;
    when_null_trace(watcher, exit, ERROR, "NULL argument watcher");
    when_null_trace(ip_char, exit, ERROR, "NULL argument ip");
    when_null_trace(mac, exit, ERROR, "NULL argument mac");

    ip = s_lookup_ip(watcher, ip_char);
    if(ip == NULL) {
        need_collision_check = true;
        ip = s_add_or_lookup_ip(watcher, ip_char);
    }
    if(!discoping_ip_has_mac(ip, mac)) {
        need_collision_check = true;
    }
    discoping_ip_mark_seen_network(ip, mac);

    discoping_verifier_mark_seen_network(watcher->verifier, ip, mac, need_collision_check);

exit:
    return;
}

void discoping_watcher_mark_seen_user(discoping_watcher_t* watcher, const char* ip_char,
                                      const char* mac) {

    discoping_ip_t* ip = NULL;
    when_null_trace(watcher, exit, ERROR, "NULL argument watcher");
    when_null_trace(ip_char, exit, ERROR, "NULL argument ip");
    when_null_trace(mac, exit, ERROR, "NULL argument mac");

    ip = s_add_or_lookup_ip(watcher, ip_char);
    discoping_ip_add_or_set_mac(ip, mac, ARP_IP_MAC_USERVIEW_REACHABLE);

exit:
    return;
}

void discoping_watcher_verify(discoping_watcher_t* watcher, const char* ip_char, const char* mac_char,
                              bool periodic_reverify_enabled) {

    discoping_ip_t* ip = NULL;
    when_null_trace(watcher, error, ERROR, "NULL argument watcher");
    when_null_trace(ip_char, error, ERROR, "NULL argument ip_char");

    ip = s_add_or_lookup_ip(watcher, ip_char);
    discoping_ip_set_periodic_reverify_enabled(ip, periodic_reverify_enabled);

    if((mac_char != NULL) && !discoping_ip_has_mac(ip, mac_char)) {
        // Add the mac to the macs that the user thinks the IP has, so that in case there is no reply
        // for this mac, we know we have to call the on_down callback for this mac.
        discoping_ip_add_or_set_mac(ip, mac_char, ARP_IP_MAC_USERVIEW_UNKNOWN);
    }

    discoping_verifier_verify(watcher->verifier, ip);

error:
    return;
}

bool discoping_watcher_is_watching(discoping_watcher_t* watcher, const char* ip_char, const char* mac) {
    discoping_ip_t* ip = NULL;
    when_null_trace(watcher, error, ERROR, "NULL argument");
    when_str_empty_trace(ip_char, error, ERROR, "NULL/Empty argument");

    ip = s_lookup_ip(watcher, ip_char);
    if(ip == NULL) {
        return false;
    }
    if(mac == NULL) {
        return true;
    }
    return discoping_ip_has_mac(ip, mac);

error:
    return false;
}

void discoping_watcher_set_periodic_reverify_enabled(discoping_watcher_t* watcher,
                                                     const char* ip_char,
                                                     bool enabled
                                                     ) {
    discoping_ip_t* ip = NULL;
    when_null_trace(watcher, exit, ERROR, "NULL argument");

    if(enabled) {
        ip = s_add_or_lookup_ip(watcher, ip_char);
    } else {
        ip = s_lookup_ip(watcher, ip_char);
        when_null(ip, exit);
    }

    discoping_ip_set_periodic_reverify_enabled(ip, enabled);

exit:
    return;
}