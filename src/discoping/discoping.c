/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "discoping.h"
#include "discoping/discoping_socketlist.h"
#include "discoping/discoping_priv.h"
#include "arp/arp_msg.h"
#include "discoping/discoping_watcher.h"
#include "arp/arp_eth_socket.h"
#include "util.h"
#include "priv.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <string.h>
#include <stdlib.h>

/**
 * @file This .c file implements the public API in the public .h file
 *
 * It mostly serves as facade on top of the more complex internal API.
 */

struct discoping {
    discoping_socketlist_t* socketlist;
    discoping_watcher_t* watcher;
    discoping_up_cb_t up_cb;
    discoping_down_cb_t down_cb;
    discoping_collision_cb_t collision_cb;
    void* userdata;
};

bool discoping_new(discoping_t** discoping, amxo_parser_t* parser) {
    when_null_trace(discoping, error, ERROR, "NULL argument discoping");
    when_null_trace(parser, error, ERROR, "NULL argument parser");
    *discoping = (discoping_t*) calloc(1, sizeof(discoping_t));
    when_null_trace(*discoping, error, ERROR, "Out of mem");

    discoping_socketlist_new(&(*discoping)->socketlist, parser);
    when_null_trace((*discoping)->socketlist, error, ERROR, "Error creating socketlist");

    discoping_watcher_new(&(*discoping)->watcher, *discoping, (*discoping)->socketlist);
    when_null_trace((*discoping)->watcher, error, ERROR, "Error creating watcher");

    return true;

error:
    discoping_delete(discoping);
    return false;
}

bool discoping_open(discoping_t* discoping, const char* netdevname, const char* mac, const char* ip) {
    when_null_trace(discoping, error, ERROR, "NULL");
    when_null_trace(netdevname, error, ERROR, "NULL");
    when_null_trace(mac, error, ERROR, "NULL");
    when_null_trace(ip, error, ERROR, "NULL");

    return discoping_socketlist_open(discoping->socketlist, netdevname, mac, ip);

error:
    return false;
}

bool discoping_close(discoping_t* discoping, const char* netdev_name, const char* mac, const char* ip) {
    when_null_trace(discoping, error, ERROR, "NULL");

    return discoping_socketlist_close(discoping->socketlist, netdev_name, mac, ip);
error:
    return false;
}

void discoping_delete(discoping_t** discoping) {
    if((discoping == NULL) || (*discoping == NULL)) {
        return;
    }

    discoping_watcher_delete(&(*discoping)->watcher);

    discoping_socketlist_delete(&(*discoping)->socketlist);

    free(*discoping);
    *discoping = NULL;
}

void discoping_set_on_up_cb(discoping_t* discoping, discoping_up_cb_t up_cb) {
    when_null_trace(discoping, exit, ERROR, "NULL");
    discoping->up_cb = up_cb;
exit:
    return;
}

void discoping_set_on_down_cb(discoping_t* discoping, discoping_down_cb_t down_cb) {
    when_null_trace(discoping, exit, ERROR, "NULL");
    discoping->down_cb = down_cb;
exit:
    return;
}

void discoping_set_on_collision_cb(discoping_t* discoping, discoping_collision_cb_t collision_cb) {
    when_null_trace(discoping, exit, ERROR, "NULL");
    discoping->collision_cb = collision_cb;
exit:
    return;
}

void discoping_set_userdata(discoping_t* discoping, void* userdata) {
    when_null_trace(discoping, exit, ERROR, "NULL");
    discoping->userdata = userdata;
exit:
    return;
}

void discoping_internal_on_down(discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument discoping");
    when_null_trace(ip, exit, ERROR, "NULL argument ip");

    if(discoping->down_cb != NULL) {
        discoping->down_cb(ip, util_ip_family(ip), mac, discoping->userdata);
    }

exit:
    return;
}

void discoping_internal_on_up(discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument discoping");
    when_null_trace(ip, exit, ERROR, "NULL argument ip");
    when_null_trace(mac, exit, ERROR, "NULL argument mac");

    if(discoping->up_cb != NULL) {
        discoping->up_cb(ip, util_ip_family(ip), mac, discoping->userdata);
    }
exit:
    return;
}

void discoping_internal_on_collision(discoping_t* discoping, const char* ip,
                                     const amxc_var_t* colliding_macs) {

    when_null_trace(discoping, exit, ERROR, "NULL argument discoping");
    when_null_trace(ip, exit, ERROR, "NULL argument ip");
    when_null_trace(colliding_macs, exit, ERROR, "NULL argument colliding_macs");
    when_false_trace(amxc_var_type_of(colliding_macs) == AMXC_VAR_ID_LIST, exit, ERROR, "Wrong type");
    when_null_trace(GETI_CHAR(colliding_macs, 1), exit, ERROR, "Collision but < 2 macs");

    if(discoping->collision_cb != NULL) {
        discoping->collision_cb(ip, util_ip_family(ip), colliding_macs, discoping->userdata);
    }
exit:
    return;
}

void discoping_mark_seen(discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument discoping");
    when_null_trace(ip, exit, ERROR, "NULL argument ip");
    when_null_trace(mac, exit, ERROR, "NULL argument mac");

    discoping_watcher_mark_seen_user(discoping->watcher, ip, mac);

exit:
    return;
}

void discoping_mark_seen_network(discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument");

    discoping_watcher_mark_seen_network(discoping->watcher, ip, mac);
exit:
    return;
}

void discoping_verify(discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument discoping");
    when_null_trace(ip, exit, ERROR, "NULL argument ip");

    discoping_watcher_verify(discoping->watcher, ip, mac, true);

exit:
    return;
}

void discoping_verify_once(discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument discoping");
    when_null_trace(ip, exit, ERROR, "NULL argument ip");

    discoping_watcher_verify(discoping->watcher, ip, mac, false);

exit:
    return;
}


bool discoping_is_watching(discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument discoping");
    when_null_trace(ip, exit, ERROR, "NULL argument ip");

    return discoping_watcher_is_watching(discoping->watcher, ip, mac);

exit:
    return false;
}

void discoping_set_periodic_reverify_enabled(discoping_t* discoping, const char* ip, bool enabled) {
    when_null_trace(discoping, exit, ERROR, "NULL argument");
    discoping_watcher_set_periodic_reverify_enabled(discoping->watcher, ip, enabled);

exit:
    return;
}

amxc_var_t* discoping_get_open_sockets(discoping_t* discoping) {
    when_null_trace(discoping, error, ERROR, "NULL argument");
    return discoping_socketlist_get_open_sockets(discoping->socketlist);

error:
    return NULL;
}
