/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "discoping/discoping_verifier.h"
#include "discoping/discoping_ip.h"
#include "discoping/discoping_socketlist.h"
#include "priv.h"
#include "util.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <net/if.h>
#include <string.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxo/amxo.h>

typedef enum {
    /**
     * Collision detection has not been started yet
     */
    COLLISION_DETECT_STATE_BEFORE,
    /**
     * We are performing collision detection, i.e. gathering which MACs are claiming the IP.
     */
    COLLISION_DETECT_STATE_DURING,
    /**
     * We are not doing collision detection anymore, i.e. collision detection did not find
     * collisions or MACs claiming the IP at all, and on an incoming MAC claiming the IP we'll
     * assume it is the only one.
     */
    COLLISION_DETECT_STATE_AFTER,
} collision_detect_state_e;

typedef struct {
    amxc_htable_it_t it;
    discoping_ip_t* ip;
    collision_detect_state_e collision_detect_state;
    uint8_t tries_left;
} verifylist_entry_t;

struct discoping_verifier {
    /**
     * IPs we are currently verifying.
     * Element of type @ref verifylist_entry_t
     */
    amxc_htable_t verifylist;
    amxp_timer_t* send_requests_timer;
    discoping_watcher_t* watcher;
    discoping_socketlist_t* socketlist;
    amxp_timer_t* collision_timer;
};

static void s_verifylist_entry_delete(verifylist_entry_t** entry);

static verifylist_entry_t* s_lookup_entry(discoping_verifier_t* verifier, discoping_ip_t* ip) {
    amxc_htable_it_t* it = NULL;
    when_null_trace(verifier, error, ERROR, "NULL argument verifier");
    when_null_trace(ip, error, ERROR, "NULL argument ip");
    it = amxc_htable_get(&verifier->verifylist, discoping_ip_char(ip));
    when_null(it, error);
    return amxc_htable_it_get_data(it, verifylist_entry_t, it);

error:
    return NULL;
}

static verifylist_entry_t* s_verifylist_entry_lookup_or_new(discoping_verifier_t* verifier, discoping_ip_t* ip) {
    verifylist_entry_t* entry = NULL;
    when_null_trace(verifier, error, ERROR, "NULL argument verifier");
    when_null_trace(ip, error, ERROR, "NULL argument ip");

    entry = s_lookup_entry(verifier, ip);
    if(entry != NULL) {
        return entry; // already in progress to verify this ip.
    }

    entry = (verifylist_entry_t*) calloc(1, sizeof(verifylist_entry_t));
    when_null_trace(entry, error, ERROR, "Out of mem");

    entry->ip = ip;
    entry->tries_left = ARP_VERIFIER_NB_TRIES;
    entry->collision_detect_state = COLLISION_DETECT_STATE_BEFORE;

    amxc_htable_insert(&verifier->verifylist, discoping_ip_char(ip), &entry->it);

    return entry;

error:
    s_verifylist_entry_delete(&entry);
    return NULL;
}

static void s_verifylist_entry_delete(verifylist_entry_t** entry) {
    if((entry == NULL) || (*entry == NULL)) {
        return;
    }

    amxc_htable_it_clean(&(*entry)->it, NULL);
    free(*entry);
    *entry = NULL;
}

/** @implements amxc_htable_it_delete_t */
static void s_verifylist_entry_delete_htable_cb(const char* key UNUSED, amxc_htable_it_t* it) {
    verifylist_entry_t* entry = amxc_htable_it_get_data(it, verifylist_entry_t, it);
    s_verifylist_entry_delete(&entry);
}

static void s_send_request_now(discoping_verifier_t* verifier, verifylist_entry_t* entry) {
    when_null_trace(verifier, exit, ERROR, "NULL argument verifier");
    when_null_trace(entry, exit, ERROR, "NULL argument entry");
    when_false_trace(entry->tries_left > 0, exit, ERROR, "Cannot (re)try when no tries left");
    entry->tries_left--;

    discoping_socketlist_send_request(verifier->socketlist, discoping_ip_char(entry->ip));

exit:
    return;
}

void discoping_verifier_verify(discoping_verifier_t* verifier, discoping_ip_t* ip) {
    verifylist_entry_t* entry = NULL;
    when_null_trace(verifier, exit, ERROR, "NULL argument");
    when_null_trace(ip, exit, ERROR, "NULL argument");

    entry = s_verifylist_entry_lookup_or_new(verifier, ip);
    when_null_trace(entry, exit, ERROR, "Could not lookup/create entry");

    // If we're about to wrap up and we're asked to verify it now, we have to "cancel" the wrap-up
    // which we do by just setting the number of tries to non-zero again.
    if(entry->tries_left == 0) {
        entry->tries_left = ARP_VERIFIER_NB_TRIES;
    }

    if(amxp_timer_get_state(verifier->send_requests_timer) == amxp_timer_off) {
        int status = amxp_timer_start(verifier->send_requests_timer, 0);
        when_failed_trace(status, exit, ERROR, "Error starting timer");
    }

exit:
    return;
}

/**
 * Conclude whether the (un)confirmed MACs of given entry are up, down, or colliding.
 *
 * Reports this conclusion to the user via callback.
 *
 * Stop verifying given entry since we reached conclusion of verifying the entry.
 */
static void s_conclude(discoping_verifier_t* verifier, verifylist_entry_t** entry) {
    discoping_ip_t* ip = NULL;
    when_null_trace(entry, error, ERROR, "NULL argument entry");

    // We don't support callbacks manipulating discoping, but it doesn't hurt to be safe
    // against callbacks triggering re-insertion and deletion,
    // so delete before calling @ref discoping_ip_conclude
    ip = (*entry)->ip;
    s_verifylist_entry_delete(entry);

    discoping_ip_conclude(&ip, verifier->watcher);

error:
    return;
}

/** @implements amxp_timer_cb_t */
static void s_arp_requests_timer_timeout_cb(amxp_timer_t* timer, void* priv) {
    discoping_verifier_t* verifier = (discoping_verifier_t*) priv;
    bool still_todo = false;
    bool should_start_collision_timer = false;
    when_null_trace(verifier, error, ERROR, "NULL argument priv");
    when_false_trace(verifier->send_requests_timer == timer, error, ERROR, "BUG wrong timer");
    when_false_trace(amxp_timer_get_state(verifier->collision_timer) == amxp_timer_off,
                     error, ERROR, "BUG collision timer already/still running but arp request timer timed out");

    amxc_htable_for_each(it, &verifier->verifylist) {
        verifylist_entry_t* entry = amxc_htable_it_get_data(it, verifylist_entry_t, it);
        when_false_trace(entry->collision_detect_state != COLLISION_DETECT_STATE_DURING,
                         error, ERROR, "BUG collision detection did not finish before re-sending ARP");

        if(entry->collision_detect_state == COLLISION_DETECT_STATE_BEFORE) {
            when_false_trace(entry->tries_left != 0,
                             error, ERROR, "BUG collision check not started yet while no arp requests left");
            entry->collision_detect_state = COLLISION_DETECT_STATE_DURING;
            should_start_collision_timer = true;
        }
        if(entry->tries_left == 0) {
            s_conclude(verifier, &entry);
        } else {
            s_send_request_now(verifier, entry);
            still_todo = true;
        }
    }

    if(still_todo) {
        int status = amxp_timer_start(verifier->send_requests_timer, ARP_VERIFIER_RETRY_INTERVAL_MS);
        when_failed_trace(status, error, ERROR, "Error starting timer");
    }
    if(should_start_collision_timer) {
        int status = amxp_timer_start(verifier->collision_timer,
                                      ARP_VERIFIER_COLLISION_TIMEOUT_MS);
        when_failed_trace(status, error, ERROR, "Error starting collision timer");
    }

error:
    return;
}

/** @implements amxp_timer_cb_t */
static void s_collision_timeout_cb(amxp_timer_t* timer, void* priv) {
    discoping_verifier_t* verifier = (discoping_verifier_t*) priv;
    when_null_trace(verifier, error, ERROR, "NULL argument priv");
    when_false_trace(verifier->collision_timer == timer, error, ERROR, "BUG wrong timer");

    amxc_htable_for_each(it, &verifier->verifylist) {
        verifylist_entry_t* entry = amxc_htable_it_get_data(it, verifylist_entry_t, it);
        if(entry->collision_detect_state != COLLISION_DETECT_STATE_DURING) {
            continue;
        }

        if(discoping_ip_has_network_seen_macs(entry->ip)) {
            // one or more MACs seen => we can report up or collision
            s_conclude(verifier, &entry);
        } else {
            entry->collision_detect_state = COLLISION_DETECT_STATE_AFTER;
        }
    }

error:
    return;
}

bool discoping_verifier_new(discoping_verifier_t** verifier, discoping_watcher_t* watcher, discoping_socketlist_t* socketlist) {
    int status = -1;
    when_null_trace(verifier, error, ERROR, "NULL argument verifier");
    when_null_trace(watcher, error, ERROR, "NULL argument watcher");
    when_false_trace(ARP_VERIFIER_RETRY_INTERVAL_MS > ARP_VERIFIER_COLLISION_TIMEOUT_MS,
                     error, ERROR, "Invalid configuration: collision time must be less than arp request resend time");
    when_false_trace(discoping_watcher_verifier(watcher) == NULL, error, ERROR, "Watcher has another verifier");

    *verifier = (discoping_verifier_t*) calloc(1, sizeof(discoping_verifier_t));
    when_null_trace(*verifier, error, ERROR, "Out of mem");

    amxc_htable_init(&(*verifier)->verifylist, 4);

    (*verifier)->watcher = watcher;

    status = amxp_timer_new(&(*verifier)->send_requests_timer, s_arp_requests_timer_timeout_cb, *verifier);
    when_failed_trace(status, error, ERROR, "Error creating timer");

    status = amxp_timer_new(&(*verifier)->collision_timer, s_collision_timeout_cb, *verifier);
    when_failed_trace(status, error, ERROR, "Error creating collision timer");

    (*verifier)->socketlist = socketlist;

    return true;

error:
    discoping_verifier_delete(verifier);
    return false;
}

void discoping_verifier_delete(discoping_verifier_t** verifier) {
    when_null(verifier, exit);
    when_null(*verifier, exit);
    when_false_trace(discoping_watcher_verifier((*verifier)->watcher) == NULL, exit,
                     ERROR, "To enforce bidirectional association invariant, watcher must unlink first");

    amxp_timer_delete(&(*verifier)->send_requests_timer);
    amxp_timer_delete(&(*verifier)->collision_timer);

    amxc_htable_clean(&(*verifier)->verifylist, s_verifylist_entry_delete_htable_cb);

    free(*verifier);
    *verifier = NULL;

exit:
    return;
}

void discoping_verifier_mark_seen_network(discoping_verifier_t* verifier, discoping_ip_t* ip,
                                          const char* mac, bool need_collision_check) {

    verifylist_entry_t* entry = NULL;
    when_null_trace(verifier, exit, ERROR, "NULL argument verifier");
    when_null_trace(ip, exit, ERROR, "NULL argument ip");
    when_null_trace(mac, exit, ERROR, "NULL argument mac");

    entry = s_lookup_entry(verifier, ip);

    // Verifying does a collision check. If we are not verifying and a collision
    // check is needed, we do that by just starting verifying:
    if((entry == NULL) && need_collision_check) {
        discoping_verifier_verify(verifier, ip);
        return;
    }

    // Not verifying and no collision check needed? Ignore.
    when_null(entry, exit);

    if(entry->collision_detect_state == COLLISION_DETECT_STATE_AFTER) {
        s_conclude(verifier, &entry);
    }

exit:
    return;
}

