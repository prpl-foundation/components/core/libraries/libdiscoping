/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "discoping/discoping_ip.h"
#include "priv.h"
#include "util.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <net/if.h>
#include <string.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>

/**
 * Size needed to store textual representation of mac, including terminating null-char.
 */
#define MAC_CHAR_SIZE 18

typedef struct {
    amxc_htable_it_t it;
    /**
     * Arping's user's view of the state mac for the ip.
     */
    discoping_ip_mac_userview_e userview;
    /**
     * Whether (in the last "block" of time) we've seen (e.g. by ARP reply) that this MAC
     * is claiming the IP.
     */
    bool confirmed;
} discoping_ip_mac_t;

/** @implements amxc_htable_it_delete_t */
static void s_delete_ip_mac_by_it(const char* key UNUSED, amxc_htable_it_t* it) {
    discoping_ip_mac_t* mac = amxc_htable_it_get_data(it, discoping_ip_mac_t, it);
    amxc_htable_it_clean(it, NULL);
    free(mac);
}

bool discoping_ip_new(discoping_ip_t** ip) {
    when_null_trace(ip, error, ERROR, "NULL argument ip");

    *ip = (discoping_ip_t*) calloc(1, sizeof(discoping_ip_t));
    when_null_trace(*ip, error, ERROR, "Out of mem");

    amxc_htable_init(&(*ip)->macs, 1);

    (*ip)->periodic_reverify_enabled = true;

    return true;

error:
    discoping_ip_delete(ip);
    return false;
}

void discoping_ip_delete(discoping_ip_t** ip) {
    if((ip == NULL) || (*ip == NULL)) {
        return;
    }

    amxc_htable_it_clean(&(*ip)->it, NULL);

    amxc_htable_clean(&(*ip)->macs, s_delete_ip_mac_by_it);

    free(*ip);
    *ip = NULL;
}

const char* discoping_ip_char(discoping_ip_t* ip) {
    when_null_trace(ip, error, ERROR, "NULL argument ip");
    return amxc_htable_it_get_key(&ip->it);

error:
    return NULL;
}

static discoping_ip_mac_t* s_get_mac(discoping_ip_t* ip, const char* mac_char) {
    char mac_uppercase[MAC_CHAR_SIZE] = "";
    util_cstring_to_upper(mac_uppercase, sizeof(mac_uppercase), mac_char);
    amxc_htable_it_t* it = amxc_htable_get(&ip->macs, mac_uppercase);
    if(it != NULL) {
        return amxc_htable_it_get_data(it, discoping_ip_mac_t, it);
    } else {
        return NULL;
    }
}

static discoping_ip_mac_t* s_add_mac(discoping_ip_t* ip, const char* mac_char, discoping_ip_mac_userview_e userview, bool seen_network) {
    discoping_ip_mac_t* mac = NULL;

    mac = s_get_mac(ip, mac_char);
    if(mac == NULL) {
        char mac_uppercase[MAC_CHAR_SIZE] = "";
        util_cstring_to_upper(mac_uppercase, sizeof(mac_uppercase), mac_char);

        mac = (discoping_ip_mac_t*) calloc(1, sizeof(discoping_ip_mac_t));
        when_null_trace(mac, error, ERROR, "Out of mem");
        amxc_htable_insert(&ip->macs, mac_uppercase, &mac->it);
    }
    mac->confirmed = seen_network;
    mac->userview = userview;
    return mac;

error:
    return NULL;
}

void discoping_ip_add_or_set_mac(discoping_ip_t* ip, const char* mac_char,
                                 discoping_ip_mac_userview_e userview) {

    discoping_ip_mac_t* mac = NULL;
    char mac_uppercase[MAC_CHAR_SIZE] = "";
    when_null_trace(ip, error, ERROR, "NULL argument ip");
    when_null_trace(mac_char, error, ERROR, "NULL argument mac_char");

    util_cstring_to_upper(mac_uppercase, sizeof(mac_uppercase), mac_char);

    mac = s_get_mac(ip, mac_uppercase);
    if(mac == NULL) {
        s_add_mac(ip, mac_uppercase, userview, false);
    } else {
        mac->userview = userview;
    }

error:
    return;
}

bool discoping_ip_has_mac(discoping_ip_t* ip, const char* mac_char) {
    char mac_uppercase[MAC_CHAR_SIZE] = "";
    when_null_trace(ip, exit, ERROR, "NULL argument");
    when_null_trace(mac_char, exit, ERROR, "NULL argument");

    util_cstring_to_upper(mac_uppercase, sizeof(mac_uppercase), mac_char);

    return amxc_htable_contains(&ip->macs, mac_uppercase);

exit:
    return false;
}


void discoping_ip_mark_seen_network(discoping_ip_t* ip, const char* mac_char) {
    discoping_ip_mac_t* mac = NULL;
    when_null_trace(ip, error, ERROR, "NULL argument ip");
    when_null_trace(mac_char, error, ERROR, "NULL argument mac_char");

    mac = s_get_mac(ip, mac_char);
    if(mac == NULL) {
        s_add_mac(ip, mac_char, ARP_IP_MAC_USERVIEW_UNREACHABLE, true);
    } else {
        mac->confirmed = true;
    }

error:
    return;
}

void discoping_ip_conclude(discoping_ip_t** ip, discoping_watcher_t* watcher) {
    amxc_var_t reachable;
    bool userview_changed = false;
    bool had_no_macs = false;
    amxc_var_init(&reachable);
    amxc_var_set_type(&reachable, AMXC_VAR_ID_LIST);
    when_null_trace(ip, exit, ERROR, "NULL argument");
    when_null_trace(*ip, exit, ERROR, "NULL argument");
    when_null_trace(watcher, exit, ERROR, "NULL argument");

    had_no_macs = !discoping_ip_has_macs(*ip);

    amxc_htable_for_each(it, &(*ip)->macs) {
        const char* mac_char = amxc_htable_it_get_key(it);
        discoping_ip_mac_t* mac = amxc_htable_it_get_data(it, discoping_ip_mac_t, it);
        discoping_ip_mac_userview_e target_userview =
            mac->confirmed ? ARP_IP_MAC_USERVIEW_REACHABLE
            : ARP_IP_MAC_USERVIEW_UNREACHABLE;
        if(target_userview != mac->userview) {
            userview_changed = true;
            mac->userview = target_userview;
        }
        if(mac->confirmed) {
            amxc_var_add(cstring_t, &reachable, mac_char);
        } else {
            discoping_watcher_on_down(watcher, *ip, mac_char);
            amxc_htable_it_clean(it, s_delete_ip_mac_by_it);
        }
    }

    if(userview_changed) {
        if(GETI_CHAR(&reachable, 1) != NULL) {        // at least 2 MACS reachable
            discoping_watcher_on_collision(watcher, *ip, &reachable);
        } else if(GETI_CHAR(&reachable, 0) != NULL) { // exactly 1 MAC reachable
            const char* mac = GETI_CHAR(&reachable, 0);
            discoping_watcher_on_up(watcher, *ip, mac);
        }
    }

    // Note: must also happen if userview unchanged (there are no macs that can change).
    if((GETI_CHAR(&reachable, 0) == NULL) && had_no_macs) {
        discoping_watcher_on_down(watcher, *ip, NULL);
    }

    if(amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, &reachable))) {
        discoping_ip_delete(ip);
    }

exit:
    amxc_var_clean(&reachable);
}

bool discoping_ip_clear_network_seen(discoping_ip_t* ip) {
    bool unconfirmed_mac = false;
    when_null_trace(ip, exit, ERROR, "NULL argument");

    amxc_htable_for_each(it, &ip->macs) {
        discoping_ip_mac_t* mac = amxc_htable_it_get_data(it, discoping_ip_mac_t, it);

        if(!mac->confirmed) {
            unconfirmed_mac = true;
        }
        mac->confirmed = false;
    }

    return unconfirmed_mac || amxc_htable_is_empty(&ip->macs);
exit:
    return false;
}

bool discoping_ip_has_network_seen_macs(discoping_ip_t* ip) {
    when_null_trace(ip, exit, ERROR, "NULL argument");

    amxc_htable_for_each(it, &ip->macs) {
        discoping_ip_mac_t* mac = amxc_htable_it_get_data(it, discoping_ip_mac_t, it);

        if(mac->confirmed) {
            return true;
        }
    }

exit:
    return false;
}

bool discoping_ip_has_macs(discoping_ip_t* ip) {
    when_null_trace(ip, error, ERROR, "NULL argument");

    return !amxc_htable_is_empty(&ip->macs);
error:
    return false;
}

bool PRIVATE discoping_ip_get_periodic_reverify_enabled(discoping_ip_t* ip) {
    when_null_trace(ip, error, ERROR, "NULL argument");
    return ip->periodic_reverify_enabled;
error:
    return false;
}

void discoping_ip_set_periodic_reverify_enabled(discoping_ip_t* ip, bool periodic_reverify_enabled) {
    when_null_trace(ip, error, ERROR, "NULL argument");

    ip->periodic_reverify_enabled = periodic_reverify_enabled;

error:
    return;
}