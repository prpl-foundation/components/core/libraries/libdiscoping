/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "discoping/discoping_socketlist.h"
#include "arp/arp_msg.h"
#include "discoping/discoping_watcher.h"
#include "discoping/discoping_socket_implementations.h"
#include "arp/arp_socket_priv.h"
#include "ndp/ndp_socket_priv.h"
#include "priv.h"
#include "util.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

typedef struct {
    amxc_htable_it_t hit;
    /** never NULL */
    const discoping_socket_t* socket;
    /** never NULL. Data for `socket`. */
    void* socket_data;
    char* physaddress;
    char* ipaddress;
    char* netdevname;
} socket_entry_t;

struct discoping_socketlist {
    /** values are of type @ref socket_entry_t */
    amxc_htable_t socket_entries;
    amxo_parser_t* parser;
    discoping_watcher_t* watcher;
};

bool discoping_socketlist_new(discoping_socketlist_t** socketlist, amxo_parser_t* parser) {
    when_null_trace(socketlist, error, ERROR, "NULL");
    *socketlist = (discoping_socketlist_t*) calloc(1, sizeof(discoping_socketlist_t));
    when_null_trace(*socketlist, error, ERROR, "Out of mem");

    (*socketlist)->parser = parser;
    amxc_htable_init(&(*socketlist)->socket_entries, 4);

    return true;

error:
    discoping_socketlist_delete(socketlist);
    return false;
}

static void s_get_entry_key(amxc_string_t* tgt_key, const char* netdevname, const char* mac_from,
                            const char* ip_from) {
    amxc_string_setf(tgt_key, "%s__%s__", mac_from, ip_from);
    amxc_string_to_lower(tgt_key);
    amxc_string_append(tgt_key, netdevname, strlen(netdevname));
}

static socket_entry_t* s_get_entry(discoping_socketlist_t* socketlist,
                                   const char* netdev_name,
                                   const char* mac_interface,
                                   const char* ip_interface) {
    amxc_htable_it_t* hit = NULL;
    socket_entry_t* entry = NULL;
    amxc_string_t key;
    amxc_string_init(&key, 96);
    when_null_trace(netdev_name, exit, ERROR, "NULL");
    when_null_trace(socketlist, exit, ERROR, "NULL");
    when_null_trace(mac_interface, exit, ERROR, "NULL");
    when_null_trace(ip_interface, exit, ERROR, "NULL");

    s_get_entry_key(&key, netdev_name, mac_interface, ip_interface);
    hit = amxc_htable_get(&socketlist->socket_entries, amxc_string_get(&key, 0));

    when_null(hit, exit);
    entry = amxc_htable_it_get_data(hit, socket_entry_t, hit);
exit:
    amxc_string_clean(&key);
    return entry;
}

bool discoping_socketlist_open(discoping_socketlist_t* socketlist,
                               const char* netdevname, const char* mac, const char* ip) {
    bool ok = false;
    int ret = -1;
    amxc_htable_it_t* hit = NULL;
    socket_entry_t* entry = NULL;
    const discoping_socket_t* socket = NULL;
    void* socket_data = NULL;
    amxc_string_t key;
    amxc_string_init(&key, 96);
    when_null_trace(socketlist, exit, ERROR, "NULL");
    when_null_trace(mac, exit, ERROR, "NULL");
    when_null_trace(ip, exit, ERROR, "NULL");

    s_get_entry_key(&key, netdevname, mac, ip);

    hit = amxc_htable_get(&socketlist->socket_entries, amxc_string_get(&key, 0));
    when_false_trace(hit == NULL, exit, INFO, "Already open %s %s %s (%s)", netdevname, mac, ip, amxc_string_get(&key, 0));

    // Search a socket implementation that can handle it.
    for(size_t i = 0; i < ARRAY_SIZE(discoping_socket_implementations); i++) {
        socket = discoping_socket_implementations[i];
        socket->new_socket(&socket_data, socketlist->parser, netdevname, ip, mac, discoping_socketlist_mark_seen_network);
        if(socket_data != NULL) {
            socket->set_userdata(&socket_data, socketlist);
            break;
        }
    }
    when_null_trace(socket_data, exit, ERROR, "No socket implementation can handle %s %s %s",
                    netdevname, mac, ip);

    // Create entry:
    entry = (socket_entry_t*) calloc(1, sizeof(socket_entry_t));
    when_null_trace(entry, exit, ERROR, "Out of mem");
    entry->ipaddress = strdup(ip);
    entry->physaddress = strdup(mac);
    entry->netdevname = strdup(netdevname);
    entry->socket = socket;
    entry->socket_data = socket_data;

    // Add entry:
    ret = amxc_htable_insert(&socketlist->socket_entries, amxc_string_get(&key, 0), &entry->hit);
    when_failed_trace(ret, exit, ERROR, "Out of mem");

    // Scan:
    socket->scan(socket_data);

    ok = true;
exit:
    amxc_string_clean(&key);
    if(!ok) {
        if(socket != NULL) {
            socket->delete_socket(&socket_data);
        }
        if(entry != NULL) {
            free(entry->ipaddress);
            free(entry->physaddress);
            free(entry->netdevname);
            free(entry);
        }
    }
    return ok;
}

/**
 * @implements amxc_htable_it_delete_t
 */
static void s_delete_eth_socket_entry(const char* key UNUSED, amxc_htable_it_t* hit) {
    socket_entry_t* entry = amxc_htable_it_get_data(hit, socket_entry_t, hit);
    entry->socket->delete_socket(&entry->socket_data);
    free(entry->ipaddress);
    free(entry->physaddress);
    free(entry->netdevname);
    free(entry);
}

bool discoping_socketlist_close(discoping_socketlist_t* socketlist, const char* netdev_name, const char* mac, const char* ip) {
    bool ok = false;
    socket_entry_t* entry = s_get_entry(socketlist, netdev_name, mac, ip);
    when_null_trace(entry, exit, INFO, "Closing non opened %s %s %s", netdev_name, mac, ip);

    amxc_htable_it_clean(&entry->hit, s_delete_eth_socket_entry);

    ok = true;

exit:
    return ok;
}

void discoping_socketlist_delete(discoping_socketlist_t** socketlist) {
    if((socketlist == NULL) || (*socketlist == NULL)) {
        return;
    }

    amxc_htable_clean(&(*socketlist)->socket_entries, s_delete_eth_socket_entry);

    free(*socketlist);
    *socketlist = NULL;
}

void discoping_socketlist_set_watcher(discoping_socketlist_t* socketlist, discoping_watcher_t* watcher) {
    when_null_trace(socketlist, error, ERROR, "NULL argument socketlist");
    socketlist->watcher = watcher;

error:
    return;
}

void discoping_socketlist_mark_seen_network(const char* ip_char, const char* mac, void* socketlist) {
    discoping_socketlist_t* self = (discoping_socketlist_t*) socketlist;
    when_null_trace(self, exit, ERROR, "NULL argument");

    discoping_watcher_mark_seen_network(self->watcher, ip_char, mac);

exit:
    return;
}

bool discoping_socketlist_send_request(discoping_socketlist_t* socketlist, const char* ip_to) {
    bool success = false;
    when_null_trace(socketlist, exit, ERROR, "NULL argument socketlist");
    when_null_trace(ip_to, exit, ERROR, "NULL argument");

    amxc_htable_for_each(it, &socketlist->socket_entries) {
        socket_entry_t* entry = amxc_htable_it_get_data(it, socket_entry_t, hit);

        success |= entry->socket->send_request(entry->socket_data, entry->ipaddress, ip_to, entry->physaddress);
    }

    when_false_trace(success, exit, ERROR, "Failed to send request (%zu sockets open)",
                     amxc_htable_size(&socketlist->socket_entries));

exit:
    return success;
}

void discoping_socketlist_scan(discoping_socketlist_t* socketlist) {
    when_null_trace(socketlist, exit, ERROR, "NULL argument");

    amxc_htable_for_each(it, &socketlist->socket_entries) {
        socket_entry_t* entry = amxc_htable_it_get_data(it, socket_entry_t, hit);
        entry->socket->scan(entry->socket_data);
    }

exit:
    return;
}

amxc_var_t* discoping_socketlist_get_open_sockets(discoping_socketlist_t* socketlist) {
    amxc_var_t* ret = NULL;
    when_null_trace(socketlist, error, ERROR, "NULL argument");
    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_htable_for_each(it, &socketlist->socket_entries) {
        socket_entry_t* entry = amxc_htable_it_get_data(it, socket_entry_t, hit);
        amxc_var_t* list_item = amxc_var_add_new(ret);
        amxc_var_set_type(list_item, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, list_item, "mac", entry->physaddress);
        amxc_var_add_key(cstring_t, list_item, "ip", entry->ipaddress);
        amxc_var_add_key(cstring_t, list_item, "netdev_name", entry->netdevname);
    }

    return ret;
error:
    amxc_var_delete(&ret);
    return NULL;
}