/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "arp/arp_eth_socket.h"
#include "priv.h"
#include "util.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <net/if.h>
#include <string.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>

struct arp_eth_socket {
    /** Used both bind when starting listening and when sending ARP packets */
    struct sockaddr_ll* addr;
    amxo_parser_t* odl_parser;
    int socket_fd;
    eth_frame_cb_t callback;
    void* userdata;
};

static bool s_get_frame(arp_eth_socket_t* socket, int sockfd, arp_eth_socket_frame_t* target_eth_frame) {
    bool retval = false;
    struct msghdr hdr = {0};
    struct iovec vec = {0};
    struct cmsghdr* cmsgptr = NULL;
    char controldata[1000] = {0};
    when_false_trace(sockfd >= 0, exit, ERROR, "Invalid socket file descriptor");

    vec.iov_base = &(target_eth_frame->frame);
    vec.iov_len = sizeof(target_eth_frame->frame);

    hdr.msg_name = &(target_eth_frame->address);
    hdr.msg_namelen = sizeof(struct sockaddr_ll);
    hdr.msg_iov = &vec;
    hdr.msg_iovlen = 1;
    hdr.msg_control = controldata;
    hdr.msg_controllen = sizeof(controldata);

    target_eth_frame->rdlen = recvmsg(sockfd, &hdr, 0);
    when_false_trace(target_eth_frame->rdlen != -1, exit, ERROR, "Failed to read data (%d)", errno);

    if(target_eth_frame->rdlen > (ssize_t) sizeof(target_eth_frame->frame)) {
        SAH_TRACEZ_INFO(ME, "received jumbo frame of %zd bytes len, truncated", target_eth_frame->rdlen);
    }

    when_false_trace(target_eth_frame->address.sll_pkttype != PACKET_OUTGOING, exit,
                     INFO, "Received outgoing frame, do not handle these");

    when_false_trace(socket->addr->sll_ifindex == 0 || target_eth_frame->address.sll_ifindex == socket->addr->sll_ifindex, exit,
                     INFO, "Received frame on wrong interface %d", target_eth_frame->address.sll_ifindex);

    // retrieve vlan id
    for(cmsgptr = CMSG_FIRSTHDR(&hdr); cmsgptr != NULL; cmsgptr = CMSG_NXTHDR(&hdr, cmsgptr)) {
        if(cmsgptr->cmsg_type == PACKET_AUXDATA) {
            struct tpacket_auxdata* aux = (struct tpacket_auxdata*) CMSG_DATA(cmsgptr);
            target_eth_frame->vlanId = aux->tp_vlan_tci;
            retval = true;
            break;
        }
    }

exit:
    return retval;
}

/**
 * @implements amxo_fd_cb_t
 */
static void s_recvmsg(int sockfd, void* priv) {
    arp_eth_socket_t* socket = (arp_eth_socket_t*) priv;
    arp_eth_socket_frame_t eth_frame;
    bool ok = false;
    memset(&eth_frame, 0, sizeof(arp_eth_socket_frame_t));
    when_null_trace(socket, exit, ERROR, "NULL");

    ok = s_get_frame(socket, sockfd, &eth_frame);
    when_false(ok, exit); // can legitimately happen (some messages are filtered)

    if(socket->callback != NULL) {
        socket->callback(&eth_frame, socket->userdata);
    }

exit:
    return;
}

bool arp_eth_socket_new(arp_eth_socket_t** self,
                        amxo_parser_t* odl_parser, const char* netdev_name,
                        eth_frame_cb_t callback, void* userdata) {

    struct ifreq ifr;
    int ret = -1;
    int flags = 0;
    int yes = 1;
    memset(&ifr, 0, sizeof(struct ifreq));
    when_null_trace(odl_parser, error, ERROR, "NULL");
    when_null_trace(self, error, ERROR, "NULL/Empty argument socket");

    SAH_TRACEZ_INFO(ME, "Opening peer for %s", netdev_name);

    *self = (arp_eth_socket_t*) calloc(1, sizeof(arp_eth_socket_t));
    when_null_trace(*self, error, ERROR, "Out of mem");
    (*self)->socket_fd = -1;

    (*self)->userdata = userdata;
    (*self)->callback = callback;

    (*self)->addr = (struct sockaddr_ll*) calloc(1, sizeof(struct sockaddr_ll) + 4);
    when_null_trace((*self)->addr, error, ERROR, "Out of mem");

    (*self)->odl_parser = odl_parser;

    // create the socket
    (*self)->socket_fd = socket(PF_PACKET, SOCK_DGRAM, htons(ETHERTYPE_ARP));
    when_false_trace((*self)->socket_fd != -1, error, ERROR, "Failed to create socket: %d %s", errno, strerror(errno));

    // Bind to interface
    if(netdev_name != NULL) {
        strncpy(ifr.ifr_name, netdev_name, IFNAMSIZ - 1);
        ret = ioctl((*self)->socket_fd, SIOCGIFINDEX, &ifr);
        when_false_trace(ret != -1, error, ERROR, "Failed to get interface index for name %s: %d %s", netdev_name, errno, strerror(errno));
    }

    (*self)->addr->sll_family = AF_PACKET;
    (*self)->addr->sll_protocol = htons(ETHERTYPE_ARP);
    (*self)->addr->sll_ifindex = ifr.ifr_ifindex;

    ret = bind((*self)->socket_fd, (struct sockaddr*) (*self)->addr, sizeof(struct sockaddr_ll));
    when_false_trace(ret != -1, error, ERROR, "Failed to bind: %d %s", errno, strerror(errno));

    (*self)->addr->sll_halen = 8;
    for(int i = 0; i < 8; i++) {
        (*self)->addr->sll_addr[i] = 0xFF;
    }

    ret = setsockopt((*self)->socket_fd, SOL_PACKET, PACKET_AUXDATA, &yes, sizeof(yes));
    when_false_trace(ret != -1, error, ERROR, "socket sesockopt PACKET_AUXDATA failed: %d %s", errno, strerror(errno));

    // set non-blocking
    flags = fcntl((*self)->socket_fd, F_GETFL, 0);
    when_false_trace(flags >= 0, error, ERROR, "socket fnctl F_GETFL failed: %d %s", errno, strerror(errno));
    ret = fcntl((*self)->socket_fd, F_SETFL, flags | O_NONBLOCK);
    when_false_trace(ret >= 0, error, ERROR, "socket fnctl F_SETFL failed %d %s", errno, strerror(errno));

    // add to event loop
    ret = amxo_connection_add(odl_parser, (*self)->socket_fd, s_recvmsg, NULL, AMXO_CUSTOM, (*self));
    when_failed_trace(ret, error, ERROR, "amxo_connection_add() failed %d", ret);

    return true;

error:
    arp_eth_socket_delete(self);
    return false;
}

void arp_eth_socket_delete(arp_eth_socket_t** socket) {
    if((socket == NULL) || (*socket == NULL)) {
        return;
    }

    free((*socket)->addr);
    if((*socket)->socket_fd != -1) {
        amxo_connection_remove((*socket)->odl_parser, (*socket)->socket_fd);
        close((*socket)->socket_fd);
    }

    free(*socket);
    *socket = NULL;
}

bool arp_eth_socket_send(arp_eth_socket_t* socket, const char* to_mac, const void* message, size_t message_len) {
    int status = -1;
    bool ok = false;
    when_null_trace(socket, error, ERROR, "NULL");
    when_null_trace(message, error, ERROR, "NULL");

    ok = util_mac_string_to_bytes(socket->addr->sll_addr, to_mac);
    when_false_trace(ok, error, ERROR, "Error converting mac %s", to_mac);

    status = sendto(socket->socket_fd, message, message_len, 0, (struct sockaddr*) socket->addr, sizeof(struct sockaddr_ll));
    when_false_trace(status != -1 && status == (int) message_len, error, ERROR, "Error sending ethernet frame");

    return true;

error:
    return false;
}