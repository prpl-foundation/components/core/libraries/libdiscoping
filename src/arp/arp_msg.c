/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "arp/arp_msg.h"
#include "arp/arp_eth_socket.h"
#include "priv.h"
#include "util.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <net/if.h>
#include <net/ethernet.h>
#include <string.h>

bool arp_msg_filter_and_parse(arp_msg_t* tgt_arp_msg, const arp_eth_socket_frame_t* eth_frame) {
    bool retval = false;
    arp_msg_bin_t* msg = NULL;
    when_null_trace(tgt_arp_msg, exit, ERROR, "NULL");
    when_null_trace(eth_frame, exit, ERROR, "NULL");

    when_false_trace(eth_frame->address.sll_protocol == htons(ETHERTYPE_ARP), exit, INFO, "Received not an ethernet arp packet");

    when_false_trace(eth_frame->vlanId == 0, exit, INFO, "Received ARP is tagged with vlan %d", eth_frame->vlanId);

    when_false_trace(sizeof(arp_msg_bin_t) <= sizeof(eth_frame->frame), exit, ERROR, "Frame buffer space too short");
    when_false_trace(eth_frame->rdlen >= (ssize_t) sizeof(arp_msg_bin_t), exit, INFO, "Received ARP message is too short");
    msg = (arp_msg_bin_t*) eth_frame->frame;

    msg->arp_header.hw_addr_format = ntohs(msg->arp_header.hw_addr_format);
    msg->arp_header.protocol_addr_format = ntohs(msg->arp_header.protocol_addr_format);
    msg->arp_header.hw_addr_length = 6;
    msg->arp_header.protocol_addr_length = 4;
    msg->arp_header.operation_type = ntohs(msg->arp_header.operation_type);

    util_mac_bytes_to_string(tgt_arp_msg->mac_sender, msg->sender_hw_addr);
    util_mac_bytes_to_string(tgt_arp_msg->mac_target, msg->target_hw_addr);
    inet_ntop(AF_INET, &msg->sender_ip, tgt_arp_msg->ip_sender, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, &msg->target_ip, tgt_arp_msg->ip_target, INET_ADDRSTRLEN);

    when_false_trace(msg->arp_header.operation_type == ARP_MSG_OPERATION_TYPE_REQUEST
                     || msg->arp_header.operation_type == ARP_MSG_OPERATION_TYPE_REPLY, exit,
                     INFO, "Invalid ARP operation type");
    tgt_arp_msg->operation_type = (arp_msg_operation_type_e) msg->arp_header.operation_type;

    retval = true;
exit:
    return retval;
}

bool arp_msg_serialize(arp_msg_bin_t* tgt_arp_bin, const arp_msg_t* arp_msg) {
    bool ok = false;
    int status = -1;
    when_null_trace(tgt_arp_bin, error, ERROR, "NULL");
    when_null_trace(arp_msg, error, ERROR, "NULL");
    when_false_trace(arp_msg->operation_type == ARP_MSG_OPERATION_TYPE_REQUEST, error,
                     ERROR, "Only request type for manually sending ARP is allowed");

    tgt_arp_bin->arp_header = (arp_msg_bin_hdr_t) {
        .hw_addr_format = htons(1),            // ethernet
        .protocol_addr_format = htons(0x0800), // ipv4
        .hw_addr_length = 6,
        .protocol_addr_length = 4,
        .operation_type = htons(arp_msg->operation_type),
    };

    ok = util_mac_string_to_bytes(tgt_arp_bin->sender_hw_addr, arp_msg->mac_sender);
    when_false_trace(ok, error, ERROR, "Error converting mac %s", arp_msg->mac_sender);

    ok = util_mac_string_to_bytes(tgt_arp_bin->target_hw_addr, arp_msg->mac_target);
    when_false_trace(ok, error, ERROR, "Error converting mac %s", arp_msg->mac_target);

    status = inet_pton(AF_INET, arp_msg->ip_sender, &tgt_arp_bin->sender_ip);
    when_false_trace(status == 1, error, ERROR, "Error converting ip %s", arp_msg->ip_sender);

    status = inet_pton(AF_INET, arp_msg->ip_target, &tgt_arp_bin->target_ip);
    when_false_trace(status == 1, error, ERROR, "Error converting ip %s", arp_msg->ip_target);

    return true;
error:
    if(tgt_arp_bin != NULL) {
        memset(tgt_arp_bin, 0, sizeof(arp_msg_bin_t));
    }
    return false;
}
