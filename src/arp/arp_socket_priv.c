/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "discoping.h"
#include "arp/arp_socket_priv.h"
#include "arp/arp_msg.h"
#include "arp/arp_eth_socket.h"
#include "priv.h"
#include "util.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <net/if.h>
#include <string.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>

struct arp_socket {
    arp_eth_socket_t* eth_socket;
    socket_common_reply_cb_t ext_cb;
    void* userdata;
};

bool arp_socket_internal_on_arp_msg(arp_socket_t* self, arp_msg_t* arp_msg) {
    bool is_gratuitous = false;
    when_null_trace(self, error, ERROR, "NULL");
    when_null_trace(arp_msg, error, ERROR, "NULL");

    is_gratuitous = strcmp(arp_msg->ip_sender, arp_msg->ip_target) == 0;

    if(is_gratuitous) {
        self->ext_cb(arp_msg->ip_sender, arp_msg->mac_sender, self->userdata);
    } else if((arp_msg->operation_type == ARP_MSG_OPERATION_TYPE_REPLY)
              && (0 == strcmp(arp_msg->mac_target, "00:00:00:00:00:00"))) {
        // Invalid reply, ignore it.
    } else if(arp_msg->operation_type == ARP_MSG_OPERATION_TYPE_REPLY) {
        self->ext_cb(arp_msg->ip_sender, arp_msg->mac_sender, self->userdata);
    }
    return true;

error:
    return false;
}

void arp_socket_internal_on_eth_frame(arp_socket_t* self, const arp_eth_socket_frame_t* frame) {
    bool ok = false;
    arp_msg_t arp_msg;
    memset(&arp_msg, 0, sizeof(arp_msg_t));
    when_null_trace(self, exit, ERROR, "NULL argument");
    when_null_trace(frame, exit, ERROR, "NULL argument");

    ok = arp_msg_filter_and_parse(&arp_msg, frame);
    when_false(ok, exit); // can legitimately happen if filtered.

    arp_socket_internal_on_arp_msg(self, &arp_msg);

exit:
    return;
}

/**
 * @implements eth_frame_cb_t
 */
static void s_on_eth_frame_cb(const arp_eth_socket_frame_t* frame, void* userdata) {
    arp_socket_t* self = (arp_socket_t*) userdata;
    arp_socket_internal_on_eth_frame(self, frame);
}

bool arp_socket_new(arp_socket_t** self,
                    amxo_parser_t* odl_parser, const char* netdev_name,
                    const char* ip_from,
                    socket_common_reply_cb_t cb) {

    when_null_trace(self, error, ERROR, "NULL argument");
    when_null_trace(odl_parser, error, ERROR, "NULL argument");
    when_null_trace(cb, error, ERROR, "NULL argument");
    when_str_empty_trace(ip_from, error, ERROR, "NULL/empty argument");
    when_false_trace(util_ip_family(ip_from) == AF_INET, error, INFO, "ARP is for ipv4 only");

    *self = (arp_socket_t*) calloc(1, sizeof(arp_socket_t));
    when_null_trace(*self, error, ERROR, "Out of mem");
    (*self)->ext_cb = cb;

    arp_eth_socket_new(&(*self)->eth_socket, odl_parser, netdev_name, s_on_eth_frame_cb, *self);
    when_null_trace((*self)->eth_socket, error, ERROR, "Error creating eth socket");

    return true;

error:
    arp_socket_delete(self);
    return false;
}

void arp_socket_set_userdata(arp_socket_t** self, void* userdata) {
    when_null_trace(*self, exit, ERROR, "NULL");
    (*self)->userdata = userdata;
exit:
    return;
}

void arp_socket_delete(arp_socket_t** self) {
    if((self == NULL) || (*self == NULL)) {
        return;
    }

    arp_eth_socket_delete(&(*self)->eth_socket);

    free(*self);
    *self = NULL;
}

bool arp_socket_send_request(arp_socket_t* self, const char* ip_from,
                             const char* ip_to, const char* mac_from) {

    bool ok = false;
    arp_msg_bin_t bin_msg;
    arp_msg_t msg = {
        .ip_sender = {},
        .ip_target = {},
        .mac_sender = {},
        .mac_target = "00:00:00:00:00:00",
        .operation_type = ARP_MSG_OPERATION_TYPE_REQUEST,
    };
    when_null_trace(self, exit, ERROR, "NULL argument");
    when_str_empty_trace(ip_from, exit, ERROR, "NULL/empty argument");
    when_str_empty_trace(ip_to, exit, ERROR, "NULL/empty argument");
    when_str_empty_trace(mac_from, exit, ERROR, "NULL/empty argument");
    when_false_trace(util_ip_family(ip_to) == AF_INET, exit, INFO, "ARP is ipv4 only");

    strncpy(msg.ip_sender, ip_from, sizeof(msg.ip_sender) - 1);
    strncpy(msg.ip_target, ip_to, sizeof(msg.ip_target) - 1);
    strncpy(msg.mac_sender, mac_from, sizeof(msg.mac_sender) - 1);

    ok = arp_msg_serialize(&bin_msg, &msg);
    when_false_trace(ok, exit, ERROR, "Error serializing ARP message %s", ip_to);

    ok = arp_eth_socket_send(self->eth_socket, "FF:FF:FF:FF:FF:FF", &bin_msg,
                             sizeof(arp_msg_bin_t));
    when_false_trace(ok, exit, ERROR, "Error sending ARP message %s", ip_to);

exit:
    return ok;
}

/** @implements discoping_socket_t.new_socket() */
static void s_new(void** self, amxo_parser_t* odl_parser, const char* netdev_name,
                  const char* ip_from, const char* mac_from UNUSED, socket_common_reply_cb_t cb) {
    arp_socket_new((arp_socket_t**) self, odl_parser, netdev_name, ip_from, cb);
}

/** @implements discoping_socket_t.set_userdata() */
static void s_set_userdata(void** self, void* userdata) {
    arp_socket_set_userdata((arp_socket_t**) self, userdata);
}

/** @implements discoping_socket_t.delete_socket() */
static void s_delete(void** self) {
    arp_socket_delete((arp_socket_t**) self);
}

/** @implements discoping_socket_t.send_request() */
static bool s_send_request(void* self, const char* ip_from, const char* ip_to, const char* mac_from) {
    return arp_socket_send_request((arp_socket_t*) self, ip_from, ip_to, mac_from);
}

/** @implements discoping_socket_t.scan() */
static void s_scan(UNUSED void* self) {
    // On ipv4/arp we do not do a broadcast ping.
    // So do nothing.
}

discoping_socket_t arp_socket_impl = {
    .new_socket = s_new,
    .set_userdata = s_set_userdata,
    .delete_socket = s_delete,
    .send_request = s_send_request,
    .scan = s_scan,
};