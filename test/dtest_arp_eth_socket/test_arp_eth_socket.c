/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_arp_eth_socket.h"
#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "../common/eventing.h"
#include "arp/arp_eth_socket.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>

#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>

#include <event2/event.h>


#define ARP_PACKET "\x00\x01"                 /* hardware type*/ \
    "\x08\x00"                                /* protocol type */ \
    "\x06"                                    /* hardware address length */ \
    "\x04"                                    /* protocol address length */ \
    "\x00\x01"                                /* operation type */ \
    "\x00\x00\x00\x00\x00\x00"                /* sender hardware(mac) address */ \
    "\x7f\x00\x00\x01"                        /* sender protocol(ip) address  */ \
    "\x00\x00\x00\x00\x00\x00"                /* target hardware(mac) address */ \
    "\x7f\x00\x00\x01"                        /* target protocol(ip) address */

typedef struct {
    eventing_t* eventing;
    arp_eth_socket_frame_t received_frame;
} cb_data_t;

/** @implements eth_frame_cb_t */
static void s_eth_frame_cb(const arp_eth_socket_frame_t* frame, void* userdata) {
    cb_data_t* cb_data = userdata;
    assert_non_null(cb_data);
    cb_data->received_frame = *frame;

    eventing_loopbreak(cb_data->eventing);
}

void test_arp_eth_socket_receive(UNUSED void** state) {
    arp_eth_socket_t* socket = NULL;
    eventing_t* eventing = NULL;
    cb_data_t cb_data = {0};
    if(!test_util_has_permission_to_open_arp_socket()) {
        printf("No permissions to open ARP socket, so cannot run test. Skipping.\n");
        skip();
    }

    // GIVEN an eth socket
    eventing = eventing_create(test_setup_parser());
    cb_data.eventing = eventing;
    bool ok = arp_eth_socket_new(&socket, test_setup_parser(), "lo", s_eth_frame_cb, &cb_data);
    assert_true(ok);
    test_util_handle_events();

    // WHEN an ethernet packet is send
    uint8_t buffer[] =
        "\xff\xff\xff\xff\xff\xff"         // destination mac
        "\x01\x02\x03\x04\x05\x06"         // source mac
        "\x08\x06"                         // type = arp
        ARP_PACKET;
    eventing_send_eth_frame(eventing, buffer, sizeof(buffer) - 1);
    test_util_handle_events();
    eventing_once(eventing);

    // THEN the received frame content equals the sent frame content:
    assert_int_equal(28, cb_data.received_frame.rdlen);
    assert_int_equal(0, memcmp(cb_data.received_frame.frame, ARP_PACKET, 28));
    // THEN the address received from equals the address sent from
    assert_int_equal(1, cb_data.received_frame.address.sll_addr[0]);
    assert_int_equal(2, cb_data.received_frame.address.sll_addr[1]);
    assert_int_equal(3, cb_data.received_frame.address.sll_addr[2]);
    assert_int_equal(4, cb_data.received_frame.address.sll_addr[3]);
    assert_int_equal(5, cb_data.received_frame.address.sll_addr[4]);
    assert_int_equal(6, cb_data.received_frame.address.sll_addr[5]);
    // THEN vlanId is 0 because no vlan used
    assert_int_equal(0, cb_data.received_frame.vlanId);

    // cleanup:
    arp_eth_socket_delete(&socket);
    eventing_delete(&eventing);
}


void test_arp_eth_socket_send(UNUSED void** state) {
    arp_eth_socket_t* socket1 = NULL;
    arp_eth_socket_t* socket2 = NULL;
    eventing_t* eventing = NULL;
    cb_data_t cb_data = {0};
    bool ok = false;
    if(!test_util_has_permission_to_open_arp_socket()) {
        printf("No permissions to open ARP socket, so cannot run test. Skipping.\n");
        skip();
    }

    // GIVEN two eth sockets
    eventing = eventing_create(test_setup_parser());
    cb_data.eventing = eventing;
    ok = arp_eth_socket_new(&socket1, test_setup_parser(), "lo", NULL, NULL);
    assert_true(ok);
    ok = arp_eth_socket_new(&socket2, test_setup_parser(), "lo", s_eth_frame_cb, &cb_data);
    assert_true(ok);
    test_util_handle_events();

    // WHEN an ethernet packet is sent from the first socket
    uint8_t buffer[] = ARP_PACKET;
    ok = arp_eth_socket_send(socket1, "00:00:00:00:00:00", &buffer, sizeof(buffer) - 1);
    test_util_handle_events();
    eventing_once(eventing);

    // THEN the packet was sent successfully, and was received by the second socket
    assert_true(ok);
    assert_int_equal(28, cb_data.received_frame.rdlen);
    assert_int_equal(0, memcmp(cb_data.received_frame.frame, ARP_PACKET, 28));

    // cleanup:
    arp_eth_socket_delete(&socket1);
    arp_eth_socket_delete(&socket2);
    eventing_delete(&eventing);
}
