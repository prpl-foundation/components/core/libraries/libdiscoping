/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_ndp_msg.h"
#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "ndp/ndp_msg.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>
#include <string.h>

static const uint8_t neighbor_sollicitation_packet[] = {
    135,                                                // Type = neighbor sollicitation
    0,                                                  // Code = 0
    0, 0,                                               // chucksum (will be cumputed by kernel)
    0, 0, 0, 0,                                         // Reserved
    0xfe, 0x80, 0, 0, 0, 0, 0, 0,                       // Target ipv6 address (first half)
    0x70, 0x31, 0x67, 0xca, 0x65, 0x0e, 0xc4, 0x30,     // Target ipv6 address (second half)
    1,                                                  // Option type = source link-layer address
    1,                                                  // Option length incl header = 1 times 8 octets.
    1, 2, 3, 4, 5, 6                                    // Option data = Source MAC address
};

static const uint8_t ping_packet[] = {
    0x80,                                           // type: echo (ping) request
    0,                                              // code
    0, 0,                                           // checksum
    0x37, 0x03,                                     // identifier
    0, 0,                                           // sequence
    0, 0, 0, 0, 0, 0, 0, 0,                         // data
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // data
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // data
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // data
};

static const uint8_t neighbor_advertisement_packet[] = {
    0x88, 0x00, 0xfa, 0xf5, 0x60, 0x00, 0x00, 0x00,
    0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x70, 0x31, 0x67, 0xca, 0x65, 0x0e, 0xc4, 0x30,
    0x02, 0x01, 0x9c, 0xeb, 0xe8, 0x99, 0x61, 0x1c
};

static void s_testhelper_get_invalid_option(uint32_t options_length_in_bytes, uint8_t first_option_length_in_8octets) {
    // GIVEN a list of 1 option, that claims its length is `first_option_length_in_8octets`
    uint8_t options[512] = {};
    assert_true(sizeof(options) >= options_length_in_bytes);
    assert_true(sizeof(options) >= sizeof(struct nd_opt_hdr));
    struct nd_opt_hdr* option_header = (struct nd_opt_hdr*) options;
    option_header->nd_opt_type = 1;
    option_header->nd_opt_len = first_option_length_in_8octets;

    // WHEN getting the option
    uint32_t option_length_in_bytes = 0;
    const uint8_t* option = ndp_msg_get_option(1, &option_length_in_bytes, options, options_length_in_bytes);

    // THEN getting the option returned an error and valgrind is happy
    assert_null(option);
}

void test_ndp_msg__get_option__option_too_large(UNUSED void** state) {
    // Case: option claims its size is bigger than the buffer
    s_testhelper_get_invalid_option(20, 3);
}

void test_ndp_msg__get_option__option_size_zero(UNUSED void** state) {
    // Case: option with size 0
    s_testhelper_get_invalid_option(20, 0);
}

void test_ndp_msg__get_option__optionlist_size_too_small(UNUSED void** state) {
    s_testhelper_get_invalid_option(0, 1);
    s_testhelper_get_invalid_option(1, 1);
    s_testhelper_get_invalid_option(2, 1);
}
typedef struct {
    uint8_t option1_type;
    uint8_t option1_8octets;
    uint8_t option1_data[6];
    uint8_t option2_type;
    uint8_t option2_8octets;
    uint8_t option2_data[22];
    uint8_t option3_type;
    uint8_t option3_8octets;
    uint8_t option3_data[14];
} many_options_t;

void test_ndp_msg__get_option__normalcase(UNUSED void** state) {
    // GIVEN some options
    many_options_t options = {
        .option1_type = 7,
        .option1_8octets = 1,
        .option1_data = {2, 3, 4, 5, 6, 7},
        .option2_type = 85,
        .option2_8octets = 3,
        .option2_data = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
        .option3_type = 93,
        .option3_8octets = 2,
        .option3_data = {40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53},
    };

    // WHEN searching an option that exists in this list of options
    uint32_t option_size = 0;
    const uint8_t* option = ndp_msg_get_option(85, &option_size, (uint8_t*) &options, sizeof(options));

    // THEN the correct option is found
    assert_ptr_equal(option, &options.option2_data);
    assert_int_equal(option_size, 22);
}

void test_ndp_msg__get_option__not_found(UNUSED void** state) {
    // GIVEN some options
    many_options_t options = {
        .option1_type = 7,
        .option1_8octets = 1,
        .option1_data = {1, 2, 3, 4, 5, 6},
        .option2_type = 85,
        .option2_8octets = 3,
        .option2_data = {1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 18, 21, 22, 23, 24, 25, 26},
        .option3_type = 3,
        .option3_8octets = 2,
        .option3_data = {1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16},
    };

    // WHEN searching an option that does not exist in this list of options
    uint32_t option_size = 0;
    const uint8_t* option = ndp_msg_get_option(123, &option_size, (uint8_t*) &options, sizeof(options));

    // THEN the no option was found
    assert_null(option);
    assert_int_equal(option_size, 0);
}

void test_ndp_msg__get_option__last_one_not_enough_space(UNUSED void** state) {
    // GIVEN some options
    many_options_t options = {
        .option1_type = 7,
        .option1_8octets = 1,
        .option1_data = {1, 2, 3, 4, 5, 6},
        .option2_type = 85,
        .option2_8octets = 3,
        .option2_data = {1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 18, 21, 22, 23, 24, 25, 26},
        .option3_type = 3,
        .option3_8octets = 2,
        .option3_data = {1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16},
    };

    // WHEN searching an option, but the optionlist is not long enough
    uint32_t option_size = 0;
    const uint8_t* option = ndp_msg_get_option(3, &option_size, (uint8_t*) &options, sizeof(options) - 1);

    // THEN the no option was found
    assert_null(option);
    assert_int_equal(option_size, 0);
}

void test_ndp_msg__get_option__invalid_argument(UNUSED void** state) {
    uint32_t option_size = 9999;
    many_options_t options = {
        .option1_type = 7,
        .option1_8octets = 1,
        .option1_data = {2, 3, 4, 5, 6, 7},
        .option2_type = 85,
        .option2_8octets = 3,
        .option2_data = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
        .option3_type = 93,
        .option3_8octets = 2,
        .option3_data = {40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53},
    };

    // Case: NULL option size
    assert_null(ndp_msg_get_option(85, NULL, (uint8_t*) &options, sizeof(options)));

    // Case: NULL option list
    assert_null(ndp_msg_get_option(85, &option_size, NULL, sizeof(options)));
    assert_int_equal(option_size, 0);
}


void test_ndp_msg_make_solicited_node_multicast_address__normal_case(UNUSED void** state) {
    // GIVEN an ipv6 address
    uint8_t ipv6_address[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

    // WHEN turning that into a solicited node multicast address
    ndp_msg_make_solicited_node_multicast_address(ipv6_address);

    // THEN the address least-significant 24 bits are kept, and the prefix ff02::1:ff00:0/104 is added
    uint8_t expected[16] = {0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0xff, 14, 15, 16};
    assert_true(0 == memcmp(ipv6_address, expected, 16));
}


void test_ndp_msg_make_solicited_node_multicast_address__invalid_argument(UNUSED void** state) {
    // WHEN turning a NULLpointer into a solicited node multicast address
    ndp_msg_make_solicited_node_multicast_address(NULL);

    // THEN nothing happens.
}

void test_ndp_msg_solicit_serialize__normal_case(UNUSED void** state) {
    // GIVEN an target ip and a source mac
    const char* target_ip = "fe80::7031:67ca:650e:c430";
    const char* source_mac = "01:02:03:04:05:06";

    // WHEN serializing an NDP neighbor sollicitation packet from this mac to that IP
    ndp_msg_solicit_packet_t actual_msg;
    bool ok = ndp_msg_solicit_serialize(&actual_msg, target_ip, source_mac);

    // THEN the serialization is the same as what we expect when hand-crafting that packet
    assert_int_equal(sizeof(neighbor_sollicitation_packet), sizeof(actual_msg));
    assert_true(0 == memcmp(&actual_msg, neighbor_sollicitation_packet, sizeof(actual_msg)));
    assert_true(ok);
}

void test_ndp_msg_solicit_serialize__invalid_args(UNUSED void** state) {
    ndp_msg_solicit_packet_t msg;
    const char* target_ip = "fe80::7031:67ca:650e:c430";
    const char* source_mac = "01:02:03:04:05:06";

    // Case: NULL IP
    assert_false(ndp_msg_solicit_serialize(&msg, NULL, source_mac));

    // Case: NULL MAC
    assert_false(ndp_msg_solicit_serialize(&msg, target_ip, NULL));

    // Case: NULL msg
    assert_false(ndp_msg_solicit_serialize(NULL, target_ip, source_mac));

    // Case: ipv4 address
    assert_false(ndp_msg_solicit_serialize(&msg, "192.168.1.123", source_mac));

    // Case: empty IP address
    assert_false(ndp_msg_solicit_serialize(&msg, "", source_mac));

    // Case: nonsense IP address
    assert_false(ndp_msg_solicit_serialize(&msg, "hello", source_mac));

    // Case: too short IP address
    assert_false(ndp_msg_solicit_serialize(&msg, "fe80:1234", source_mac));

    // Case: empty MAC address
    assert_false(ndp_msg_solicit_serialize(&msg, target_ip, ""));

    // Case: nonsense MAC address
    assert_false(ndp_msg_solicit_serialize(&msg, target_ip, "hello"));

    // Case: too short MAC address
    assert_false(ndp_msg_solicit_serialize(&msg, target_ip, "01:02:03"));
}

void test_ndp_msg_advert_parse__normal_case(UNUSED void** state) {
    // GIVEN an ICMPv6 Neighbor Advertisement packet
    uint8_t packet[] = {
        0x88, 0x00, 0xfa, 0xf5, 0x60, 0x00, 0x00, 0x00,
        0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x70, 0x31, 0x67, 0xca, 0x65, 0x0e, 0xc4, 0x30,
        0x02, 0x01, 0x9c, 0xeb, 0xe8, 0x99, 0x61, 0x1c
    };

    // WHEN deserializing this packet
    char actual_ipv6_addr[INET6_ADDRSTRLEN];
    char actual_mac[18];
    bool ok = ndp_msg_advert_parse(actual_ipv6_addr, actual_mac, packet, sizeof(packet));

    // THEN the IP<->MAC mapping was successfully extracted from the packet
    assert_string_equal("fe80::7031:67ca:650e:c430", actual_ipv6_addr);
    assert_string_equal("9C:EB:E8:99:61:1C", actual_mac);
    assert_true(ok);
}

void test_ndp_msg_advert_parse__too_short(UNUSED void** state) {
    // GIVEN an ICMPv6 Neighbor Advertisement packet
    const uint8_t* packet = neighbor_advertisement_packet;
    const size_t packet_size = sizeof(neighbor_advertisement_packet);

    // WHEN parsing only part of the packet
    // THEN parsing reported to have failed
    char actual_ipv6_addr[INET6_ADDRSTRLEN];
    char actual_mac[18];
    assert_false(ndp_msg_advert_parse(actual_ipv6_addr, actual_mac, packet, packet_size - 1));
    assert_false(ndp_msg_advert_parse(actual_ipv6_addr, actual_mac, packet, packet_size - 6));
    assert_false(ndp_msg_advert_parse(actual_ipv6_addr, actual_mac, packet, packet_size - 9));
    assert_false(ndp_msg_advert_parse(actual_ipv6_addr, actual_mac, packet, 8));
    assert_false(ndp_msg_advert_parse(actual_ipv6_addr, actual_mac, packet, 9));
}

void test_ndp_msg_advert_parse__wrong_type(UNUSED void** state) {
    // GIVEN a neghbor sollicitation packet (so not a neighbor advertisement)
    const uint8_t* packet = neighbor_sollicitation_packet;
    const size_t packet_size = sizeof(neighbor_sollicitation_packet);

    // WHEN parsing
    char actual_ipv6_addr[INET6_ADDRSTRLEN];
    char actual_mac[18];
    bool ok = ndp_msg_advert_parse(actual_ipv6_addr, actual_mac, packet, packet_size);

    // THEN parsing reported to have failed
    assert_false(ok);
}

void ndp_msg_echo_serialize__normal_case(UNUSED void** state) {
    // GIVEN a to-be-filled-in echo packet
    ndp_msg_echo_packet_t actual_packet;
    memset(&actual_packet, 0b10101010, sizeof(ndp_msg_echo_packet_t));

    // WHEN filling in the echo packet
    ndp_msg_echo_serialize(&actual_packet);

    // THEN the echo packet's contents is as expected
    assert_int_equal(64, sizeof(ndp_msg_echo_packet_t));
    assert_int_equal(64, sizeof(actual_packet));
    assert_int_equal(64, sizeof(ping_packet));
    assert_int_equal(ARRAY_SIZE(ping_packet), sizeof(actual_packet));
    assert_memory_equal(&actual_packet, ping_packet, sizeof(ping_packet));
}

void ndp_msg_echo_serialize__invalid_argument(UNUSED void** state) {
    ndp_msg_echo_serialize(NULL);
}
