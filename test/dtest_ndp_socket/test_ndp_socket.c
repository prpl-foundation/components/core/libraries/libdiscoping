/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_ndp_socket.h"
#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "ndp/ndp_socket_priv.h"
#include "discoping/discoping_socketlist.h"
#include "util.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>
#include <string.h>
#include "../common/mock_socket.h"
#include "../common/mock_socketlist.h"

#include <netinet/icmp6.h>
#include <fcntl.h>

typedef int32_t bitfield_t;

static const uint8_t neighbor_advertisement_packet[] = {
    0x88, 0x00, 0xfa, 0xf5, 0x60, 0x00, 0x00, 0x00,
    0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
    0x02, 0x01, 0xc2, 0xd3, 0xe4, 0xf5, 0x06, 0x17
};

uint8_t neighbor_sollicitation_packet[] = {
    135,                                                // Type = neighbor sollicitation
    0,                                                  // Code = 0
    0, 0,                                               // chucksum (will be cumputed by kernel)
    0, 0, 0, 0,                                         // Reserved
    0xfe, 0x80, 0, 0, 0, 0, 0, 0,                       // Target ipv6 address (first half)
    0xaa, 0xaa, 0xbb, 0xbb, 0xcc, 0xcc, 0xdd, 0xd1,     // Target ipv6 address (second half)
    1,                                                  // Option type = source link-layer address
    1,                                                  // Option length incl header = 1 times 8 octets.
    0x11, 0x22, 0x33, 0x44, 0x55, 0x66                  // Option data = Source MAC address
};

static const uint8_t ping_packet[] = {
    0x80,                                           // type: echo (ping) request
    0,                                              // code
    0, 0,                                           // checksum
    0x37, 0x03,                                     // identifier
    0, 0,                                           // sequence
    0, 0, 0, 0, 0, 0, 0, 0,                         // data
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // data
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // data
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // data
};

/**
 * Calculate return value that a mocked function will return
 *
 * `fails` is a bitfield. Each bit correspond to an action, it's 1 when it must fails, 0 when succeed.
 * `action_nb` is the how manyth action is currently running, starting from 0.
 * So if action_nb is 4, and the 5th bit of `soft_fails` is 1, then this function return a value
 * that indicates error, otherwise a value that indicates success.
 *
 * This function automatically increases the action_nb.
 */
static int s_retval(bitfield_t fails, int32_t* action_nb) {
    bool should_fail = (fails & (1 << *action_nb)) != 0;
    (*action_nb)++;
    return should_fail ? -1 : 0;
}

static void s_testhelper_new_socket_ext(ndp_socket_t** socket,
                                        amxo_fd_read_t* target_reader,
                                        void** target_priv,
                                        discoping_socketlist_t* socketlist, bitfield_t soft_fails,
                                        int32_t hard_fail_action_nb) {

    bool ok = false;
    int32_t soft_action_nb = 0;
    amxo_connection_add_mock_data_t con_add_mockdata = {
        .retval = 0,
        .expected_parser = test_setup_parser(),
        .expected_fd = 7,
        .expected_uri = NULL,
        .expected_type = AMXO_CUSTOM,
    };

    // GIVEN a socketlist
    assert_non_null(socketlist);

    // EXPECT socket will be opened with a lot of options set
    mock_socket_set_enabled(true);
    expect_socket(hard_fail_action_nb == 0 ? -1 : 7, PF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
    if(hard_fail_action_nb > 0) {
        expect_setsockopt(hard_fail_action_nb == 1 ? -1 : 0, 7, SOL_SOCKET, SO_BINDTODEVICE);
    }
    if(hard_fail_action_nb > 1) {
        expect_bind(s_retval(soft_fails, &soft_action_nb), 7);
        expect_setsockopt(s_retval(soft_fails, &soft_action_nb), 7, IPPROTO_IPV6, IPV6_JOIN_GROUP);
        expect_setsockopt(s_retval(soft_fails, &soft_action_nb), 7, IPPROTO_ICMPV6, ICMP6_FILTER);
        expect_setsockopt(s_retval(soft_fails, &soft_action_nb), 7, SOL_SOCKET, SO_DONTROUTE);
        expect_setsockopt(s_retval(soft_fails, &soft_action_nb), 7, IPPROTO_IPV6, IPV6_MULTICAST_HOPS);
        expect_setsockopt(s_retval(soft_fails, &soft_action_nb), 7, IPPROTO_IPV6, IPV6_UNICAST_HOPS);
        expect_setsockopt(s_retval(soft_fails, &soft_action_nb), 7, IPPROTO_ICMPV6, IPV6_ADDR_PREFERENCES);
        expect_fcntl_void(hard_fail_action_nb == 2 ? -1 : 13, 7, F_GETFL);
    }
    if(hard_fail_action_nb > 2) {
        expect_fcntl_int(hard_fail_action_nb == 3 ? -1 : 0, 7, F_SETFL, 13 | O_NONBLOCK);
    }
    if(hard_fail_action_nb > 3) {
        con_add_mockdata.retval = hard_fail_action_nb == 4 ? -1 : 0;
        expect_amxo_connection_add(&con_add_mockdata);
    }

    // WHEN creating an NDP socket
    ok = ndp_socket_new(socket, test_setup_parser(), "netdevname1", "fe80::1234:5678:9abc:def0", discoping_socketlist_mark_seen_network);
    ndp_socket_set_userdata((ndp_socket_t**) socket, socketlist);

    if(hard_fail_action_nb == INT32_MAX) {
        // THEN creation of the NDP socket did not fail
        assert_non_null(*socket);
        assert_true(ok);
    } else {
        // THEN creation of the NDP socket failed
        assert_null(*socket);
        assert_false(ok);
    }


    if(target_reader != NULL) {
        *target_reader = con_add_mockdata.reader;
    }
    if(target_priv != NULL) {
        *target_priv = con_add_mockdata.priv;
    }
}

/**
 * @param soft_fails bitfield, nth bit is 1 if the nth action that is allowed to fail, fails
 * @param hard_fail_action_nb number of action that is not allowed to fail that fails, INT32_MAX if none.
 */
static void s_testhelper_new_socket(bitfield_t soft_fails, int32_t hard_fail_action_nb) {
    ndp_socket_t* socket = NULL;
    assert_true(hard_fail_action_nb >= 0); // Use INT32_MAX for no hard failure.

    // GIVEN a socketlist
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());
    mock_socket_set_enabled(true);
    assert_non_null(socketlist);

    // EXPECT socket will be opened with a lot of options set
    //        with failures as per soft_fails and hard_fail_action_nb
    // WHEN creating an NDP socket
    // THEN it failed if and only if no socket action failed that was not allowed to fail
    s_testhelper_new_socket_ext(&socket, NULL, NULL, socketlist, soft_fails, hard_fail_action_nb);

    // cleanup
    ndp_socket_delete(&socket);
    discoping_socketlist_delete(&socketlist);
    mock_socket_set_enabled(false);
}

void test_ndp_socket_new__normal_case(UNUSED void** state) {
    // EXPECT socket will be opened with a lot of options set, all succeeding
    // WHEN creating an NDP socket
    // THEN the creation of the NDP socket succeeded
    s_testhelper_new_socket(0, INT32_MAX);
}

void test_ndp_socket_new__invalid_args(UNUSED void** state) {
    ndp_socket_t* ndp_socket = NULL;
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());

    // Case: NULL ndp socket pointer
    assert_false(ndp_socket_new(NULL, test_setup_parser(), "netdevname1", "fe80::1234:5678:9abc:def0", discoping_socketlist_mark_seen_network));

    // Case: NULL parser
    assert_false(ndp_socket_new(&ndp_socket, NULL, "netdevname1", "fe80::1234:5678:9abc:def0", discoping_socketlist_mark_seen_network));
    assert_null(ndp_socket);

    // Case: NULL netdevname
    assert_false(ndp_socket_new(&ndp_socket, test_setup_parser(), NULL, "fe80::1234:5678:9abc:def0", discoping_socketlist_mark_seen_network));
    assert_null(ndp_socket);

    // Case: Empty netdevname
    assert_false(ndp_socket_new(&ndp_socket, test_setup_parser(), "", "fe80::1234:5678:9abc:def0", discoping_socketlist_mark_seen_network));
    assert_null(ndp_socket);

    // Case: NULL ip
    assert_false(ndp_socket_new(&ndp_socket, test_setup_parser(), "netdevname1", NULL, discoping_socketlist_mark_seen_network));
    assert_null(ndp_socket);

    // Case: Empty IP
    assert_false(ndp_socket_new(&ndp_socket, test_setup_parser(), "netdevname1", "", discoping_socketlist_mark_seen_network));
    assert_null(ndp_socket);

    // Case: NULL cb
    assert_false(ndp_socket_new(&ndp_socket, test_setup_parser(), "netdevname1", "fe80::1234:5678:9abc:def0", NULL));
    assert_null(ndp_socket);

    discoping_socketlist_delete(&socketlist);
}

void test_ndp_socket_new__soft_fail_one(UNUSED void** state) {
    for(uint32_t which_action_fails = 0; which_action_fails < 4; which_action_fails++) {
        bitfield_t soft_fails = 1 << which_action_fails;

        // EXPECT socket will be opened with a lot of options set, and only one (that is allowed to
        //        fail) fails
        // WHEN creating an NDP socket
        // THEN the creation of the NDP socket failed
        s_testhelper_new_socket(soft_fails, INT32_MAX);
    }
}

void test_ndp_socket_new__soft_fail_all(UNUSED void** state) {
    // GIVEN a socketlist
    // EXPECT socket will be opened with a lot of options set, and all that are allowed to fail fail
    // WHEN creating an NDP socket
    // THEN the creation of the NDP socket failed
    bitfield_t soft_fails = 0b11111111111111111111111111111111;
    s_testhelper_new_socket(soft_fails, INT32_MAX);
}

/** hard failures are separate test functions to avoid cmocka's expect of one test to leak into the next */
void test_ndp_socket_new__fail_open_socket(UNUSED void** state) {
    // GIVEN a socketlist
    // EXPECT socket opening will fail at the very start
    // WHEN creating an NDP socket
    // THEN the creation of the NDP socket failed
    s_testhelper_new_socket(0, 0);
}

/** hard failures are separate test functions to avoid cmocka's expect of one test to leak into the next */
void test_ndp_socket_new__fail_setsockopt_bind(UNUSED void** state) {
    // GIVEN a socketlist
    // EXPECT socket will be opened with a lot of options set, and all succeed except the call
    //        to setsockopt to bind the socket
    // WHEN creating an NDP socket
    // THEN the creation of the NDP socket failed
    s_testhelper_new_socket(0, 1);
}

/** hard failures are separate test functions to avoid cmocka's expect of one test to leak into the next */
void test_ndp_socket_new__fail_fcntl_get(UNUSED void** state) {
    // GIVEN a socketlist
    // EXPECT socket will be opened with a lot of options set, and all succeed except the call
    //        to fcntl with command F_GETFL.
    // WHEN creating an NDP socket
    // THEN the creation of the NDP socket failed
    s_testhelper_new_socket(0, 2);
}

/** hard failures are separate test functions to avoid cmocka's expect of one test to leak into the next */
void test_ndp_socket_new__fail_fcntl_set(UNUSED void** state) {
    // GIVEN a socketlist
    // EXPECT socket will be opened with a lot of options set, and all succeed except the call
    //        to fcntl with command F_SETFL.
    // WHEN creating an NDP socket
    // THEN the creation of the NDP socket failed
    s_testhelper_new_socket(0, 3);
}

/** hard failures are separate test functions to avoid cmocka's expect of one test to leak into the next */
void test_ndp_socket_new__fail_add_amxo_connection(UNUSED void** state) {
    // GIVEN a socketlist
    // EXPECT socket will be opened and configured and everything will succeed except amxo_connection_add
    // WHEN creating an NDP socket
    // THEN the creation of the NDP socket failed
    s_testhelper_new_socket(0, 4);
}

static void s_testhelper_ndp_socket__new_message(recv_mock_data_t* recv_mockdata, const char* expect_ip, const char* expect_mac) {
    ndp_socket_t* socket = NULL;
    amxo_fd_read_t reader = NULL;
    void* priv = NULL;

    // GIVEN a socket
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());
    mock_socket_set_enabled(true);
    s_testhelper_new_socket_ext(&socket, &reader, &priv, socketlist, 0, INT32_MAX);
    assert_non_null(reader);

    // EXPECT socket will be read
    expect_recv(recv_mockdata);


    if(expect_ip != NULL) {
        // EXPECT the ip&mac will be reported as seen on the network
        expect_socketlist_mark_seen_network(expect_ip, expect_mac);
    } else {
        // EXPECT no mac&ip will be reported as seen on the network.
    }
    assert_true((expect_ip == NULL) == (expect_mac == NULL));

    // WHEN a proper neighbor advertisement is received
    reader(7, priv);

    // cleanup
    ndp_socket_delete(&socket);
    discoping_socketlist_delete(&socketlist);
    mock_socket_set_enabled(false);
}

void test_ndp_socket__receive_normalcase(UNUSED void** state) {
    recv_mock_data_t recv_mockdata = {
        .retval = ARRAY_SIZE(neighbor_advertisement_packet),
        .data = neighbor_advertisement_packet,
        .data_len = ARRAY_SIZE(neighbor_advertisement_packet),
        .expected_fd = 7,
        .expected_flags = MSG_DONTWAIT,
    };
    s_testhelper_ndp_socket__new_message(&recv_mockdata, "fe80::123:4567:89ab:cdef", "C2:D3:E4:F5:06:17");
}

void test_ndp_socket__receive_too_short(UNUSED void** state) {
    recv_mock_data_t recv_mockdata = {
        .retval = ARRAY_SIZE(neighbor_advertisement_packet) - 1,  // <-- too short
        .data = neighbor_advertisement_packet,
        .data_len = ARRAY_SIZE(neighbor_advertisement_packet),
        .expected_fd = 7,
        .expected_flags = MSG_DONTWAIT,
    };
    s_testhelper_ndp_socket__new_message(&recv_mockdata, NULL, NULL);
}

void test_ndp_socket__receive_empty(UNUSED void** state) {
    recv_mock_data_t recv_mockdata = {
        .retval = 0, // <-- empty
        .data = neighbor_advertisement_packet,
        .data_len = ARRAY_SIZE(neighbor_advertisement_packet),
        .expected_fd = 7,
        .expected_flags = MSG_DONTWAIT,
    };
    s_testhelper_ndp_socket__new_message(&recv_mockdata, NULL, NULL);
}

void test_ndp_socket__receive_nonsense(UNUSED void** state) {
    const char* nonsense = "Hello, this is nonsense speaking";
    assert_true(strlen(nonsense) == ARRAY_SIZE(neighbor_advertisement_packet)); // <-- right size
    recv_mock_data_t recv_mockdata = {
        .retval = ARRAY_SIZE(neighbor_advertisement_packet),
        .data = nonsense,
        .data_len = strlen(nonsense) + 1,
        .expected_fd = 7,
        .expected_flags = MSG_DONTWAIT,
    };
    s_testhelper_ndp_socket__new_message(&recv_mockdata, NULL, NULL);
}

void test_ndp_socket__receive_failure(UNUSED void** state) {
    recv_mock_data_t recv_mockdata = {
        .retval = -1,
        .data = NULL,
        .data_len = 0,
        .expected_fd = 7,
        .expected_flags = MSG_DONTWAIT,
    };
    s_testhelper_ndp_socket__new_message(&recv_mockdata, NULL, NULL);
}

void test_ndp_socket_send_request__normal_case(UNUSED void** state) {
    // GIVEN an NDP socket
    ndp_socket_t* socket = NULL;
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());
    mock_socket_set_enabled(true);
    s_testhelper_new_socket_ext(&socket, NULL, NULL, socketlist, 0, INT32_MAX);

    // EXPECT message will be sent
    expect_sendto(ARRAY_SIZE(neighbor_sollicitation_packet), 7, neighbor_sollicitation_packet, ARRAY_SIZE(neighbor_sollicitation_packet), 0);

    // WHEN sending a message
    ndp_socket_impl.send_request(socket, "fe80::123:4567:89ab:cdef", "fe80::aaaa:bbbb:cccc:ddd1", "11:22:33:44:55:66");

    // cleanup
    ndp_socket_delete(&socket);
    discoping_socketlist_delete(&socketlist);
}

void test_ndp_socket_send_request__error(UNUSED void** state) {
    // GIVEN an NDP socket
    ndp_socket_t* socket = NULL;
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());
    mock_socket_set_enabled(true);
    s_testhelper_new_socket_ext(&socket, NULL, NULL, socketlist, 0, INT32_MAX);

    // EXPECT sending message will be attempted but will fail
    expect_sendto(-1, 7, neighbor_sollicitation_packet, ARRAY_SIZE(neighbor_sollicitation_packet), 0);

    // WHEN sending a message
    ndp_socket_impl.send_request(socket, "fe80::123:4567:89ab:cdef", "fe80::aaaa:bbbb:cccc:ddd1", "11:22:33:44:55:66");

    // cleanup
    ndp_socket_delete(&socket);
    discoping_socketlist_delete(&socketlist);
}

void test_ndp_socket_send_request__incomplete(UNUSED void** state) {
    // GIVEN an NDP socket
    ndp_socket_t* socket = NULL;
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());
    mock_socket_set_enabled(true);
    s_testhelper_new_socket_ext(&socket, NULL, NULL, socketlist, 0, INT32_MAX);

    // EXPECT sending message will be attempted but will be incomplete
    expect_sendto(ARRAY_SIZE(neighbor_sollicitation_packet) - 1, 7, neighbor_sollicitation_packet, ARRAY_SIZE(neighbor_sollicitation_packet), 0);

    // WHEN sending a message
    ndp_socket_impl.send_request(socket, "fe80::123:4567:89ab:cdef", "fe80::aaaa:bbbb:cccc:ddd1", "11:22:33:44:55:66");

    // cleanup
    ndp_socket_delete(&socket);
    discoping_socketlist_delete(&socketlist);
}

void test_ndp_socket_scan__normal_case(UNUSED void** state) {
    // GIVEN an NDP socket
    ndp_socket_t* socket = NULL;
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());
    mock_socket_set_enabled(true);
    s_testhelper_new_socket_ext(&socket, NULL, NULL, socketlist, 0, INT32_MAX);

    // EXPECT icmpv6 echo request will be sent
    expect_sendto(ARRAY_SIZE(ping_packet), 7, ping_packet, ARRAY_SIZE(ping_packet), 0);

    // WHEN performing a scan
    ndp_socket_impl.scan(socket);

    // cleanup
    ndp_socket_delete(&socket);
    discoping_socketlist_delete(&socketlist);
}

void test_ndp_socket_scan__invalid_argument(UNUSED void** state) {
    ndp_socket_impl.scan(NULL);
}