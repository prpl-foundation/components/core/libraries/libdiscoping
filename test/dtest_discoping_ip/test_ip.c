/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_ip.h"
#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "discoping/discoping_ip.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>
#include <string.h>

void test_discoping_ip_char__no_htable(UNUSED void** state) {
    // GIVEN an ip that is not placed in a htable
    discoping_ip_t* ip = NULL;
    discoping_ip_new(&ip);

    // WHEN getting the character representation of the IP address
    const char* ip_char = discoping_ip_char(ip);

    // THEN it is NULL
    assert_null(ip_char);

    // cleanup:
    discoping_ip_delete(&ip);
}

void test_discoping_ip_char__normalcase(UNUSED void** state) {
    // GIVEN an ip that is placed in a htable
    amxc_htable_t htable;
    amxc_htable_init(&htable, 1);
    discoping_ip_t* ip = NULL;
    discoping_ip_new(&ip);
    amxc_htable_insert(&htable, "192.168.1.1", &ip->it);

    // WHEN getting the character representation of the IP address
    const char* ip_char = discoping_ip_char(ip);

    // THEN it is the key
    assert_non_null(ip_char);
    assert_string_equal("192.168.1.1", ip_char);

    // cleanup:
    amxc_htable_clean(&htable, NULL);
    discoping_ip_delete(&ip);
}

void test_discoping_ip_char__invalid_args(UNUSED void** state) {
    // Case: NULL argument
    assert_null(discoping_ip_char(NULL));
}

void test_discoping_ip_get_periodic_reverify_enabled__after_construction(UNUSED void** state) {
    // GIVEN nothing

    // WHEN constructing a new ip
    discoping_ip_t* ip = NULL;
    discoping_ip_new(&ip);
    assert_non_null(ip);

    // THEN periodic-verify is enabled
    assert_true(discoping_ip_get_periodic_reverify_enabled(ip));

    // cleanup
    discoping_ip_delete(&ip);
}

void test_discoping_ip_get_periodic_reverify_enabled__after_set(UNUSED void** state) {
    // GIVEN an IP without periodic-reverify
    discoping_ip_t* ip = NULL;
    discoping_ip_new(&ip);
    assert_non_null(ip);
    assert_false(discoping_ip_get_periodic_reverify_enabled(ip));

    // WHEN enabling periodic-reverify
    discoping_ip_set_periodic_reverify_enabled(ip, true);

    // THEN periodic-verify is enabled
    assert_true(discoping_ip_get_periodic_reverify_enabled(ip));

    // cleanup
    discoping_ip_delete(&ip);
}

void test_discoping_ip_get_periodic_reverify_enabled__after_unset(UNUSED void** state) {
    // GIVEN an IP with periodic-reverify
    discoping_ip_t* ip = NULL;
    discoping_ip_new(&ip);
    assert_non_null(ip);
    discoping_ip_set_periodic_reverify_enabled(ip, true);
    assert_true(discoping_ip_get_periodic_reverify_enabled(ip));

    // WHEN disabling periodic-reverify
    discoping_ip_set_periodic_reverify_enabled(ip, false);

    // THEN periodic-verify is disabled
    assert_false(discoping_ip_get_periodic_reverify_enabled(ip));

    // cleanup
    discoping_ip_delete(&ip);
}

void test_discoping_ip_get_periodic_reverify_enabled__invalid_args(UNUSED void** state) {
    // Case: NULL
    assert_false(discoping_ip_get_periodic_reverify_enabled(NULL));
}

void test_discoping_ip_set_periodic_reverify_enabled__invalid_args(UNUSED void** state) {
    // Case: NULL
    discoping_ip_set_periodic_reverify_enabled(NULL, false);
}
