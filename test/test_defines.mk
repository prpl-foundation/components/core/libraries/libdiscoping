MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv)
MOCK_SRCDIR = $(realpath ../common/)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) $(wildcard $(SRCDIR)/arp/*.c) $(wildcard $(SRCDIR)/ndp/*.c) $(wildcard $(SRCDIR)/discoping/*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu11 -g3 -Wmissing-declarations \
          -Wshadow \
          -Wwrite-strings -Wredundant-decls \
          -Wno-format-nonliteral \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxp -lamxd -lamxo -lamxb \
		   -lsahtrace \
		   -ldl -lpthread \
		   -levent \
		   -lamxut

WRAP_FUNC=-Wl,--wrap=

MOCK_WRAP +=

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
