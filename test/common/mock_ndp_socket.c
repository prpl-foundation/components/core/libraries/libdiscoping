/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_ndp_socket.h"
#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>
#include <amxc/amxc_macros.h>

bool __wrap_ndp_socket_new(ndp_socket_t** socket,
                           amxo_parser_t* parser,
                           const char* netdev_name,
                           const char* ip,
                           socket_common_reply_cb_t cb) {

    wrap_ndp_socket_t* wrap_socket = (wrap_ndp_socket_t*) mock();

    check_expected(parser);
    check_expected(netdev_name);
    check_expected(ip);
    check_expected(cb);

    if(wrap_socket != NULL) {
        *socket = (ndp_socket_t*) wrap_socket;
        return true;
    } else {
        return false;
    }
}

void mock_ndp_socket_expect_new(wrap_ndp_socket_t* socket,
                                amxo_parser_t* parser,
                                const char* netdev_name,
                                const char* ip,
                                socket_common_reply_cb_t cb) {
    will_return(__wrap_ndp_socket_new, socket);
    expect_value(__wrap_ndp_socket_new, parser, parser);
    expect_string(__wrap_ndp_socket_new, netdev_name, netdev_name);
    expect_string(__wrap_ndp_socket_new, ip, ip);
    expect_value(__wrap_ndp_socket_new, cb, cb);
}

void __wrap_ndp_socket_delete(ndp_socket_t** socket) {
    if((socket == NULL) || (*socket == NULL)) {
        return;
    }

    wrap_ndp_socket_t* wrap_socket = (wrap_ndp_socket_t*) *socket;
    assert_false(wrap_socket->freed);
    wrap_socket->freed = true;
}

void __wrap_ndp_socket_send_request(UNUSED ndp_socket_t* self,
                                    UNUSED const char* ip_from,
                                    UNUSED const char* ip_to,
                                    UNUSED const char* mac_from) {
    fail_msg("Mock not implemented because not needed yet. If you need it, you can implement it.");
}


void __wrap_ndp_socket_scan(ndp_socket_t* socket) {
    check_expected(socket);
    assert_false(((wrap_ndp_socket_t*) socket)->freed);
}

void __wrap_ndp_socket_set_userdata(ndp_socket_t* socket, void* userdata) {
    assert_non_null(socket);
    ((wrap_ndp_socket_t*) socket)->userdata = userdata;
}