/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_discoping_helpers.h"
#include "test-setup.h"
#include "test-util.h"
#include <amxut/amxut_timer.h>
#include "mock_socketlist.h"
#include "discoping/discoping_priv.h"
#include "arp/arp_socket_priv.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>
#include <arpa/inet.h>
#include <string.h>

const arp_eth_socket_frame_t test_discoping_helpers_gratuitous_arp_eth_frame = {
    .rdlen = 46,
    .vlanId = 0,
    .address = {
        .sll_family = 17,
        .sll_protocol = 1544,
        .sll_ifindex = 19,
        .sll_hatype = 1,
        .sll_pkttype = 0,
        .sll_halen = 6,
        .sll_addr = {1, 2, 10, 11, 12, 13, 0, 0}
    },
    .frame = "\x00\x01"                // hardware type
        "\x08\x00"                     // protocol type
        "\x06"                         // hardware address length
        "\x04"                         // protocol address length
        "\x00\x02"                     // operation type
        "\x01\x02\x0a\x0b\x0c\x0d"     // sender hardware(mac) address
        "\xc0\xa8\x01\x65"             // sender protocol(ip) address
        "\x00\x00\x00\x00\x00\x00"     // target hardware(mac) address
        "\xc0\xa8\x01\x65"             // target protocol(ip) address
};

static void s_on_up_or_down(const char* ip, uint32_t family, const char* mac, void* userdata, test_discoping_helpers_cbtype_e updown) {
    test_discoping_helpers_cb_tracking_t* tracking = userdata;
    assert_non_null(tracking);
    assert_non_null(ip);

    assert_true((size_t) tracking->callcount < sizeof(tracking->calls) / sizeof(tracking->calls[0]));

    assert_int_equal(AF_INET, family);
    tracking->calls[tracking->callcount].mac = mac == NULL ? NULL : strdup(mac);
    tracking->calls[tracking->callcount].ip = strdup(ip);
    tracking->calls[tracking->callcount].up = updown;

    tracking->callcount++;

    if(tracking->eventing != NULL) {
        eventing_loopbreak(tracking->eventing);
    }
}

/** @implements discoping_up_cb_t */
void test_discoping_helpers_on_up_cb(const char* ip, uint32_t family, const char* mac, void* userdata) {
    s_on_up_or_down(ip, family, mac, userdata, TEST_ARPING_HELPERS_CB_TYPE_UP);
}

/** @implements discoping_down_cb_t */
void test_discoping_helpers_on_down_cb(const char* ip, uint32_t family, const char* mac, void* userdata) {
    s_on_up_or_down(ip, family, mac, userdata, TEST_ARPING_HELPERS_CB_TYPE_DOWN);
}

/** @implements discoping_collision_cb_t */
void test_discoping_helpers_on_collision_cb(const char* ip, uint32_t family,
                                            const amxc_var_t* dev_mac_list, void* userdata) {

    test_discoping_helpers_cb_tracking_t* tracking = userdata;
    assert_non_null(tracking);
    assert_non_null(ip);

    assert_true((size_t) tracking->callcount < sizeof(tracking->calls) / sizeof(tracking->calls[0]));

    assert_int_equal(AF_INET, family);

    tracking->calls[tracking->callcount].ip = strdup(ip);
    for(size_t i = 0; i < sizeof(tracking->calls[tracking->callcount].mac_collision) / sizeof(tracking->calls[tracking->callcount].mac_collision[0]); i++) {
        const char* mac = GETI_CHAR(dev_mac_list, i);
        if(mac == NULL) {
            tracking->calls[tracking->callcount].mac_collision[i] = NULL;
        } else {
            tracking->calls[tracking->callcount].mac_collision[i] = strdup(mac);
        }
    }

    tracking->calls[tracking->callcount].up = TEST_ARPING_HELPERS_CB_TYPE_COLLISION;

    tracking->callcount++;

    if(tracking->eventing != NULL) {
        eventing_loopbreak(tracking->eventing);
    }
}

void test_discoping_helpers_assert_cb(test_discoping_helpers_teststate_t* teststate, int32_t callcount,
                                      const char* ip, const char* mac, test_discoping_helpers_cbtype_e updown) {

    test_discoping_helpers_assert_cb_at(teststate, callcount - 1, callcount, ip, mac, updown);
}

void test_discoping_helpers_assert_cb_at(test_discoping_helpers_teststate_t* teststate, int32_t call_idx, int32_t total_callcount,
                                         const char* ip, const char* mac, test_discoping_helpers_cbtype_e updown) {


    assert_int_equal(total_callcount, teststate->cb_tracking.callcount);
    assert_true(call_idx < total_callcount);

    if(total_callcount != 0) {
        assert_int_equal(updown, teststate->cb_tracking.calls[call_idx].up);
        if(mac != NULL) {
            assert_string_equal(mac, teststate->cb_tracking.calls[call_idx].mac);
        } else {
            assert_null(teststate->cb_tracking.calls[call_idx].mac);
        }
        assert_non_null(ip);
        assert_non_null(teststate->cb_tracking.calls[call_idx].ip);
        assert_string_equal(ip, teststate->cb_tracking.calls[call_idx].ip);
    }

}

/** `a` contains case-insensitive `b` once */
static bool s_contains_once_uncased(const char* const* a, const char* b, size_t nb_elems) {
    size_t seen_count = 0;
    for(size_t i = 0; i < nb_elems; i++) {
        if((a[i] != NULL) && (0 == strcasecmp(a[i], b))) {
            seen_count++;
        }
    }
    return seen_count == 1;
}

/** `b` is a superset of `a` */
static bool s_superset_uncased(const char* const* a, const char* const* b, size_t nb_elems) {
    for(size_t i = 0; i < nb_elems; i++) {
        if((b[i] != NULL) && !s_contains_once_uncased(a, b[i], nb_elems)) {
            return false;
        }
    }
    return true;
}

static bool s_string_set_eq_uncased(const char* const* a, const char* const* b, size_t nb_elems) {
    return s_superset_uncased(a, b, nb_elems) && s_superset_uncased(b, a, nb_elems);
}

void test_discoping_helpers_assert_collision_cb(test_discoping_helpers_teststate_t* teststate,
                                                int32_t callcount, const char* ip,
                                                const char* mac1, const char* mac2, const char* mac3) {

    assert_true(callcount > 0);
    assert_int_equal(callcount, teststate->cb_tracking.callcount);
    size_t i = callcount - 1;

    const char* macs_expected[] = {mac1, mac2, mac3};
    size_t nb_macs_max = sizeof(teststate->cb_tracking.calls[i].mac_collision) / sizeof(teststate->cb_tracking.calls[i].mac_collision[0]);
    // if more macs, do not forget to adapt this function or make it more generic:
    assert_int_equal(nb_macs_max, 3);

    assert_string_equal(ip, teststate->cb_tracking.calls[i].ip);
    assert_int_equal(TEST_ARPING_HELPERS_CB_TYPE_COLLISION, teststate->cb_tracking.calls[i].up);

    assert_true(s_string_set_eq_uncased(macs_expected, (const char* const*) teststate->cb_tracking.calls[i].mac_collision, nb_macs_max));
}

void test_discoping_helpers_teststate_init_without_socket_open(test_discoping_helpers_teststate_t* teststate) {
    assert_true(discoping_new(&teststate->discoping, test_setup_parser()));
    discoping_set_on_up_cb(teststate->discoping, test_discoping_helpers_on_up_cb);
    discoping_set_on_down_cb(teststate->discoping, test_discoping_helpers_on_down_cb);
    discoping_set_on_collision_cb(teststate->discoping, test_discoping_helpers_on_collision_cb);
    discoping_set_userdata(teststate->discoping, &teststate->cb_tracking);
}

void test_discoping_helpers_open_arp_socket(test_discoping_helpers_teststate_t* teststate) {
    will_return(__wrap_arp_eth_socket_new, &(teststate->mock_eth_socket));
    expect_string(__wrap_arp_eth_socket_new, netdev_name, "mynetdev");
    expect_value(__wrap_arp_eth_socket_new, odl_parser, test_setup_parser());
    discoping_open(teststate->discoping, "mynetdev", "00:aa:bb:cc:dd:ee", "192.168.1.101");
}

void test_discoping_helpers_teststate_init(test_discoping_helpers_teststate_t* teststate) {
    test_discoping_helpers_teststate_init_without_socket_open(teststate);

    test_discoping_helpers_open_arp_socket(teststate);

    ignore_function_calls(__wrap_discoping_socketlist_scan);
}

void test_discoping_helpers_teststate_cleanup(test_discoping_helpers_teststate_t* teststate) {
    discoping_delete(&teststate->discoping);
    for(size_t call_idx = 0; call_idx < (size_t) teststate->cb_tracking.callcount; call_idx++) {
        free(teststate->cb_tracking.calls[call_idx].ip);
        teststate->cb_tracking.calls[call_idx].ip = NULL;
        free(teststate->cb_tracking.calls[call_idx].mac);
        teststate->cb_tracking.calls[call_idx].mac = NULL;
        for(size_t i = 0; i < sizeof(teststate->cb_tracking.calls[call_idx].mac_collision) / sizeof(teststate->cb_tracking.calls[call_idx].mac_collision[0]); i++) {
            free(teststate->cb_tracking.calls[call_idx].mac_collision[i]);
            teststate->cb_tracking.calls[call_idx].mac_collision[i] = NULL;
        }
    }
    teststate->cb_tracking.callcount = 0;
}

void test_discoping_helpers_receive_arp_reply(test_discoping_helpers_teststate_t* teststate, const char* ip, const char* mac) {
    arp_msg_t msg = {
        .ip_target = "10.254.14.142",
        .mac_target = "24:0e:e5:2d:5c:b1",
        .operation_type = ARP_MSG_OPERATION_TYPE_REPLY
    };
    assert_true(strlen(ip) <= sizeof(msg.ip_sender));
    strncpy(msg.ip_sender, ip, sizeof(msg.ip_sender));
    assert_true(strlen(mac) <= sizeof(msg.mac_sender));
    strncpy(msg.mac_sender, mac, sizeof(msg.mac_sender));
    arp_socket_internal_on_arp_msg(teststate->mock_eth_socket.arp_socket, &msg);
}