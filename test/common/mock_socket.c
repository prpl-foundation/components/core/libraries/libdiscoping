/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE // for RTLD_NEXT. Must be before includes.
#include "mock_socket.h"
#include <dlfcn.h>  // for dlsym()
#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>
#include <stdbool.h>

#include "util.h"
#include <net/if.h>
#include <string.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>
#include <netinet/icmp6.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <netdb.h>


/**
 * Whether mocking of socket-related functions is enabled.
 *
 * If false, the mock-functions forward the call to the real functions.
 */
static bool s_enabled = false;

/** Values for second argument of fnctl() that causes the third argument to be of type `int`: */
static const int fnctl_cmds_int[] = {
    F_ADD_SEALS,
    F_DUPFD_CLOEXEC,
    F_DUPFD,
    F_SETFL,
    F_SETLEASE,
    F_SETFD,
    F_NOTIFY,
    F_SETOWN,
    F_SETPIPE_SZ,
    F_SETSIG
};

/** Values for second argument of fnctl() that causes the third argument to be of pointer type: */
static const int fnctl_cmds_pointer[] = {
    F_GETLK,
    F_GETOWN_EX,
    F_OFD_GETLK,
    F_OFD_SETLK,
    F_OFD_SETLKW,
    F_GET_RW_HINT,
    F_SET_FILE_RW_HINT,
    F_SETLK,
    F_SETLKW,
    F_SETOWN_EX,
    F_SET_RW_HINT,
    F_GET_FILE_RW_HINT
};

/** Values for second argument of fnctl() that causes absence of third argument: */
static const int fnctl_cmds_void[] = {
    F_GETFD,
    F_GETFL,
    F_GETLEASE,
    F_GETOWN,
    F_GETPIPE_SZ,
    F_GET_SEALS,
    F_GETSIG
};

static bool s_array_contains(const int* haystack, size_t haystack_size, int needle) {
    for(size_t i = 0; i < haystack_size; i++) {
        if(haystack[i] == needle) {
            return true;
        }
    }
    return false;
}

/** mocked version of socket() */
int socket (int domain, int type, int protocol) {
    if(!s_enabled) {
        int (* real_socket)(int domain, int type, int protocol) = dlsym(RTLD_NEXT, "socket");
        return real_socket(domain, type, protocol);
    }
    check_expected(domain);
    check_expected(type);
    check_expected(protocol);
    return (int) mock();
}

void expect_socket(int retval, int domain, int type, int protocol) {
    expect_value(socket, domain, domain);
    expect_value(socket, type, type);
    expect_value(socket, protocol, protocol);
    will_return(socket, retval);
}

// /** mocked version of setsockopt() */
int setsockopt(int fd, int level, int optname, const void* optval, uint32_t optlen) {
    if(!s_enabled) {
        int (* real_setsockopt)(int fd, int level, int optname, const void* optval, uint32_t optlen) =
            dlsym(RTLD_NEXT, "setsockopt");
        return real_setsockopt(fd, level, optname, optval, optlen);
    }
    check_expected(fd);
    check_expected(level);
    check_expected(optname);
    return (int) mock();
}

void expect_setsockopt(int retval, int fd, int level, int optname) {
    expect_value(setsockopt, fd, fd);
    expect_value(setsockopt, level, level);
    expect_value(setsockopt, optname, optname);
    will_return(setsockopt, retval);
}


static int s_fcntl_mockwrap_int(int fd, int cmd, int third_argument) {
    check_expected(fd);
    check_expected(cmd);
    check_expected(third_argument);
    return (int) mock();
}

static int s_fcntl_mockwrap_void(int fd, int cmd) {
    check_expected(fd);
    check_expected(cmd);
    return (int) mock();
}

/**
 * mocked version of fcntl()
 */
int fcntl(int fd, int cmd, ... /* arg */) {
    va_list arguments;
    int returnvalue = -1;
    va_start(arguments, cmd);
    if(!s_enabled) {
        // There is no va_list-version of fcntl available, so we cannot do "normal" call forwarding
        // So workaround that:
        int (* real_fcntl)(int fd, int cmd, ...) = dlsym(RTLD_NEXT, "fcntl");
        if(s_array_contains(fnctl_cmds_int, ARRAY_SIZE(fnctl_cmds_int), cmd)) {
            int third_argument = va_arg(arguments, int);
            returnvalue = real_fcntl(fd, cmd, third_argument);
        } else if(s_array_contains(fnctl_cmds_pointer, ARRAY_SIZE(fnctl_cmds_pointer), cmd)) {
            void* third_argument = va_arg(arguments, void*);
            returnvalue = real_fcntl(fd, cmd, third_argument);
        } else if(s_array_contains(fnctl_cmds_void, ARRAY_SIZE(fnctl_cmds_void), cmd)) {
            returnvalue = real_fcntl(fd, cmd);
        } else {
            fail_msg("Unknown fcntl command: %i", cmd);
        }
    } else {
        // cmocka does not seem to support mocking with varargs, so workaround that:
        if(s_array_contains(fnctl_cmds_int, ARRAY_SIZE(fnctl_cmds_int), cmd)) {
            int third_argument = va_arg(arguments, int);
            returnvalue = s_fcntl_mockwrap_int(fd, cmd, third_argument);
        } else if(s_array_contains(fnctl_cmds_void, ARRAY_SIZE(fnctl_cmds_void), cmd)) {
            returnvalue = s_fcntl_mockwrap_void(fd, cmd);
        } else {
            fail_msg("Currently only mocking for fcntl supported for known commands that have third argument of type int or void");
        }
    }
    va_end(arguments);
    return returnvalue;
}

void expect_fcntl_int(int retval, int fd, int cmd, int third_argument) {
    expect_value(s_fcntl_mockwrap_int, fd, fd);
    expect_value(s_fcntl_mockwrap_int, cmd, cmd);
    expect_value(s_fcntl_mockwrap_int, third_argument, third_argument);
    will_return(s_fcntl_mockwrap_int, retval);
}

void expect_fcntl_void(int retval, int fd, int cmd) {
    expect_value(s_fcntl_mockwrap_void, fd, fd);
    expect_value(s_fcntl_mockwrap_void, cmd, cmd);
    will_return(s_fcntl_mockwrap_void, retval);
}

void mock_socket_set_enabled(bool enabled) {
    s_enabled = enabled;
}

/** mocked version of bind() */
int bind(int sockfd, const struct sockaddr* addr, socklen_t addrlen) {
    if(!s_enabled) {
        int (* real_bind)(int sockfd, const struct sockaddr* addr, socklen_t addrlen) =
            dlsym(RTLD_NEXT, "bind");
        return real_bind(sockfd, addr, addrlen);
    }
    check_expected(sockfd);
    assert_non_null(addr);
    assert_true(addrlen > 0);
    return (int) mock();
}

void expect_bind(int retval, int sockfd) {
    expect_value(bind, sockfd, sockfd);
    will_return(bind, retval);
}

/** mocked version of amxo_connection_add() */
int amxo_connection_add(amxo_parser_t* parser,
                        int fd,
                        amxo_fd_read_t reader,
                        const char* uri,
                        amxo_con_type_t type,
                        void* priv) {
    if(!s_enabled) {
        int (* real_add)(amxo_parser_t* parser,
                         int fd,
                         amxo_fd_read_t reader,
                         const char* uri,
                         amxo_con_type_t type,
                         void* priv) =
            dlsym(RTLD_NEXT, "amxo_connection_add");
        return real_add(parser, fd, reader, uri, type, priv);
    }

    amxo_connection_add_mock_data_t* mockdata = (amxo_connection_add_mock_data_t*) mock();
    assert_ptr_equal(parser, mockdata->expected_parser);
    assert_int_equal(fd, mockdata->expected_fd);
    mockdata->reader = reader;
    mockdata->priv = priv;
    if(uri == NULL) {
        assert_null(mockdata->expected_uri);
    } else {
        assert_string_equal(uri, mockdata->expected_uri);
    }
    assert_int_equal(type, mockdata->expected_type);
    return mockdata->retval;
}

void expect_amxo_connection_add(amxo_connection_add_mock_data_t* mockdata) {
    will_return(amxo_connection_add, mockdata);
}

/** mock/fake version of recv() */
ssize_t recv (int fd, void* buf, size_t n, int flags) {
    recv_mock_data_t* mockdata = (recv_mock_data_t*) mock();
    assert_int_equal(fd, mockdata->expected_fd);
    assert_int_equal(flags, mockdata->expected_flags);
    assert_non_null(buf);
    if(mockdata->retval != -1) {
        assert_non_null(mockdata->data);
        assert_true(mockdata->retval <= (ssize_t) mockdata->data_len);
        assert_true(mockdata->retval <= (ssize_t) n);
        memcpy(buf, mockdata->data, mockdata->retval);
    }
    return mockdata->retval;
}

void expect_recv(recv_mock_data_t* mockdata) {
    will_return(recv, mockdata);
}

/** mock version of sendto() */
ssize_t sendto(int sockfd, const void* buf, size_t len, int flags,
               const struct sockaddr* dest_addr, socklen_t addrlen) {

    if(!s_enabled) {
        int (* real_sendto)(int sockfd, const void* buf, size_t len, int flags,
                            const struct sockaddr* dest_addr, socklen_t addrlen) =
            dlsym(RTLD_NEXT, "sendto");
        return real_sendto(sockfd, buf, len, flags, dest_addr, addrlen);
    }

    check_expected(sockfd);
    check_expected(len); // check before buffer because buffer-check relies on len
    check_expected(buf);
    check_expected(flags);
    return (ssize_t) mock();
}

void expect_sendto(ssize_t retval, int sockfd, const void* buf, size_t len, int flags) {
    expect_value(sendto, sockfd, sockfd);
    expect_value(sendto, len, len);
    expect_memory(sendto, buf, buf, len);
    expect_value(sendto, flags, flags);
    will_return(sendto, retval);
}