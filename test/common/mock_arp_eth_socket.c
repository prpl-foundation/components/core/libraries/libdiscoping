/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_arp_eth_socket.h"
#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>
#include <amxc/amxc_macros.h>

bool __wrap_arp_eth_socket_new(arp_eth_socket_t** eth_socket,
                               amxo_parser_t* odl_parser, const char* netdev_name,
                               eth_frame_cb_t callback UNUSED, void* userdata) {

    wrap_arp_eth_socket_t* wrap_socket = (wrap_arp_eth_socket_t*) mock();

    check_expected(odl_parser);
    check_expected(netdev_name);
    assert_non_null(eth_socket);

    if(wrap_socket != NULL) {
        *eth_socket = (arp_eth_socket_t*) wrap_socket;
        wrap_socket->arp_socket = (arp_socket_t*) userdata; // note: we whitebox assume this.
        return true;
    } else {
        return false;
    }
}

void __wrap_arp_eth_socket_delete(arp_eth_socket_t** socket) {
    if((socket == NULL) || (*socket == NULL)) {
        return;
    }

    wrap_arp_eth_socket_t* wrap_socket = (wrap_arp_eth_socket_t*) *socket;
    assert_false(wrap_socket->freed);
    wrap_socket->freed = true;
}

bool __wrap_arp_eth_socket_send(arp_eth_socket_t* socket, const char* to_mac,
                                const void* message, size_t message_len) {

    check_expected(socket);
    check_expected(to_mac);
    check_expected(message_len);
    check_expected(message);
    assert_false(((wrap_arp_eth_socket_t*) socket)->freed);
    return true;
}