/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_socketlist.h"
#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>
#include <amxc/amxc_macros.h>

void __wrap_discoping_socketlist_send_request(discoping_socketlist_t* socketlist, const char* ip) {
    assert_non_null(socketlist);
    check_expected(ip);
    bool* tgt_has_sent = (bool*) mock();
    if(tgt_has_sent != NULL) {
        *tgt_has_sent = true;
    }
}

void mock_socketlist_expect_send_request(const char* ip, bool* tgt_has_sent) {
    expect_string(__wrap_discoping_socketlist_send_request, ip, ip);
    will_return(__wrap_discoping_socketlist_send_request, tgt_has_sent);
}

void __wrap_discoping_socketlist_mark_seen_network(const char* ip_char, const char* mac, void* socketlist) {
    discoping_socketlist_t* self = (discoping_socketlist_t*) socketlist;
    assert_non_null(self);
    check_expected(ip_char);
    check_expected(mac);
}

void expect_socketlist_mark_seen_network(const char* ip_char, const char* mac) {
    expect_string(__wrap_discoping_socketlist_mark_seen_network, ip_char, ip_char);
    expect_string(__wrap_discoping_socketlist_mark_seen_network, mac, mac);
}

void __wrap_discoping_socketlist_scan(discoping_socketlist_t* socketlist) {
    assert_non_null(socketlist);
    function_called();
}

void __wrap_discoping_socketlist_get_open_sockets(discoping_socketlist_t* socketlist) {
    assert_non_null(socketlist);
    function_called();
}

void expect_socketlist_get_open_sockets(void) {
    expect_function_call(__wrap_discoping_socketlist_get_open_sockets);
}