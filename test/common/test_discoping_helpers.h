/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TEST_DISCOPING_HELPERS_H__
#define __TEST_DISCOPING_HELPERS_H__

#include "../common/eventing.h"
#include "discoping.h"
#include "arp/arp_msg.h"
#include "../common/mock_arp_eth_socket.h"

extern const arp_eth_socket_frame_t test_discoping_helpers_gratuitous_arp_eth_frame;

static const uint8_t test_discoping_helpers_raw_gratuitous_arp[] =
    "\xff\xff\xff\xff\xff\xff"      // destination mac
    "\x01\x02\x03\x04\x05\x06"      // source mac
    "\x08\x06"                      // type = arp
    "\x00\x01"                      // hardware typ
    "\x08\x00"                      // protocol type
    "\x06"                          // hardware address length
    "\x04"                          // protocol address length
    "\x00\x01"                      // operation type
    "\x01\x02\x03\x04\x50\x60"      // sender hardware(mac) address
    "\x0a\x99\x88\x77"              // sender protocol(ip) address
    "\x00\x00\x00\x00\x00\x00"      // target hardware(mac) address
    "\x0a\x99\x88\x77";             // target protocol(ip) address


/** callback type */
typedef enum {
    TEST_ARPING_HELPERS_CB_TYPE_INVALID,
    TEST_ARPING_HELPERS_CB_TYPE_DOWN,
    TEST_ARPING_HELPERS_CB_TYPE_UP,
    TEST_ARPING_HELPERS_CB_TYPE_COLLISION
} test_discoping_helpers_cbtype_e;

typedef struct {
    char* ip;
    char* mac;

    char* mac_collision[3];
    test_discoping_helpers_cbtype_e up;

} test_discoping_helpers_cb_call_t;

typedef struct {
    int callcount;
    /** number of elements = callcount. */
    test_discoping_helpers_cb_call_t calls[16];

    eventing_t* eventing;
} test_discoping_helpers_cb_tracking_t;

typedef struct {
    test_discoping_helpers_cb_tracking_t cb_tracking;
    discoping_t* discoping;
    wrap_arp_eth_socket_t mock_eth_socket;
} test_discoping_helpers_teststate_t;

void test_discoping_helpers_teststate_init_without_socket_open(test_discoping_helpers_teststate_t* teststate);
void test_discoping_helpers_teststate_init(test_discoping_helpers_teststate_t* teststate);
void test_discoping_helpers_open_arp_socket(test_discoping_helpers_teststate_t* teststate);
void test_discoping_helpers_teststate_cleanup(test_discoping_helpers_teststate_t* teststate);

void test_discoping_helpers_assert_cb(test_discoping_helpers_teststate_t* teststate, int32_t callcount, const char* ip, const char* mac, test_discoping_helpers_cbtype_e updown);
void test_discoping_helpers_assert_cb_at(test_discoping_helpers_teststate_t* teststate, int32_t call_idx,
                                         int32_t total_callcount, const char* ip, const char* mac,
                                         test_discoping_helpers_cbtype_e updown);
void test_discoping_helpers_assert_collision_cb(test_discoping_helpers_teststate_t* teststate,
                                                int32_t callcount, const char* ip,
                                                const char* mac1, const char* mac2, const char* mac3);

void test_discoping_helpers_on_up_cb(const char* ip, uint32_t family, const char* mac, void* userdata);
void test_discoping_helpers_on_down_cb(const char* ip, uint32_t family, const char* mac, void* userdata);
void test_discoping_helpers_on_collision_cb(const char* ip, uint32_t family,
                                            const amxc_var_t* dev_mac_list, void* userdata);

void test_discoping_helpers_receive_arp_reply(test_discoping_helpers_teststate_t* teststate, const char* ip, const char* mac);

#endif