/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "eventing.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include "test-setup.h"
#include "test-util.h"

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>

#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>

#include <event2/event.h>

/**
 * @file amxrt contains infrastructure needed to do sockets, but we want to use them inside tests,
 * and amxrt is not available inside tests (because the main loop is the test runner, not amxrt).
 * This file provides this infrastructure in a way that it can be used inside tests.
 */

typedef struct _el_data {
    struct event* read;
    amxc_llist_it_t it;
} el_data_t;

struct eventing {
    /** Elements of type @ref el_data_t */
    amxc_llist_t el_datas;
    struct event_base* base;
    amxo_parser_t* parser;
};

static void el_connection_read_cb(UNUSED evutil_socket_t fd,
                                  UNUSED short flags,
                                  void* arg) {
    amxo_connection_t* con = (amxo_connection_t*) arg;
    con->reader(fd, con->priv);
}

static void el_slot_add_fd(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           void* const priv) {
    eventing_t* eventing = (eventing_t*) priv;
    amxo_connection_t* con = NULL;
    el_data_t* el_data = NULL;
    assert_non_null(eventing);

    assert_int_equal(amxc_var_type_of(data), AMXC_VAR_ID_FD);

    con = amxo_connection_get(eventing->parser, amxc_var_constcast(fd_t, data));
    // Note: if this fails, you might have a missing eventing_delete().
    assert_non_null(con);

    assert_null(el_data);
    el_data = (el_data_t*) calloc(1, sizeof(el_data_t));
    assert_non_null(el_data);
    el_data->read = event_new(eventing->base,
                              con->fd,
                              EV_READ | EV_PERSIST,
                              el_connection_read_cb,
                              con);
    con->el_data = el_data;
    event_add(el_data->read, NULL);

    amxc_llist_it_init(&el_data->it);
    amxc_llist_append(&eventing->el_datas, &el_data->it);
}

/** @implements amxc_llist_it_delete_t */
static void s_el_data_delete(amxc_llist_it_t* it) {
    assert_non_null(it);
    el_data_t* el_data = amxc_llist_it_get_data(it, el_data_t, it);
    event_del(el_data->read);
    event_free(el_data->read);
    free(el_data);
}

eventing_t* eventing_create(amxo_parser_t* parser) {
    assert_non_null(parser);
    eventing_t* eventing = calloc(1, sizeof(eventing_t));
    amxc_llist_init(&eventing->el_datas);
    eventing->base = event_base_new();
    eventing->parser = parser;
    assert_non_null(eventing->base);
    amxp_sigmngr_add_signal(NULL, "connection-added");
    assert_int_equal(0, amxp_slot_connect(NULL, "connection-added", NULL, el_slot_add_fd, eventing));
    test_util_handle_events();
    return eventing;
}

void eventing_send_eth_frame(eventing_t* eventing UNUSED, const uint8_t* const buffer, size_t buffer_len) {
    struct sockaddr_ll socket_address = {
        .sll_family = PF_PACKET,
        .sll_protocol = htons(ETH_P_ARP),
        .sll_ifindex = 1,
        .sll_hatype = ARPHRD_ETHER,
        .sll_halen = 0,
        // note: sll_addr is kept {0,0,0,0,0,0,0,0}.
    };
    int s = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ARP));
    assert_int_not_equal(s, -1);
    ssize_t sent = sendto(s, buffer, buffer_len, 0, (struct sockaddr*) &socket_address, sizeof(socket_address));
    assert_int_not_equal(sent, -1);
}

void eventing_loopbreak(eventing_t* eventing) {
    assert_non_null(eventing);
    event_base_loopbreak(eventing->base);
}

void eventing_dispatch(eventing_t* eventing) {
    assert_non_null(eventing);
    event_base_dispatch(eventing->base);
}

void eventing_once(eventing_t* eventing) {
    assert_non_null(eventing);
    event_base_loop(eventing->base, EVLOOP_ONCE);
}

void eventing_once_nonblock(eventing_t* eventing) {
    assert_non_null(eventing);
    event_base_loop(eventing->base, EVLOOP_NONBLOCK | EVLOOP_ONCE);
}

void eventing_delete(eventing_t** eventing) {
    if((eventing == NULL) || (*eventing == NULL)) {
        return;
    }

    assert_int_equal(0, amxp_slot_disconnect_signal_with_priv(NULL, "connection-added", el_slot_add_fd, *eventing));
    test_util_handle_events();
    amxc_llist_clean(&(*eventing)->el_datas, s_el_data_delete);
    event_base_free((*eventing)->base);
    free(*eventing);
    *eventing = NULL;
}