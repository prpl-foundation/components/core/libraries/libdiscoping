/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_arp_msg.h"
#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "arp/arp_msg.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>
#include <string.h>

const arp_eth_socket_frame_t normal_eth_frame = {
    .rdlen = 46,
    .vlanId = 0,
    .address = {
        .sll_family = 17,
        .sll_protocol = 1544,
        .sll_ifindex = 19,
        .sll_hatype = 1,
        .sll_pkttype = 0,
        .sll_halen = 6,
        .sll_addr = {88, 239, 104, 180, 38, 46, 0, 0}
    },
    .frame = "\x00\x01"            // hardware type
        "\x08\x00"                 // protocol type
        "\x06"                     // hardware address length
        "\x04"                     // protocol address length
        "\x00\x02"                 // operation type
        "\x58\xef\x68\xb4\x26\x2e" // sender hardware(mac) address
        "\xc0\xa8\x01\x65"         // sender protocol(ip) address
        "\x00\x11\x11\x01\x24\x01" // target hardware(mac) address
        "\xc0\xa8\x01\x01"         // target protocol(ip) address
};

static void s_assert_normal_arp_msg_content(const arp_msg_t* arp_msg) {
    assert_string_equal("192.168.1.101", arp_msg->ip_sender);
    assert_string_equal("192.168.1.1", arp_msg->ip_target);
    assert_string_equal("58:EF:68:B4:26:2E", arp_msg->mac_sender);
    assert_string_equal("00:11:11:01:24:01", arp_msg->mac_target);
    assert_int_equal(ARP_MSG_OPERATION_TYPE_REPLY, arp_msg->operation_type);
}

void test_arp_msg_filter_and_parse__normalcase(UNUSED void** state) {
    arp_msg_t arp_msg = {0};

    // GIVEN an ethernet frame containing a valid ARP packet (taken "from the wild")
    arp_eth_socket_frame_t eth_frame = normal_eth_frame;

    // WHEN parsing the ethernet frame
    bool ok = arp_msg_filter_and_parse(&arp_msg, &eth_frame);

    // THEN the correct IPs and MACs were extracted from the ARP packet inside the ethernet frame
    assert_true(ok);
    s_assert_normal_arp_msg_content(&arp_msg);
}

void test_arp_msg_filter_and_parse__too_short(UNUSED void** state) {
    arp_msg_t arp_msg = {0};

    // GIVEN an ethernet frame containing a packet that is too short for ARP
    arp_eth_socket_frame_t eth_frame = normal_eth_frame;
    eth_frame.rdlen = 27; // <-- an APR packet is 28 bytes

    // WHEN parsing the ethernet frame
    bool ok = arp_msg_filter_and_parse(&arp_msg, &eth_frame);

    // THEN it is rejected
    assert_false(ok);
}

void test_arp_msg_filter_and_parse__vlan(UNUSED void** state) {
    arp_msg_t arp_msg = {0};

    // GIVEN an ethernet frame that is on a vlan
    arp_eth_socket_frame_t eth_frame = normal_eth_frame;
    eth_frame.vlanId = 1;

    // WHEN parsing the ethernet frame
    bool ok = arp_msg_filter_and_parse(&arp_msg, &eth_frame);

    // THEN it is rejected
    assert_false(ok);
}

void test_arp_msg_filter_and_parse__xmas(UNUSED void** state) {
    arp_msg_t arp_msg = {0};

    // GIVEN an ethernet frame with all bits in payload set to one
    arp_eth_socket_frame_t eth_frame = normal_eth_frame;
    memset(&eth_frame.frame, 255, eth_frame.rdlen);

    // WHEN parsing the ethernet frame
    bool ok = arp_msg_filter_and_parse(&arp_msg, &eth_frame);

    // THEN it is rejected
    assert_false(ok);
}

void test_arp_msg_filter_and_parse__big_sizes(UNUSED void** state) {
    arp_msg_t arp_msg = {0};

    // GIVEN an ethernet frame with ARP message where the size fields are too high
    arp_eth_socket_frame_t eth_frame = normal_eth_frame;
    uint8_t frame_begin[6] =
        "\x00\x01"
        "\x08\x00"
        "\xff"         // <-- very high hardware address length
        "\xff";        // <-- very high protocol address length
    memcpy(eth_frame.frame, frame_begin, sizeof(frame_begin));

    // WHEN parsing that
    bool ok = arp_msg_filter_and_parse(&arp_msg, &eth_frame);

    // THEN the incorrect size fields are ignored
    assert_true(ok);
    s_assert_normal_arp_msg_content(&arp_msg);
}

void test_arp_msg_filter_and_parse__invalid_type(UNUSED void** state) {
    arp_msg_t arp_msg = {0};

    // GIVEN an ethernet frame with ARP message of invalid operation type
    arp_eth_socket_frame_t eth_frame = normal_eth_frame;
    assert_int_equal(2, eth_frame.frame[7]); // normal
    eth_frame.frame[7] = 3;                  // <-- 3 is an invalid operation type

    // WHEN parsing that
    bool ok = arp_msg_filter_and_parse(&arp_msg, &eth_frame);

    // THEN it is rejected
    assert_false(ok);
}

void test_arp_msg_filter_and_parse__request(UNUSED void** state) {
    arp_msg_t arp_msg = {0};

    // GIVEN an ARP request
    arp_eth_socket_frame_t eth_frame = {
        .rdlen = 46,
        .vlanId = 0,
        .address = {
            .sll_family = 17,
            .sll_protocol = 1544,
            .sll_ifindex = 19,
            .sll_hatype = 1,
            .sll_pkttype = 0,
            .sll_halen = 6,
            .sll_addr = {88, 239, 104, 180, 38, 46, 0, 0}
        },
        .frame = "\x00\x01"            // hardware type
            "\x08\x00"                 // protocol type
            "\x06"                     // hardware address length
            "\x04"                     // protocol address length
            "\x00\x01"                 // operation type
            "\x58\xef\x68\xb4\x26\x2e" // sender hardware(mac) address
            "\xc0\xa8\x01\x0a"         // sender protocol(ip) address
            "\x00\x00\x00\x00\x00\x00" // target hardware(mac) address
            "\xc0\xa8\x01\x01"         // target protocol(ip) address
    };

    // WHEN parsing the arp request
    bool ok = arp_msg_filter_and_parse(&arp_msg, &eth_frame);

    // THEN it is parsed as a request
    assert_true(ok);
    assert_string_equal("192.168.1.10", arp_msg.ip_sender);
    assert_string_equal("192.168.1.1", arp_msg.ip_target);
    assert_string_equal("58:EF:68:B4:26:2E", arp_msg.mac_sender);
    assert_string_equal("00:00:00:00:00:00", arp_msg.mac_target);
    assert_int_equal(ARP_MSG_OPERATION_TYPE_REQUEST, arp_msg.operation_type);
}

static void s_testhelper_arp_msg_serialize(const arp_msg_t* msg, uint8_t* expected_arp_msg_bin) {
    // GIVEN an ARP message

    // WHEN serializing it
    arp_msg_bin_t actual_arp_msg_bin = {0};
    bool ok = arp_msg_serialize(&actual_arp_msg_bin, msg);

    if(expected_arp_msg_bin != NULL) {
        // THEN we get a binary ARP message with correct contents
        assert_true(ok);
        assert_int_equal(0, memcmp(expected_arp_msg_bin, &actual_arp_msg_bin, 28));
    } else {
        // THEN we expect serialization to fail.
        assert_false(ok);
    }
}

void test_arp_msg_serialize__normal_case(UNUSED void** state) {
    s_testhelper_arp_msg_serialize(
        &(arp_msg_t) {
        .ip_sender = "1.2.0.254",
        .ip_target = "9.123.0.7",
        .mac_sender = "01:02:03:04:05:06",
        .mac_target = "07:08:09:0a:0B:C0",
        .operation_type = ARP_MSG_OPERATION_TYPE_REQUEST
    },
        (uint8_t[28]) {
        0x00, 0x01,                                                       // hardware type
        0x08, 0x00,                                                       // protocol type
        0x06,                                                             // hardware address length
        0x04,                                                             // protocol address length
        0x00, 0x01,                                                       // operation type
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06,                               // sender hardware(mac) address
        0x01, 0x02, 0x00, 254,                                            // sender protocol(ip) address
        0x07, 0x08, 0x09, 0x0a, 0x0b, 0xc0,                               // target hardware(mac) address
        9, 123, 0, 7                                                      // target protocol(ip) address
    });
}

void test_arp_msg_serialize__invalid_msg(UNUSED void** state) {
    // Case: Invalid IP sender
    s_testhelper_arp_msg_serialize(&(arp_msg_t) {
        .ip_sender = "",
        .ip_target = "9.123.0.7",
        .mac_sender = "01:02:03:04:05:06",
        .mac_target = "07:08:09:0a:0B:C0",
        .operation_type = ARP_MSG_OPERATION_TYPE_REQUEST,
    }, NULL);
    s_testhelper_arp_msg_serialize(&(arp_msg_t) {
        .ip_sender = "256.1.2.3",
        .ip_target = "9.123.0.7",
        .mac_sender = "01:02:03:04:05:06",
        .mac_target = "07:08:09:0a:0B:C0",
        .operation_type = ARP_MSG_OPERATION_TYPE_REQUEST,
    }, NULL);

    // Case: invalid IP target
    s_testhelper_arp_msg_serialize(&(arp_msg_t) {
        .ip_sender = "1.2.0.254",
        .ip_target = "hi",
        .mac_sender = "01:02:03:04:05:06",
        .mac_target = "07:08:09:0a:0B:C0",
        .operation_type = ARP_MSG_OPERATION_TYPE_REQUEST,
    }, NULL);

    // Case: invalid MAC sender
    s_testhelper_arp_msg_serialize(&(arp_msg_t) {
        .ip_sender = "1.2.0.254",
        .ip_target = "9.123.0.7",
        .mac_sender = "010203040506",
        .mac_target = "07:08:09:0a:0B:C0",
        .operation_type = ARP_MSG_OPERATION_TYPE_REQUEST,
    }, NULL);
    s_testhelper_arp_msg_serialize(&(arp_msg_t) {
        .ip_sender = "1.2.0.254",
        .ip_target = "9.123.0.7",
        .mac_sender = "",
        .mac_target = "07:08:09:0a:0B:C0",
        .operation_type = ARP_MSG_OPERATION_TYPE_REQUEST,
    }, NULL);

    // Case: invalid MAC target
    s_testhelper_arp_msg_serialize(&(arp_msg_t) {
        .ip_sender = "1.2.0.254",
        .ip_target = "9.123.0.7",
        .mac_sender = "01:02:03:04:05:06",
        .mac_target = ":::::",
        .operation_type = ARP_MSG_OPERATION_TYPE_REQUEST,
    }, NULL);

    // Case: invalid operation type:
    s_testhelper_arp_msg_serialize(&(arp_msg_t) {
        .ip_sender = "1.2.0.254",
        .ip_target = "9.123.0.7",
        .mac_sender = "01:02:03:04:05:06",
        .mac_target = "07:08:09:0a:0B:C0",
        .operation_type = ARP_MSG_OPERATION_TYPE_MAX,
    }, NULL);

    // Case: unsupported operation type:
    s_testhelper_arp_msg_serialize(&(arp_msg_t) {
        .ip_sender = "1.2.0.254",
        .ip_target = "9.123.0.7",
        .mac_sender = "01:02:03:04:05:06",
        .mac_target = "07:08:09:0a:0B:C0",
        .operation_type = ARP_MSG_OPERATION_TYPE_REPLY,
    }, NULL);
}