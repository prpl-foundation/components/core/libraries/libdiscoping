/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_socketlist.h"
#include "../common/test-setup.h"
#include "../common/test-util.h"
#include <amxut/amxut_timer.h>
#include "../common/mock_arp_eth_socket.h"
#include "discoping.h"
#include "discoping/discoping_socketlist.h"
#include "../common/eventing.h"
#include "../common/mock_ndp_socket.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>
#include <arpa/inet.h>
#include <string.h>

void test_discoping_socketlist_send_arp(UNUSED void** state) {
    wrap_arp_eth_socket_t wrap_eth_socket_1 = {};
    wrap_arp_eth_socket_t wrap_eth_socket_2 = {};

    // GIVEN a socketlist with two opened sockets
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());
    will_return(__wrap_arp_eth_socket_new, &wrap_eth_socket_1);
    expect_string(__wrap_arp_eth_socket_new, netdev_name, "fake_eth_1");
    expect_value(__wrap_arp_eth_socket_new, odl_parser, test_setup_parser());
    discoping_socketlist_open(socketlist, "fake_eth_1", "11:22:33:44:55:11", "10.0.1.1");
    will_return(__wrap_arp_eth_socket_new, &wrap_eth_socket_2);
    expect_string(__wrap_arp_eth_socket_new, netdev_name, "fake_eth_2");
    expect_value(__wrap_arp_eth_socket_new, odl_parser, test_setup_parser());
    discoping_socketlist_open(socketlist, "fake_eth_2", "11:22:33:44:55:22", "10.0.2.2");

    // EXPECT an arp request to be put on the network, on both sockets
    // socket 2:
    expect_value(__wrap_arp_eth_socket_send, socket, &wrap_eth_socket_2);
    expect_string(__wrap_arp_eth_socket_send, to_mac, "FF:FF:FF:FF:FF:FF");
    uint8_t message2[28] = {
        0x00, 0x01,                         // hardware type
        0x08, 0x00,                         // protocol type
        0x06,                               // hardware address length
        0x04,                               // protocol address length
        0x00, 0x01,                         // operation type
        0x11, 0x22, 0x33, 0x44, 0x55, 0x22, // sender hardware(mac) address
        0x0a, 0x00, 0x02, 0x02,             // sender protocol(ip) address
        0, 0, 0, 0, 0, 0,                   // target hardware(mac) address
        10, 0, 3, 3                         // target protocol(ip) address
    };
    expect_memory(__wrap_arp_eth_socket_send, message, message2, sizeof(message2));
    expect_value(__wrap_arp_eth_socket_send, message_len, 28);
    // socket 1:
    expect_value(__wrap_arp_eth_socket_send, socket, &wrap_eth_socket_1);
    expect_string(__wrap_arp_eth_socket_send, to_mac, "FF:FF:FF:FF:FF:FF");
    uint8_t message1[28] = {
        0x00, 0x01,                         // hardware type
        0x08, 0x00,                         // protocol type
        0x06,                               // hardware address length
        0x04,                               // protocol address length
        0x00, 0x01,                         // operation type
        0x11, 0x22, 0x33, 0x44, 0x55, 0x11, // sender hardware(mac) address
        10, 0, 1, 1,                        // sender protocol(ip) address
        0, 0, 0, 0, 0, 0,                   // target hardware(mac) address
        10, 0, 3, 3                         // target protocol(ip) address
    };
    expect_memory(__wrap_arp_eth_socket_send, message, message1, sizeof(message1));
    expect_value(__wrap_arp_eth_socket_send, message_len, 28);

    // WHEN sending an arp request
    assert_true(discoping_socketlist_send_request(socketlist, "10.0.3.3"));

    discoping_socketlist_delete(&socketlist);
}

void test_discoping_socketlist_send__not_opened(UNUSED void** state) {
    // GIVEN a socketlist without opened sockets
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());

    // EXPECT nothing to happen

    // WHEN attempting to send requests
    assert_false(discoping_socketlist_send_request(socketlist, "10.0.3.3"));
    assert_false(discoping_socketlist_send_request(socketlist, "fe80::abcd:ef01:2345:6789"));

    discoping_socketlist_delete(&socketlist);
}

void test_discoping_socketlist_scan__normal_case(UNUSED void** state) {
    wrap_ndp_socket_t wrap_socket = {};

    // GIVEN a socketlist with an opened socket
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());
    mock_ndp_socket_expect_new(&wrap_socket, test_setup_parser(), "fake_netdev_name", "fe80::abCD:ef01:2345:6789", discoping_socketlist_mark_seen_network);

    // EXPECT a scan to be performed on the socket interface
    expect_value(__wrap_ndp_socket_scan, socket, &wrap_socket);

    // WHEN opening a socket
    discoping_socketlist_open(socketlist, "fake_netdev_name", "11:22:33:44:aB:Cd", "fe80::abCD:ef01:2345:6789");

    // EXPECT a scan to be performed on the socket interface
    expect_value(__wrap_ndp_socket_scan, socket, &wrap_socket);

    // WHEN performing a scan
    discoping_socketlist_scan(socketlist);

    discoping_socketlist_delete(&socketlist);
}

void test_discoping_socketlist_scan__not_opened(UNUSED void** state) {
    // GIVEN a socketlist without an opened socket
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());

    // EXPECT no scan to be performed

    // WHEN asked to perform a scan on "all sockets" (i.e. none).
    discoping_socketlist_scan(socketlist);

    discoping_socketlist_delete(&socketlist);
}

void test_discoping_socketlist_scan__invalid_arguments(UNUSED void** state) {
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());

    discoping_socketlist_scan(NULL);

    discoping_socketlist_delete(&socketlist);
}

void test_discoping_socketlist_get_open_sockets__empty(UNUSED void** state) {
    // GIVEN a socketlist for which no sockets have been opened
    discoping_socketlist_t* socketlist = NULL;
    discoping_socketlist_new(&socketlist, test_setup_parser());

    // WHEN requesting the list of sockets that have been opened
    amxc_var_t* open_sockets = discoping_socketlist_get_open_sockets(socketlist);

    // THEN an empty list is returned
    assert_non_null(open_sockets);
    assert_int_equal(amxc_var_type_of(open_sockets), AMXC_VAR_ID_LIST);
    assert_true(amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, open_sockets)));

    discoping_socketlist_delete(&socketlist);
    amxc_var_delete(&open_sockets);
}

static void s_assert_socketinfolist_contains(amxc_var_t* socketinfolist, const char* netdev_name, const char* ip, const char* mac) {
    assert_non_null(socketinfolist);
    assert_int_equal(amxc_var_type_of(socketinfolist), AMXC_VAR_ID_LIST);
    amxc_var_for_each(socketinfo, socketinfolist) {
        const char* candidate_netdev_name = GET_CHAR(socketinfo, "netdev_name");
        const char* candidate_ip = GET_CHAR(socketinfo, "ip");
        const char* candidate_mac = GET_CHAR(socketinfo, "mac");
        assert_non_null(candidate_netdev_name);
        assert_non_null(candidate_ip);
        assert_non_null(candidate_mac);
        if((0 == strcmp(netdev_name, candidate_netdev_name)) && (0 == strcasecmp(candidate_ip, ip)) && (0 == strcasecmp(candidate_mac, mac))) {
            return;
        }
    }
    amxc_var_dump(socketinfolist, 0);
    fail_msg("Not found: %s %s %s", netdev_name, ip, mac);
}

static void s_add_three_sockets(discoping_socketlist_t* socketlist,
                                wrap_ndp_socket_t* wrap_socket1,
                                wrap_arp_eth_socket_t* wrap_socket2,
                                wrap_ndp_socket_t* wrap_socket3) {

    // socket1:
    mock_ndp_socket_expect_new(wrap_socket1, test_setup_parser(), "netdevname1", "fe80::abCD:ef01:2345:6789", discoping_socketlist_mark_seen_network);
    expect_value(__wrap_ndp_socket_scan, socket, wrap_socket1);
    discoping_socketlist_open(socketlist, "netdevname1", "11:22:33:44:aB:Cd", "fe80::abCD:ef01:2345:6789");
    // socket2:
    will_return(__wrap_arp_eth_socket_new, wrap_socket2);
    expect_string(__wrap_arp_eth_socket_new, netdev_name, "netdevname1");
    expect_value(__wrap_arp_eth_socket_new, odl_parser, test_setup_parser());
    discoping_socketlist_open(socketlist, "netdevname1", "11:22:33:44:55:11", "10.0.1.1");
    // socket3:
    mock_ndp_socket_expect_new(wrap_socket3, test_setup_parser(), "netdevname2", "fe80::1", discoping_socketlist_mark_seen_network);
    expect_value(__wrap_ndp_socket_scan, socket, wrap_socket3);
    discoping_socketlist_open(socketlist, "netdevname2", "11:22:33:00:aB:Cd", "fe80::1");
}

void test_discoping_socketlist_get_open_sockets__normal_case(UNUSED void** state) {
    // GIVEN a socketlist with three open sockets
    discoping_socketlist_t* socketlist = NULL;
    wrap_ndp_socket_t wrap_socket1 = {};
    wrap_arp_eth_socket_t wrap_socket2 = {};
    wrap_ndp_socket_t wrap_socket3 = {};
    discoping_socketlist_new(&socketlist, test_setup_parser());
    s_add_three_sockets(socketlist, &wrap_socket1, &wrap_socket2, &wrap_socket3);

    // WHEN requesting the list of sockets that have been opened
    amxc_var_t* open_sockets = discoping_socketlist_get_open_sockets(socketlist);

    // THEN the netdevname, mac, and ip, of the three sockets are returned
    s_assert_socketinfolist_contains(open_sockets, "netdevname1", "fe80::abcd:ef01:2345:6789", "11:22:33:44:ab:cd");
    s_assert_socketinfolist_contains(open_sockets, "netdevname1", "10.0.1.1", "11:22:33:44:55:11");
    s_assert_socketinfolist_contains(open_sockets, "netdevname2", "fe80::1", "11:22:33:00:ab:cd");
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, open_sockets)), 3);

    discoping_socketlist_delete(&socketlist);
    amxc_var_delete(&open_sockets);
}

void test_discoping_socketlist_get_open_sockets__after_close(UNUSED void** state) {
    // GIVEN a socketlist with a closed socket
    discoping_socketlist_t* socketlist = NULL;
    wrap_ndp_socket_t wrap_socket1 = {};
    wrap_arp_eth_socket_t wrap_socket2 = {};
    wrap_ndp_socket_t wrap_socket3 = {};
    discoping_socketlist_new(&socketlist, test_setup_parser());
    s_add_three_sockets(socketlist, &wrap_socket1, &wrap_socket2, &wrap_socket3);
    // closed socket:
    discoping_socketlist_close(socketlist, "netdevname1", "11:22:33:44:ab:CD", "fe80::ABcd:ef01:2345:6789");

    // WHEN requesting the list of sockets that are still open
    amxc_var_t* open_sockets = discoping_socketlist_get_open_sockets(socketlist);

    // THEN the netdevname, mac, and ip, of the two remaining sockets are returned
    s_assert_socketinfolist_contains(open_sockets, "netdevname1", "10.0.1.1", "11:22:33:44:55:11");
    s_assert_socketinfolist_contains(open_sockets, "netdevname2", "fe80::1", "11:22:33:00:ab:cd");
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, open_sockets)), 2);

    discoping_socketlist_delete(&socketlist);
    amxc_var_delete(&open_sockets);
}