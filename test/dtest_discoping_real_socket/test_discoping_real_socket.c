/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_discoping_real_socket.h"
#include "../common/test_discoping_helpers.h"
#include "../common/test-setup.h"
#include "../common/test-util.h"
#include <amxut/amxut_timer.h>
#include "../common/mock_socketlist.h"
#include "discoping.h"
#include "arp/arp_msg.h"
#include "arp/arp_socket_priv.h"
#include "discoping/discoping_priv.h"
#include "../common/eventing.h"
#include "discoping/discoping_verifier.h"
#include "discoping/discoping_watcher.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>
#include <arpa/inet.h>
#include <string.h>


void test_discoping_real_socket_open(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_request_sent = false;

    if(!test_util_has_permission_to_open_arp_socket()) {
        printf("No permissions to open ARP socket, so cannot run test. Skipping.\n");
        skip();
    }

    // GIVEN a discoping with an opened interface
    test_discoping_helpers_teststate_init_without_socket_open(&teststate);
    teststate.cb_tracking.eventing = eventing_create(test_setup_parser());
    assert_true(discoping_open(teststate.discoping, "lo", "00:00:00:00:00:00", "127.0.0.1"));

    // EXPECT an arp request will be sent (for collision checking);
    mock_socketlist_expect_send_request("10.153.136.119", &arp_request_sent);

    // WHEN the interface receives an ethernet frame that is a gratuitous ARP,
    //      and the collision timer timed out
    eventing_send_eth_frame(teststate.cb_tracking.eventing, test_discoping_helpers_raw_gratuitous_arp, sizeof(test_discoping_helpers_raw_gratuitous_arp) - 1);
    eventing_once(teststate.cb_tracking.eventing);
    amxut_timer_go_to_future_ms(1);

    // THEN the arp request was sent
    assert_true(arp_request_sent);

    // WHEN the collision timeout passes
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 1);

    // THEN the callback was called, informing discoping's user about the IP<->MAC mapping
    test_discoping_helpers_assert_cb(&teststate, 1, "10.153.136.119", "01:02:03:04:50:60", TEST_ARPING_HELPERS_CB_TYPE_UP);

    // THEN the ip<->mac is being watched
    assert_true(discoping_is_watching(teststate.discoping, "10.153.136.119", "01:02:03:04:50:60"));

    eventing_delete(&teststate.cb_tracking.eventing);
    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_real_socket_open__fail(UNUSED void** state) {
    bool ok = false;

    // GIVEN a discoping
    test_discoping_helpers_teststate_t teststate = {};
    test_discoping_helpers_teststate_init_without_socket_open(&teststate);

    // WHEN opening an interface that does not exist
    ok = discoping_open(teststate.discoping, "non_existing_interface", "00:00:00:00:00:00", "127.0.0.1");

    // THEN opening failed
    assert_false(ok);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_real_socket_close(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    if(!test_util_has_permission_to_open_arp_socket()) {
        printf("No permissions to open ARP socket, so cannot run test. Skipping.\n");
        skip();
    }

    // GIVEN a discoping with an opened and then closed interface
    test_discoping_helpers_teststate_init_without_socket_open(&teststate);
    teststate.cb_tracking.eventing = eventing_create(test_setup_parser());
    assert_true(discoping_open(teststate.discoping, "lo", "00:00:00:00:00:00", "127.0.0.1"));
    assert_true(discoping_close(teststate.discoping, "lo", "00:00:00:00:00:00", "127.0.0.1")); // <-- closed

    // WHEN the interface receives an ARP message
    eventing_send_eth_frame(teststate.cb_tracking.eventing, test_discoping_helpers_raw_gratuitous_arp,
                            sizeof(test_discoping_helpers_raw_gratuitous_arp) - 1);
    test_util_handle_events();
    for(int i = 0; i < 100; i++) {
        eventing_once_nonblock(teststate.cb_tracking.eventing);
    }

    // THEN the callback is not called.
    test_discoping_helpers_assert_cb(&teststate, 0, NULL, NULL, 0);

    eventing_delete(&teststate.cb_tracking.eventing);
    test_discoping_helpers_teststate_cleanup(&teststate);
}
