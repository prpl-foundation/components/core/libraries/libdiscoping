/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_discoping_collision.h"
#include "../common/test_discoping_helpers.h"
#include "../common/test-setup.h"
#include "../common/test-util.h"
#include <amxut/amxut_timer.h>
#include "../common/mock_socketlist.h"
#include "discoping.h"
#include "arp/arp_msg.h"
#include "discoping/discoping_priv.h"
#include "../common/eventing.h"
#include "discoping/discoping_verifier.h"
#include "discoping/discoping_watcher.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>
#include <arpa/inet.h>
#include <string.h>

void test_discoping_verify__is_known_colliding(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping with a known collision
    test_discoping_helpers_teststate_init(&teststate);
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:02");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 5);
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:44:55:01", "11:22:33:44:55:02", NULL);
    assert_true(arp_sent);

    // EXPECT ARP request to be sent
    arp_sent = false;
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);

    // WHEN verifying one of those known MACs:
    discoping_verify(teststate.discoping, "192.168.3.4", "11:22:33:44:55:02");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS - ARP_VERIFIER_COLLISION_TIMEOUT_MS);

    // THEN ARP request was sent
    assert_true(arp_sent);

    // WHEN the colliding MACs reply and the timeout passes
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:02");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS);

    // THEN no additional the collision callback was called because the user already knew
    // (callcount stays 1)
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:44:55:01", "11:22:33:44:55:02", NULL);

    // THEN the ip<->macs stay being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:44:55:01"));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:44:55:02"));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify__is_unknown_colliding(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // EXPECT ARP request to be sent
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);

    // WHEN verifying an IP
    discoping_verify(teststate.discoping, "192.168.3.4", "11:22:33:bb:AA:02");
    amxut_timer_go_to_future_ms(1);

    // THEN ARP request was sent
    assert_true(arp_sent);

    // WHEN are replies come in (and collision timeout passes)
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:BB:aa:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:BB:aa:02");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:BB:aa:03");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 5);

    // THEN collision callback was called with the colliding MACs
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:BB:AA:01", "11:22:33:BB:AA:02", "11:22:33:BB:AA:03");

    // THEN the ip<->macs are being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:bB:aA:01"));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.3.4", NULL));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:bB:aA:02"));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:bB:aA:03"));

    // THEN unrelated ip<->macs are not being watched
    assert_false(discoping_is_watching(teststate.discoping, "192.168.3.5", "11:22:33:bB:aA:03"));
    assert_false(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:bB:aA:04"));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_seen__is_known_colliding(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping with a known collision
    test_discoping_helpers_teststate_init(&teststate);
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:01");
    amxut_timer_go_to_future_ms(1);
    assert_true(arp_sent);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:02");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 5);
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:44:55:01", "11:22:33:44:55:02", NULL);

    // WHEN one such MAC is reported is seen by user
    discoping_mark_seen(teststate.discoping, "192.168.3.4", "11:22:33:44:55:02");

    // THEN nothing happens.
    assert_true(ARP_WATCHER_TIMEBLOCK_MS > ARP_VERIFIER_COLLISION_TIMEOUT_MS);
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - ARP_VERIFIER_COLLISION_TIMEOUT_MS - 5);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_seen__collision_known_by_user_not_by_discoping(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping with a mac<->ip mapping
    test_discoping_helpers_teststate_init(&teststate);
    mock_socketlist_expect_send_request("192.168.3.4", NULL);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 5);
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.3.4", "11:22:33:AA:BB:01", TEST_ARPING_HELPERS_CB_TYPE_UP);

    // WHEN the user says to know about another mac claiming the ip
    discoping_mark_seen(teststate.discoping, "192.168.3.4", "11:22:33:AA:bb:02");

    // THEN no arp request is sent for the remainder of the timeblock
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - ARP_VERIFIER_COLLISION_TIMEOUT_MS - 10);

    // EXPECT arp request to be sent
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);

    // WHEN the next timeblock starts
    amxut_timer_go_to_future_ms(10);

    // THEN arp request was sent
    assert_true(arp_sent);

    // WHEN both reply
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:aa:BB:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:aa:BB:02");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 5);

    // THEN no additional collision callback is called since user already knows
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.3.4", "11:22:33:AA:BB:01", TEST_ARPING_HELPERS_CB_TYPE_UP);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__new_collision_by_one_new_mac(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping with one known mac<->ip mapping.
    test_discoping_helpers_teststate_init(&teststate);
    discoping_mark_seen(teststate.discoping, "192.168.3.4", "11:22:33:44:55:01");

    // WHEN the discoping receives an ARP reply talking about another mac claiming the IP
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:02");

    // THEN an ARP request is sent and no callbacks were called
    mock_socketlist_expect_send_request("192.168.3.4", NULL);
    test_discoping_helpers_assert_cb(&teststate, 0, NULL, NULL, TEST_ARPING_HELPERS_CB_TYPE_UP);
    amxut_timer_go_to_future_ms(10);

    // WHEN the ARP replies come in
    amxut_timer_go_to_future_ms(10);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:01");
    amxut_timer_go_to_future_ms(10);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:02");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS - 30 - 5);
    test_discoping_helpers_assert_cb(&teststate, 0, NULL, NULL, 0);

    // THEN, after timeout, collision callback is called
    amxut_timer_go_to_future_ms(5 + 5);
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:44:55:01", "11:22:33:44:55:02", NULL);

    // THEN all ip<->macs are being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:44:55:01"));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:44:55:02"));

    // THEN unrelated ip<->macs are not being watched
    assert_false(discoping_is_watching(teststate.discoping, "192.168.3.101", "11:22:33:44:55:01"));
    assert_false(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:44:55:03"));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__new_collision_by_all_new_mac_without_timer_inbetween(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // EXPECT arp request will be send
    // (this is so discoping knows if there are more macs colliding than what's seen so far)
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);

    // WHEN a collision comes into existance by appearance of macs that have not been seen before,
    //      and the timer callbacks are not called inbetween macs popping up
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:03");
    amxut_timer_go_to_future_ms(1);

    // THEN arp request was sent
    assert_true(arp_sent);

    // WHEN collision timesout
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS);

    // THEN collision callback is called, reporting to discoping's user that the macs claim the same ip
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", "11:22:33:AA:BB:03");

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__new_collision_by_all_new_mac_with_timer_inbetween(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // EXPECT arp request will be send
    // (this is so discoping knows if there are more macs colliding than what's seen so far)
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);

    // WHEN a collision comes into existance by appearance of macs that have not been seen before,
    //      and the timer callbacks are called inbetween macs popping up
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    amxut_timer_go_to_future_ms(1);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");
    amxut_timer_go_to_future_ms(1);

    // THEN arp request was sent
    assert_true(arp_sent);

    // WHEN collision timesout
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS);

    // THEN collision callback is called, reporting to discoping's user that the macs claim the same ip
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", NULL);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__collision_grows(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping with a collision
    test_discoping_helpers_teststate_init(&teststate);
    mock_socketlist_expect_send_request("192.168.3.4", NULL);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", NULL);

    // EXPECT arp request will be sent (to see if more than one mac additionaly claims the ip)
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);

    // WHEN the collision grows, i.e. a new mac starts also claiming the IP
    //      *after* the retry timer timed out
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:04");
    amxut_timer_go_to_future_ms(1);

    // THEN arp request was sent
    assert_true(arp_sent);

    // WHEN all colliding macs reply
    amxut_timer_go_to_future_ms(10);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    amxut_timer_go_to_future_ms(10);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:04");
    amxut_timer_go_to_future_ms(10);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");

    // WHEN collision timesout
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS - 30);

    // THEN collision callback is called, reporting the grown set of macs claiming the IP
    test_discoping_helpers_assert_collision_cb(&teststate, 2, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", "11:22:33:AA:BB:04");

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__collision_grows_inbetween_collision_timer_and_retry_timer(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping with a collision
    test_discoping_helpers_teststate_init(&teststate);
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");
    assert_true(ARP_VERIFIER_COLLISION_TIMEOUT_MS < ARP_VERIFIER_RETRY_INTERVAL_MS - 10);
    amxut_timer_go_to_future_ms(1);
    assert_true(arp_sent);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS - 10);
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", NULL);

    // WHEN the collision grows, i.e. a new mac starts also claiming the IP
    //      *before* the retry timer times out
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:04");
    amxut_timer_go_to_future_ms(1);

    // THEN no arp request is sent yet

    // EXPECT arp request will be sent
    arp_sent = false;
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);

    // WHEN the retry interval stops
    amxut_timer_go_to_future_ms(9);

    // THEN arp request was sent
    assert_true(arp_sent);

    // WHEN all colliding macs reply
    amxut_timer_go_to_future_ms(10);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    amxut_timer_go_to_future_ms(10);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:04");
    amxut_timer_go_to_future_ms(10);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");

    // THEN no new collision callback with the 3 macs is called yet
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", NULL);

    // WHEN collision timesout
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS - 30);

    // THEN collision callback is called, reporting the grown set of macs claiming the IP
    test_discoping_helpers_assert_collision_cb(&teststate, 2, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", "11:22:33:AA:BB:04");

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__collision_shrinks(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping with a known collision
    test_discoping_helpers_teststate_init(&teststate);
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:02");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:03");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 5);
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:44:55:01", "11:22:33:44:55:02", "11:22:33:44:55:03");
    assert_true(arp_sent);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS - ARP_VERIFIER_COLLISION_TIMEOUT_MS);

    // EXPECT ARP request to be sent
    arp_sent = false;
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);

    // WHEN a whole timeblock no arp replies were seen
    // first finish previous timeblock (which had arp replies):
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - ARP_VERIFIER_RETRY_INTERVAL_MS);
    // timeblock without arp replies:
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS);

    // THEN ARP request was sent
    assert_true(arp_sent);

    // WHEN one less of the colliding MACs reply (and collision-check time ends)
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:44:55:03");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 1);

    // THEN the down callback was called with the MAC that went down
    test_discoping_helpers_assert_cb_at(&teststate, 1, 3, "192.168.3.4",
                                        "11:22:33:44:55:02", TEST_ARPING_HELPERS_CB_TYPE_DOWN);

    // THEN the collision callback was called with the two colliding MACs
    test_discoping_helpers_assert_collision_cb(&teststate, 3, "192.168.3.4",
                                               "11:22:33:44:55:01", "11:22:33:44:55:03", NULL);

    // THEN only the reachable ip<->macs stay being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:44:55:01"));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:44:55:03"));
    assert_false(discoping_is_watching(teststate.discoping, "192.168.3.4", "11:22:33:44:55:02"));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__collision_stops(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping with a collision
    test_discoping_helpers_teststate_init(&teststate);
    mock_socketlist_expect_send_request("192.168.3.4", NULL);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", NULL);

    // EXPECT arp request will be sent
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);

    // WHEN a timeblock passes in which only one of the two macs were seen
    // first finish the ongoing timeblock:
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - ARP_VERIFIER_RETRY_INTERVAL_MS);
    // next see one of the two macs:
    amxut_timer_go_to_future_ms(100);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    // and finaly finish the timeblock in which one of the two macs were seen:
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS);

    // THEN the arp request was sent
    assert_true(arp_sent);

    // WHEN only that one mac that is up replies (and the other does not)
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS);

    // THEN the down callback is called for the mac that went down,
    //      and the up callback is called for the remaining mac (that is not in collision anymore)
    test_discoping_helpers_assert_cb_at(&teststate, 1, 3, "192.168.3.4",
                                        "11:22:33:AA:BB:02", TEST_ARPING_HELPERS_CB_TYPE_DOWN);
    test_discoping_helpers_assert_cb_at(&teststate, 2, 3, "192.168.3.4",
                                        "11:22:33:AA:BB:01", TEST_ARPING_HELPERS_CB_TYPE_UP);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__collision_some_add_some_remove(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping with a collision
    test_discoping_helpers_teststate_init(&teststate);
    mock_socketlist_expect_send_request("192.168.3.4", NULL);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", NULL);

    // EXPECT arp request will be sent
    mock_socketlist_expect_send_request("192.168.3.4", &arp_sent);

    // WHEN a timeblock passes
    // first finish the ongoing first timeblock:
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - ARP_VERIFIER_RETRY_INTERVAL_MS);
    // finish the 2nd timeblock
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS + 1);

    // THEN the arp request was sent
    assert_true(arp_sent);

    // WHEN one mac stays reachable, one becomes unreachable, one new becomes reachable:
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:03");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:04");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS);

    // THEN the down callback is called for the mac that went down,
    //      and the collision callback for the ones that are now reachable
    test_discoping_helpers_assert_cb_at(&teststate, 1, 3, "192.168.3.4",
                                        "11:22:33:AA:BB:01", TEST_ARPING_HELPERS_CB_TYPE_DOWN);
    test_discoping_helpers_assert_collision_cb(&teststate, 3, "192.168.3.4",
                                               "11:22:33:AA:BB:02", "11:22:33:AA:BB:03", "11:22:33:AA:BB:04");

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__collision_stays(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping with a collision
    test_discoping_helpers_teststate_init(&teststate);
    mock_socketlist_expect_send_request("192.168.3.4", NULL);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", NULL);
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - ARP_VERIFIER_RETRY_INTERVAL_MS);

    // WHEN a new timeblock passes where they stay reachable
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:01");
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.3.4", "11:22:33:AA:bb:02");
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS + 1);

    // THEN no additional callback is called (i.e. latest callback is still the old one)
    test_discoping_helpers_assert_collision_cb(&teststate, 1, "192.168.3.4",
                                               "11:22:33:AA:BB:01", "11:22:33:AA:BB:02", NULL);

    test_discoping_helpers_teststate_cleanup(&teststate);
}