/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_discoping.h"
#include "../common/test_discoping_helpers.h"
#include "../common/test-setup.h"
#include "../common/test-util.h"
#include <amxut/amxut_timer.h>
#include "../common/mock_socketlist.h"
#include "discoping.h"
#include "arp/arp_msg.h"
#include "arp/arp_socket_priv.h"
#include "discoping/discoping_priv.h"
#include "../common/eventing.h"
#include "discoping/discoping_verifier.h"
#include "discoping/discoping_watcher.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc_macros.h>
#include <arpa/inet.h>
#include <string.h>


void test_discoping_on_arp_msg__gratuitous(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a gratuitous ARP message (spontanious announcement of IP<->MAC mapping), and a discoping
    test_discoping_helpers_teststate_init(&teststate);

    arp_msg_t gratuitous_arp = {
        .ip_sender = "192.168.1.1",
        .ip_target = "192.168.1.1",
        .mac_sender = "01:02:03:04:05:06",
        .mac_target = "00:00:00:00:00:00",
        .operation_type = ARP_MSG_OPERATION_TYPE_REQUEST
    };

    // EXPECT arp request sent
    mock_socketlist_expect_send_request("192.168.1.1", NULL);

    // WHEN the discoping receives the gratuitous ARP message
    arp_socket_internal_on_arp_msg(teststate.mock_eth_socket.arp_socket, &gratuitous_arp);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 5);

    // THEN the callback was called, informing discoping's user about the IP<->MAC mapping
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.1", "01:02:03:04:05:06", TEST_ARPING_HELPERS_CB_TYPE_UP);

    // THEN the ip<->mac is being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.1", "01:02:03:04:05:06"));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_on_eth_frame__gratuitous(UNUSED void** state) {
    // Note: if you want to do something like this automatic unittest but manually,
    //       you can do something like this: sudo discoping -c 4 -A -I lo 127.0.0.1
    //       (adapt to your IP and interface name)

    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN an ethernet frame containing a gratuitous ARP, and given a discoping
    test_discoping_helpers_teststate_init(&teststate);
    arp_eth_socket_frame_t frame = test_discoping_helpers_gratuitous_arp_eth_frame;

    // EXPECT arp request sent
    mock_socketlist_expect_send_request("192.168.1.101", NULL);

    // WHEN the discoping receives the ethernet frame
    arp_socket_internal_on_eth_frame(teststate.mock_eth_socket.arp_socket, &frame);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 5);

    // THEN the callback was called, informing discoping's user about the IP<->MAC mapping
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.101", "01:02:0A:0B:0C:0D", TEST_ARPING_HELPERS_CB_TYPE_UP);

    // THEN the ip<->mac is being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.101", "01:02:0A:0B:0C:0D"));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_on_arp_msg__reply(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // EXPECT arp request (as part of collision check)
    mock_socketlist_expect_send_request("192.168.1.100", NULL);

    // WHEN the discoping receives a normal ARP reply
    arp_socket_internal_on_arp_msg(teststate.mock_eth_socket.arp_socket, &(arp_msg_t) {
        .ip_sender = "192.168.1.100",
        .ip_target = "192.168.1.1",
        .mac_sender = "11:22:33:44:55:66",
        .mac_target = "77:88:99:aa:bb:cc",
        .operation_type = ARP_MSG_OPERATION_TYPE_REPLY
    });
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 1); // let collision check pass.

    // THEN the callback was called, informing discoping's user about the IP<->MAC mapping
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.100", "11:22:33:44:55:66", TEST_ARPING_HELPERS_CB_TYPE_UP);

    // EXPECT an ARP request to be sent for that IP
    mock_socketlist_expect_send_request("192.168.1.100", NULL);

    // WHEN when the second "timeblock" stops (in first "timeblock" it was already was confirmed)
    amxut_timer_go_to_future_ms(2 * ARP_WATCHER_TIMEBLOCK_MS);

    // THEN the ip<->mac is still being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.100", "11:22:33:44:55:66"));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_on_arp_msg__request(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // WHEN the discoping receives a normal ARP request
    arp_socket_internal_on_arp_msg(teststate.mock_eth_socket.arp_socket, &(arp_msg_t) {
        .ip_sender = "192.168.1.100",
        .ip_target = "192.168.1.1",
        .mac_sender = "11:22:33:44:55:66",
        .mac_target = "77:88:99:aa:bb:cc",
        .operation_type = ARP_MSG_OPERATION_TYPE_REQUEST
    });
    amxut_timer_go_to_future_ms(3 * ARP_WATCHER_TIMEBLOCK_MS);

    // THEN no callback is called (requests are ignored)
    assert_int_equal(teststate.cb_tracking.callcount, 0);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__auto_verify(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping that knows an IP (but was not confirmed by network)
    test_discoping_helpers_teststate_init(&teststate);
    discoping_mark_seen(teststate.discoping, "192.168.1.123", "01:02:03:04:05:06");

    // EXPECT ARP request request sent
    mock_socketlist_expect_send_request("192.168.1.123", NULL);

    // WHEN the first timeblock passes
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS + 1);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__auto_verify_never_reply(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping that knows an IP
    test_discoping_helpers_teststate_init(&teststate);
    discoping_mark_seen(teststate.discoping, "192.168.1.123", "01:02:03:04:05:06");

    // EXPECT first ARP attempt to be sent
    mock_socketlist_expect_send_request("192.168.1.123", NULL);

    // WHEN no renew / new arp reply in the 2nd "timeblock" of 90 seconds
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS + 1);

    // EXPECT first retry ARP attempt to be sent
    mock_socketlist_expect_send_request("192.168.1.123", NULL);

    // WHEN waiting retry interval
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // EXPECT second retry ARP attempt to be sent
    mock_socketlist_expect_send_request("192.168.1.123", NULL);

    // WHEN waiting retry interval
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // EXPECT third retry ARP attempt to be sent
    mock_socketlist_expect_send_request("192.168.1.123", NULL);

    // WHEN waiting retry interval
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // WHEN waiting retry interval
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // THEN the IP was reported to be down
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.123", "01:02:03:04:05:06", TEST_ARPING_HELPERS_CB_TYPE_DOWN);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__auto_verify_up(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping that knows an IP
    test_discoping_helpers_teststate_init(&teststate);
    discoping_mark_seen(teststate.discoping, "192.168.1.123", "01:02:03:04:05:06");

    // EXPECT first ARP attempt to be sent
    mock_socketlist_expect_send_request("192.168.1.123", &arp_sent);

    // WHEN waiting the whole timeblock
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS + 1);
    assert_true(arp_sent);

    // WHEN ARP replies comes in
    amxut_timer_go_to_future_ms(1 * 1000);
    arp_socket_internal_on_arp_msg(teststate.mock_eth_socket.arp_socket, &(arp_msg_t) {
        .ip_sender = "192.168.1.123",
        .ip_target = "192.168.1.1",
        .mac_sender = "01:02:03:04:05:06",
        .mac_target = "77:88:99:aa:bb:cc",
        .operation_type = ARP_MSG_OPERATION_TYPE_REPLY
    });

    // THEN no callback is called (we don't flood with "yes still up" callbacks)
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - ARP_WATCHER_TIMEBLOCK_MS / 32);
    assert_int_equal(0, teststate.cb_tracking.callcount);

    // EXPECT no ARP sent

    // WHEN waiting the rest of the timeblock
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS / 32 + 1);

    // EXPECT again ARP sent
    arp_sent = false;
    mock_socketlist_expect_send_request("192.168.1.123", &arp_sent);

    // WHEN waiting the next timeblock
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS);
    assert_true(arp_sent);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__seen_during_verify(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping that knows an IP
    test_discoping_helpers_teststate_init(&teststate);
    discoping_mark_seen(teststate.discoping, "192.168.1.123", "01:02:03:04:ab:cd");

    // EXPECT nothing to happen

    // WHEN the first block is not finished yet
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - 100);

    // EXPECT first ARP request to be sent
    mock_socketlist_expect_send_request("192.168.1.123", &arp_sent);

    // WHEN the first block finishes
    amxut_timer_go_to_future_ms(101);

    // THEN the first ARP request was sent
    assert_true(arp_sent);

    // WHEN the IP is seen on network arp reply
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.1.123", "01:02:03:04:Ab:cD");

    // THEN no further ARP requests are sent before this 2nd 90-second-block finishes
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - 200);

    // AND THEN no additional callback was called
    assert_int_equal(0, teststate.cb_tracking.callcount);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify__during_verify(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping that is verifying an IP, and has done some of its retries already
    test_discoping_helpers_teststate_init(&teststate);
    discoping_mark_seen(teststate.discoping, "192.168.1.123", "01:02:03:ab:cd:06");
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS + 1);
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // EXPECT only the last ARP to still be sent (no more)
    mock_socketlist_expect_send_request("192.168.1.123", NULL);

    // WHEN verifying again
    discoping_verify(teststate.discoping, "192.168.1.123", "01:02:03:Ab:cd:06");
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // THEN the IP was reported to be down, without sending furter ARPs first
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.123", "01:02:03:AB:CD:06", TEST_ARPING_HELPERS_CB_TYPE_DOWN);

    // THEN the ip<->mac is not being watched anymore
    assert_false(discoping_is_watching(teststate.discoping, "192.168.1.123", "01:02:03:Ab:cd:06"));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify__during_wait_last_retry(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping that is verifying an IP, and has sent the last request and waits for that
    test_discoping_helpers_teststate_init(&teststate);
    discoping_mark_seen(teststate.discoping, "192.168.1.123", "01:02:03:ab:cd:06");
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS + 1);
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // WHEN verifying again
    discoping_verify(teststate.discoping, "192.168.1.123", "01:02:03:Ab:cd:06");

    // THEN after the last timeout finishes, retries are sent again
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    assert_int_equal(0, teststate.cb_tracking.callcount);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    mock_socketlist_expect_send_request("192.168.1.123", &arp_sent);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    assert_true(arp_sent);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // THEN the IP was reported to be down, without sending furter ARPs first
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.123", "01:02:03:AB:CD:06", TEST_ARPING_HELPERS_CB_TYPE_DOWN);

    // THEN the ip<->mac is not being watched anymore
    assert_false(discoping_is_watching(teststate.discoping, "192.168.1.123", "01:02:03:Ab:cd:06"));
    assert_false(discoping_is_watching(teststate.discoping, "192.168.1.123", "01:02:03:ab:cd:06"));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify__null_mac_no_reply(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // EXPECT arp request sent
    mock_socketlist_expect_send_request("192.168.1.123", NULL);

    // WHEN verifying an IP without knowing its MAC
    discoping_verify(teststate.discoping, "192.168.1.123", NULL);

    // THEN the retry ARP requests are sent
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS + 1);
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // THEN the IP is being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", NULL));

    // WHEN no reply comes in
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // THEN the IP with unknown MAC is reported to be down
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.123", NULL, TEST_ARPING_HELPERS_CB_TYPE_DOWN);

    // THEN the IP is not being watched anymore
    assert_false(discoping_is_watching(teststate.discoping, "192.168.1.123", NULL));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify__null_mac_with_reply(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // EXPECT arp request sent
    mock_socketlist_expect_send_request("192.168.1.123", NULL);

    // WHEN verifying an IP without knowing its MAC
    discoping_verify(teststate.discoping, "192.168.1.123", NULL);

    // THEN the retry ARP requests is sent on timeout
    mock_socketlist_expect_send_request("192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS + 3);

    // WHEN a reply comes in
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.1.123", "01:cc:DD:44:55:66");

    // THEN the IP is being watched for this mac
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", NULL));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", "01:cC:Dd:44:55:66"));

    // THEN the IP with MAC is reported to be up
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.123", "01:CC:DD:44:55:66", TEST_ARPING_HELPERS_CB_TYPE_UP);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify__up(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool sent = false;

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // EXPECT arp request sent
    mock_socketlist_expect_send_request("192.168.1.123", &sent);

    // WHEN verifying an IP
    discoping_verify(teststate.discoping, "192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(100);

    // THEN the ARP request was sent
    assert_true(sent);

    // WHEN a reply comes in (after the collision timeout is already passed)
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 1);
    arp_socket_internal_on_arp_msg(teststate.mock_eth_socket.arp_socket, &(arp_msg_t) {
        .ip_sender = "192.168.1.123",
        .ip_target = "192.168.1.1",
        .mac_sender = "ab:cd:03:04:05:06",
        .mac_target = "77:88:99:aa:bb:01",
        .operation_type = ARP_MSG_OPERATION_TYPE_REPLY
    });

    // THEN the IP with unknown MAC is reported to be up
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.123", "AB:CD:03:04:05:06", TEST_ARPING_HELPERS_CB_TYPE_UP);

    // AND THEN no further arp is sent for the (rest of the) first timeblock
    assert_true(ARP_WATCHER_TIMEBLOCK_MS > ARP_VERIFIER_COLLISION_TIMEOUT_MS);
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - ARP_VERIFIER_COLLISION_TIMEOUT_MS);

    // WHEN most of remainder of second block passes
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - 200);

    // THEN nothing happens

    // EXPECT arp request will be send
    sent = false;
    mock_socketlist_expect_send_request("192.168.1.123", &sent);

    // WHEN the second block finishes
    amxut_timer_go_to_future_ms(200);

    // THEN arp request was sent
    assert_true(sent);

    // THEN the IP is being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", "AB:CD:03:04:05:06"));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", NULL));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify__multiple_immediately(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);
    test_util_handle_events();

    // EXPECT all ARP requests to be sent immediately,
    //        so not some after a timeout of 4 or 5 seconds
    mock_socketlist_expect_send_request("192.168.1.1", NULL);
    mock_socketlist_expect_send_request("192.168.1.2", NULL);
    mock_socketlist_expect_send_request("192.168.1.3", NULL);

    // WHEN verifying multiple IPs
    discoping_verify(teststate.discoping, "192.168.1.1", "01:02:03:ab:cd:01");
    discoping_verify(teststate.discoping, "192.168.1.2", NULL);
    discoping_verify(teststate.discoping, "192.168.1.3", "01:02:ab:CD:ef:03");
    test_util_handle_events();
    amxut_timer_go_to_future_ms(100);
    test_util_handle_events();

    // THEN the IPs and MACs are being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.1", NULL));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.2", NULL));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.3", NULL));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.1", "01:02:03:ab:cd:01"));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.1", "01:02:03:ab:CD:01"));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.3", "01:02:ab:cd:EF:03"));

    // THEN unrelated IPs are not being watched
    assert_false(discoping_is_watching(teststate.discoping, "192.168.1.4", NULL));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify__one_of_many_replies(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);
    test_util_handle_events();

    // EXPECT 3 ARP requests sent
    mock_socketlist_expect_send_request("192.168.1.1", NULL);
    mock_socketlist_expect_send_request("192.168.1.2", NULL);
    mock_socketlist_expect_send_request("192.168.1.3", NULL);

    // WHEN verifying multiple IPs
    discoping_verify(teststate.discoping, "192.168.1.1", "01:02:03:aB:cd:01");
    discoping_verify(teststate.discoping, "192.168.1.2", NULL);
    discoping_verify(teststate.discoping, "192.168.1.3", "01:02:ab:CD:ef:03");
    test_util_handle_events();
    amxut_timer_go_to_future_ms(100);
    test_util_handle_events();

    // EXPECT 2 ARP requests sent
    mock_socketlist_expect_send_request("192.168.1.2", NULL);
    mock_socketlist_expect_send_request("192.168.1.3", NULL);

    // WHEN one ARP reply confirms an IP is up
    arp_socket_internal_on_arp_msg(teststate.mock_eth_socket.arp_socket, &(arp_msg_t) {
        .ip_sender = "192.168.1.1",
        .ip_target = "192.168.1.254",
        .mac_sender = "01:02:03:Ab:cd:01",
        .mac_target = "77:88:99:aa:bb:54",
        .operation_type = ARP_MSG_OPERATION_TYPE_REPLY
    });
    amxut_timer_go_to_future_ms(4 * 1000);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify_once__invalid_arguments(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    test_discoping_helpers_teststate_init(&teststate);
    test_util_handle_events();

    // Case: NULL self
    discoping_verify_once(NULL, "192.168.1.1", "01:02:03:ab:cd:01");

    // Case: NULL IP
    discoping_verify_once(teststate.discoping, NULL, "01:02:03:ab:cd:01");

    // Check that no ARPs are sent
    amxut_timer_go_to_future_ms(10 * ARP_WATCHER_TIMEBLOCK_MS);
    test_util_handle_events();

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify_once__up(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool sent = false;

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // EXPECT arp request sent
    mock_socketlist_expect_send_request("192.168.1.123", &sent);

    // WHEN verifying an IP once
    discoping_verify_once(teststate.discoping, "192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(100);

    // THEN the IP is being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", NULL));

    // AND THEN the ARP request was sent
    assert_true(sent);
    sent = false;

    // WHEN a reply comes in (after the collision timeout is already passed)
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 1);
    arp_socket_internal_on_arp_msg(teststate.mock_eth_socket.arp_socket, &(arp_msg_t) {
        .ip_sender = "192.168.1.123",
        .ip_target = "192.168.1.1",
        .mac_sender = "ab:cd:03:04:05:06",
        .mac_target = "77:88:99:aa:bb:01",
        .operation_type = ARP_MSG_OPERATION_TYPE_REPLY
    });

    // THEN the IP with formerly unknown MAC is reported to be up
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.123", "AB:CD:03:04:05:06", TEST_ARPING_HELPERS_CB_TYPE_UP);

    // AND THEN no further arp is sent ever
    amxut_timer_go_to_future_ms(10 * ARP_WATCHER_TIMEBLOCK_MS);
    assert_false(sent);

    // AND THEN the IP is being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", "AB:CD:03:04:05:06"));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", NULL));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify_once__down(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool sent = false;

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // EXPECT first ARP attempt to be sent
    mock_socketlist_expect_send_request("192.168.1.123", &sent);

    // WHEN verifying an ip-mac without enabling periodic reverification
    discoping_verify_once(teststate.discoping, "192.168.1.123", "01:02:03:04:05:06");
    amxut_timer_go_to_future_ms(100);

    // THEN ARP attempt was sent
    assert_true(sent);
    sent = false;

    // EXPECT first retry ARP attempt to be sent
    mock_socketlist_expect_send_request("192.168.1.123", &sent);

    // WHEN waiting retry interval
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // THEN ARP attempt was sent
    assert_true(sent);
    sent = false;

    // EXPECT second retry ARP attempt to be sent
    mock_socketlist_expect_send_request("192.168.1.123", &sent);

    // WHEN waiting retry interval
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // THEN ARP attempt was sent
    assert_true(sent);
    sent = false;

    // EXPECT third retry ARP attempt to be sent
    mock_socketlist_expect_send_request("192.168.1.123", &sent);

    // WHEN waiting retry interval
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // THEN ARP attempt was sent
    assert_true(sent);
    sent = false;

    // WHEN waiting retry interval
    amxut_timer_go_to_future_ms(ARP_VERIFIER_RETRY_INTERVAL_MS);

    // THEN the IP was reported to be down
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.123", "01:02:03:04:05:06", TEST_ARPING_HELPERS_CB_TYPE_DOWN);

    // EXPECT no ARPs sent anymore
    // (nothing to do to check that, cmocka will complain on unexpected calls to mocked function)

    // WHEN waiting a long time
    amxut_timer_go_to_future_ms(10 * ARP_WATCHER_TIMEBLOCK_MS);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_verify_once__after_periodic_reverify_was_enabled(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool sent = false;

    // GIVEN a discoping that is verifying with periodic reverify enabled
    test_discoping_helpers_teststate_init(&teststate);
    discoping_verify(teststate.discoping, "192.168.1.123", NULL);

    // EXPECT arp request sent
    mock_socketlist_expect_send_request("192.168.1.123", &sent);

    // WHEN verifying an IP once
    discoping_verify_once(teststate.discoping, "192.168.1.123", NULL);
    amxut_timer_go_to_future_ms(100);

    // THEN the IP is being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", NULL));

    // AND THEN the ARP request was sent
    assert_true(sent);
    sent = false;

    // WHEN a reply comes in (after the collision timeout is already passed)
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS + 1);
    arp_socket_internal_on_arp_msg(teststate.mock_eth_socket.arp_socket, &(arp_msg_t) {
        .ip_sender = "192.168.1.123",
        .ip_target = "192.168.1.1",
        .mac_sender = "ab:cd:03:04:05:06",
        .mac_target = "77:88:99:aa:bb:01",
        .operation_type = ARP_MSG_OPERATION_TYPE_REPLY
    });

    // THEN the IP with formerly unknown MAC is reported to be up
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.123", "AB:CD:03:04:05:06", TEST_ARPING_HELPERS_CB_TYPE_UP);

    // AND THEN no further arp is sent ever
    amxut_timer_go_to_future_ms(10 * ARP_WATCHER_TIMEBLOCK_MS);
    assert_false(sent);

    // AND THEN the IP is being watched
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", "AB:CD:03:04:05:06"));
    assert_true(discoping_is_watching(teststate.discoping, "192.168.1.123", NULL));

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__no_callback_ip_stay_reachable(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping with a reachable mac
    test_discoping_helpers_teststate_init(&teststate);
    test_util_handle_events();
    mock_socketlist_expect_send_request("192.168.1.4", &arp_sent);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.1.4", "01:02:03:04:05:04");
    amxut_timer_go_to_future_ms(1);
    assert_true(arp_sent);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS);
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.4", "01:02:03:04:05:04", TEST_ARPING_HELPERS_CB_TYPE_UP);
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - ARP_VERIFIER_COLLISION_TIMEOUT_MS + 1); // timeblock1 ends

    // WHEN the mac stays reachable (arp reply observed)
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.1.4", "01:02:03:04:05:04");
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS); // timeblock2 ends

    // THEN no additional up callback is called
    //      (i.e. the old callback call is still the last one)
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.4", "01:02:03:04:05:04", TEST_ARPING_HELPERS_CB_TYPE_UP);

    // WHEN the mac is reachable (no arp reply observed before verifying)
    arp_sent = false;
    mock_socketlist_expect_send_request("192.168.1.4", &arp_sent);
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS + 1); // timeblock3 ends
    assert_true(arp_sent);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.1.4", "01:02:03:04:05:04");

    // THEN no additional up callback is called during the whole next timeblock
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS); // timeblock4 ends
    //      (i.e. the old callback call is still the last one)
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.4", "01:02:03:04:05:04", TEST_ARPING_HELPERS_CB_TYPE_UP);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__no_callback_ip_stay_unreachable(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};
    bool arp_sent = false;

    // GIVEN a discoping with a mac that was reported unreachable:
    // mac first reachable:
    test_discoping_helpers_teststate_init(&teststate);
    test_util_handle_events();
    mock_socketlist_expect_send_request("192.168.1.4", &arp_sent);
    test_discoping_helpers_receive_arp_reply(&teststate, "192.168.1.4", "01:02:03:04:05:04");
    amxut_timer_go_to_future_ms(1);
    assert_true(arp_sent);
    amxut_timer_go_to_future_ms(ARP_VERIFIER_COLLISION_TIMEOUT_MS);
    test_discoping_helpers_assert_cb(&teststate, 1, "192.168.1.4", "01:02:03:04:05:04", TEST_ARPING_HELPERS_CB_TYPE_UP);
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - ARP_VERIFIER_COLLISION_TIMEOUT_MS + 1); // timeblock1 ends
    // mac unreachable:
    arp_sent = false;
    mock_socketlist_expect_send_request("192.168.1.4", &arp_sent);
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS); // timeblock2 ends
    assert_true(arp_sent);
    arp_sent = false;
    mock_socketlist_expect_send_request("192.168.1.4", NULL);
    mock_socketlist_expect_send_request("192.168.1.4", NULL);
    mock_socketlist_expect_send_request("192.168.1.4", &arp_sent);
    amxut_timer_go_to_future_ms(4 * ARP_VERIFIER_RETRY_INTERVAL_MS); // still in timeblock3
    assert_true(arp_sent);
    test_discoping_helpers_assert_cb(&teststate, 2, "192.168.1.4", "01:02:03:04:05:04", TEST_ARPING_HELPERS_CB_TYPE_DOWN);

    // WHEN time passes
    amxut_timer_go_to_future_ms(10 * ARP_WATCHER_TIMEBLOCK_MS);

    // THEN no additional down callback is called (i.e. callcount stays 2)
    test_discoping_helpers_assert_cb(&teststate, 2, "192.168.1.4", "01:02:03:04:05:04", TEST_ARPING_HELPERS_CB_TYPE_DOWN);

    test_discoping_helpers_teststate_cleanup(&teststate);
}

static void s_assert_periodically_verified(test_discoping_helpers_teststate_t* teststate) {
    bool arp_sent = false;
    assert_true(discoping_is_watching(teststate->discoping, "10.1.2.3", NULL));
    // first timeblock:
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - 10); // time becomes: 1block - 10ms
    mock_socketlist_expect_send_request("10.1.2.3", &arp_sent);
    amxut_timer_go_to_future_ms(15);                            // time becomes: 1block + 5ms
    assert_true(arp_sent);
    arp_sent = false;
    test_discoping_helpers_receive_arp_reply(teststate, "10.1.2.3", "01:02:03:04:05:04");
    // second timeblock: (no verify at end of this timeblock because it was seen during this timeblock)
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS - 10 - 5); // time becomes: 2blocks - 10ms
    // third timeblock (needs verify because not seen in second timeblock)
    amxut_timer_go_to_future_ms(ARP_WATCHER_TIMEBLOCK_MS);          // time becomes: 3blocks - 10ms
    mock_socketlist_expect_send_request("10.1.2.3", &arp_sent);
    amxut_timer_go_to_future_ms(15);                                // time becomes: 3block + 5ms
    assert_true(arp_sent);
    arp_sent = false;

    // while we're at it, assert that the up callback was called once
    test_discoping_helpers_assert_cb(teststate, 1, "10.1.2.3", "01:02:03:04:05:04", TEST_ARPING_HELPERS_CB_TYPE_UP);
}

void test_discoping_set_periodic_reverify__enable_unknown_ip(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping
    test_discoping_helpers_teststate_init(&teststate);

    // WHEN enabling periodic reverification on an IP that was previously unknown
    discoping_set_periodic_reverify_enabled(teststate.discoping, "10.1.2.3", true);

    // THEN the IP is periodically reverified
    s_assert_periodically_verified(&teststate);

    // cleanup:
    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping__scan(UNUSED void** state) {
    test_discoping_helpers_teststate_t teststate = {};

    // GIVEN a discoping with an opened socket
    test_discoping_helpers_teststate_init_without_socket_open(&teststate);
    test_discoping_helpers_open_arp_socket(&teststate);

    test_util_handle_events();

    // EXPECT scan to be performed
    expect_function_call(__wrap_discoping_socketlist_scan);

    // WHEN waiting
    amxut_timer_go_to_future_ms(ARP_WATCHER_SCAN_INITIAL_WAIT_MS);

    // EXPECT scan to be performed again
    expect_function_call(__wrap_discoping_socketlist_scan);

    // WHEN waiting for the interval to perform such scans
    amxut_timer_go_to_future_ms(ARP_WATCHER_SCAN_INTERVAL_MS);

    // cleanup:
    test_discoping_helpers_teststate_cleanup(&teststate);
}

void test_discoping_get_open_sockets(UNUSED void** state) {
    // GIVEN a discoping facade
    test_discoping_helpers_teststate_t teststate = {};
    test_discoping_helpers_teststate_init_without_socket_open(&teststate);

    // EXPECT the list of open sockets will be queried by the facade
    expect_socketlist_get_open_sockets();

    // WHEN asking the facade to querying the list of open sockets
    discoping_get_open_sockets(teststate.discoping);

    // cleanup:
    test_discoping_helpers_teststate_cleanup(&teststate);
}