/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "../common/test-setup.h"
#include "test_discoping.h"
#include "test_discoping_collision.h"

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup_teardown(test_discoping_on_arp_msg__gratuitous, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_on_eth_frame__gratuitous, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_on_arp_msg__reply, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_on_arp_msg__request, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__auto_verify, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__auto_verify_never_reply, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__auto_verify_up, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__seen_during_verify, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify__during_verify, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify__during_wait_last_retry, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify__null_mac_no_reply, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify__null_mac_with_reply, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify__up, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify__multiple_immediately, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify__one_of_many_replies, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__no_callback_ip_stay_reachable, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__no_callback_ip_stay_unreachable, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify__is_known_colliding, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify__is_unknown_colliding, test_setup, test_teardown),

        cmocka_unit_test_setup_teardown(test_discoping_verify_once__invalid_arguments, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify_once__up, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify_once__down, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_verify_once__after_periodic_reverify_was_enabled, test_setup, test_teardown),

        cmocka_unit_test_setup_teardown(test_discoping_seen__is_known_colliding, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping_seen__collision_known_by_user_not_by_discoping, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__new_collision_by_one_new_mac, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__new_collision_by_all_new_mac_without_timer_inbetween, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__new_collision_by_all_new_mac_with_timer_inbetween, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__collision_grows, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__collision_grows_inbetween_collision_timer_and_retry_timer, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__collision_shrinks, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__collision_stops, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__collision_some_add_some_remove, test_setup, test_teardown),
        cmocka_unit_test_setup_teardown(test_discoping__collision_stays, test_setup, test_teardown),

        cmocka_unit_test_setup_teardown(test_discoping_set_periodic_reverify__enable_unknown_ip, test_setup, test_teardown),

        cmocka_unit_test_setup_teardown(test_discoping__scan, test_setup, test_teardown),

        cmocka_unit_test_setup_teardown(test_discoping_get_open_sockets, test_setup, test_teardown),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
