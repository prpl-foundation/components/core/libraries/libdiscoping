/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DISCOPING_H__)
#define __DISCOPING_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @file This file is responsible for tracking the IP<->MAC mappings on the network.
 *
 * It will call callbacks on the user of this file when a new IP<->MAC mapping comes into existence
 * (see @ref discoping_set_on_up_cb), stops existing (see @ref discoping_set_on_down_cb)
 * or is in conflict with another IP<->MAC mapping (multiple different MACs for same IP, see
 * @ref discoping_set_on_conflict_cb).
 */

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxo/amxo.h>

typedef struct discoping discoping_t;

/**
 * Callback called when a new IP<->MAC mapping comes into existence in the network without collision.
 *
 * Can also be called when a collision is resolved, i.e. moving from multiple MACs claiming an
 * IP to only one MAC claiming the IP, in which case this callback is called for that one MAC.
 *
 * If an IP stays unreachable through this MAC, there is no guarantee that this callback is not
 * called again, but it is rare / reduced to make it easier for the user of discoping to have good
 * performance.
 *
 * In case of collision (i.e. multiple MACs claim the same IP), @ref discoping_collision_cb_t
 * is called instead.
 *
 * In this callback, discoping must not be manipulated,
 * i.e. no calls to @ref discoping_verify etc.
 *
 * @param mac: MAC address, never NULL.
 */
typedef void (* discoping_up_cb_t) (const char* ip, uint32_t family, const char* mac, void* userdata);

/**
 * Callback called when a IP<->MAC mapping disappears from the network.
 *
 * If an IP stays unreachable through this MAC, there is no guarantee that this callback is not
 * called again, but it is rare / reduced to make it easier for the user of discoping to have good
 * performance.
 *
 * You can not assume this callback is always after an on_up or on_collision callback for
 * the IP and MAC.
 *
 * In this callback, discoping must not be manipulated,
 * i.e. no calls to @ref discoping_verify etc.
 *
 * @param mac: MAC address. This can be NULL if we do not know a MAC associated with the IP.
 *   This can happen if e.g. @ref discoping_verify was called with NULL as MAC.
 */
typedef void (* discoping_down_cb_t) (const char* ip, uint32_t family, const char* mac, void* userdata);

/**
 * Callback called when an IP is detected to be associated with multiple MAC addresses.
 *
 * When a collision is detected (i.e. two or more MACs claim the same IP), or a collision "grows"
 * (multiple MACs were claiming the same IP and now an additional MAC claims the IP), then
 * this callback is called.
 *
 * If an existing collision stays the same, there is no guarantee that this callback is not
 * called again, but it is rare / reduced to make it easier for the user of discoping to have good
 * performance.
 *
 * When a collision is resolved (i.e. there is no collision anymore), this implies one or more MACs
 * stop claiming the IP. As a consequence, the down callback (see @ref discoping_down_cb_t)
 * will be called for one or more MAC addresses. If one MAC keeps claiming the IP (while no
 * other MAC does so), then the up callback (see @ref discoping_up_cb_t) will be called
 * for that one MAC.
 *
 * In this callback, discoping must not be manipulated,
 * i.e. no calls to @ref discoping_verify etc.
 *
 * @param dev_mac_list: List of MAC addresses that claim the ip.
 *   Always contains at least two elements.
 *   Always of type list with elements of type cstring_t.
 *   When this callback is called multiple times for the same IP, then this list contains all MAC
 *   addresses that are associated with this IP, regardless of whether the MAC address was already
 *   in this list in a previous call of the callback.
 */
typedef void (* discoping_collision_cb_t) (const char* ip, uint32_t family,
                                           const amxc_var_t* dev_mac_list, void* userdata);

bool discoping_new(discoping_t** discoping, amxo_parser_t* parser);
void discoping_delete(discoping_t** discoping);

/**
 * Use given interface to send and receive ARP/NDP messages
 *
 * Start listening for ARP/NDP replies and gratuitous ARP on the interface with given netdev_name,
 * and use this interface to send ARP/NDP requests when verifying if an IP is alive.
 * This does not replace other interfaces, a new interface is opened additionally.
 *
 * If the interface is considered appropriate to detect IP addresses by performing a scan, then
 * if opening the socket is successful, this scan will be performed immediately, and periodically
 * until the socket is closed.
 *
 * @param netdev_name Name of the interface to listen on and send from
 * @param mac MAC address to send ARP/NDP requests from, basically the mac of the interface
 * @param ip IP to send ARP/NDP requests from, basically the IP on the interface
 * @return true on success, false on failure.
 */
bool discoping_open(discoping_t* discoping, const char* netdev_name, const char* mac, const char* ip);

bool discoping_close(discoping_t* discoping, const char* netdev_name, const char* mac, const char* ip);

/**
 * Gets list of sockets opened with @ref discoping_open
 *
 * Returns sockets that were successfully opened with @ref discoping_open (that are still open,
 * i.e. not closed again with @ref discoping_close).
 *
 * The returned value is of type @ref AMXC_VAR_ID_LIST with items of type @ref AMXC_VAR_ID_HTABLE
 * with keys "netdev_name", "mac", and "ip" and values of type @ref AMXC_VAR_ID_CSTRING
 */
amxc_var_t* discoping_get_open_sockets(discoping_t*);

/**
 * Set callback called when a new IP comes into existence in the network.
 *
 * See @ref discoping_up_cb_t
 */
void discoping_set_on_up_cb(discoping_t* discoping, discoping_up_cb_t up_cb);

/**
 * Set callback called when an IP disappears from the network.
 *
 * See @ref discoping_down_cb_t
 */
void discoping_set_on_down_cb(discoping_t* discoping, discoping_down_cb_t down_cb);

/**
 * Set callback called when a collision occurs (multiple MACs for same IP).
 *
 * see @ref discoping_collision_cb_t
 */
void discoping_set_on_collision_cb(discoping_t* discoping,
                                   discoping_collision_cb_t collision_cb);

/**
 * Set userdata to be passed to on_up and on_down and on_collision callbacks.
 *
 * See @ref discoping_up_cb_t, @ref discoping_down_cb_t,
 * and @ref discoping_collision_cb_t.
 */
void discoping_set_userdata(discoping_t* discoping, void* userdata);

/**
 * The IP is observed by the user to be reachable (regardless of whether it was reachable before).
 *
 * This does not trigger an immediate check if this MAC is indeed reachable for this IP, see
 * @ref discoping_verify if that is what is desired.
 * There is no guarantee that the on_up or on_reachable callback is called to inform the
 * user that the ip is reachable through this mac (since the point of this function is that
 * the user tells discoping that he already knows the ip is reachable through this mac).
 *
 * Contrary to @ref discoping_verify, this can be called multiple times without causing
 * many more ARP/NDP messages to be send.
 *
 * When it becomes known to be unreachable, the on_down callback will be called.
 *
 * See also: @ref discoping_verify
 *
 * @param mac: not NULL.
 */
void discoping_mark_seen(discoping_t* discoping, const char* ip, const char* mac);

/**
 * The IP is observed on the network to be reachable.
 *
 * This has the same effect as if discoping itself observes on the network that the IP is reachable.
 * - If it is a new IP, collision detection will be performed and the up or collision callback
 *   will be called
 * - If it is a known IP, i.e. @ref discoping_is_watching is true, and the MAC is the
 *   known MAC and periodic reverification is enabled (see
 *   @ref discoping_set_periodic_reverify_enabled), then a reverification will not
 *   be needed for a while since the IP was seen for this MAC.
 * - If it is a known IP and the MAC is not a known MAC for the IP, then collision check will
 *   be performed.
 */
void discoping_mark_seen_network(discoping_t* discoping, const char* ip, const char* mac);

/**
 * Check now if the IP is reachable or colliding
 *
 * The check is (almost) immediately started (so not just scheduled for after a minute or so).
 * This sends ARP/NDP requests on all the sockets that were opened with @ref discoping_open().
 *
 * If as a result of this verification the situation changes (for example, an IP is reachable
 * through a MAC, but the on_up or on_collision was not called before for this MAC), then
 * on_up, on_down, or on_collision callback is called.
 *
 * If the mac parameter is NULL, and the ip is not reachable, then the on_down callback
 * will be called, even if it was called before.
 *
 * The IP will periodically be checked to be reachable/colliding (until it becomes unreachable).
 * When it becomes unreachable, the on_down callback will be called.
 * Whether other IPs will periodically be verified is unaffected.
 *
 * @ref discoping_is_watching() will return true for given ip and mac.
 *
 * @param mac: can be NULL.
 */
void discoping_verify(discoping_t* discoping, const char* ip, const char* mac);

/**
 * Same as discoping_verify followed by discoping_set_periodic_enabled(false).
 */
void discoping_verify_once(discoping_t* discoping, const char* ip, const char* mac);

/**
 * Enable/disable repeatedly checking if the IP is still reachable
 *
 * If true, then for an IP that is being watched, an ARP/NDP/... request will be sent
 * every number of seconds to check if it is still reachable. When this check reveals the IP
 * is not reachable anymore, the down callback will be called (and the IP stops being watched
 * as usual).
 *
 * When disabling for an IP that is not being watched (see @ref discoping_is_watching),
 * then this has no effect.
 * When enabling this for an IP that is not being watched, the IP starts being watched.
 */
void discoping_set_periodic_reverify_enabled(discoping_t* discoping, const char* ip, bool enabled);

/**
 * Whether the given IP & MAC is kept an eye on.
 *
 * If true, then a callback will be called when the IP<->MAC mapping changes status, i.e.
 * becomes unreachable, colliding, or reachable and was not in that status before.
 *
 * Note that if this returns true, it does not mean the IP is being reverified periodically.
 * Whether the IP is being reverified periodically can be chosen by chosing between calling
 * @ref discoping_verify or @ref discoping_verify_once, and can be changed
 * by calling @ref discoping_set_periodic_reverify_enabled.
 */
bool discoping_is_watching(discoping_t* discoping, const char* ip, const char* mac);

#ifdef __cplusplus
}
#endif

#endif
