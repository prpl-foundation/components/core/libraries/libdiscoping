# Discoping

[[_TOC_]]

## Responsibility

Discoping **disco**vers when IPs appear and disappear in the network through a **ping** mechanism.

Discoping is implemented as a library.
Components that use discoping will get a callback call when a new IP becomes reachable in
the network, when a previously reachable IP becomes unreachable, and when different
devices claim the same IP or stop doing so.

For IPv6, discovering new IPs that are not seen is not done by discoping,
but it does periodically send ICMPv6 echo requests to facilitate other components to discover new
IPv6 address.

See the public API documentation for details.


## Timing

<!-- svg file can be editted as diagram with the drawio application -->
![Diagram timing](doc/timing.drawio.svg)

In the diagram above, we see the most basic timing of discoping:

- During a Timeblock, we observe incoming ARP replies.
- At the start of each Timeblock, we Verify the IPs for which we have not seen an ARP reply in
  the previous Timeblock
- The verification consists of (1) send an ARP request is for the IPs we want to verify and
  (2) a collision check
- If within collision-check time multiple MACs claim an IP that is being verified, report collision.
- If after the attempt-time no MAC replied for an IP, start a new attempt. If it was the last,
  report the IP went down.

This is a bit a simplified view. In practice verification can start at any time:
- when discoping's user requests to do a verification
- when an ARP reply from an unseen MAC is received, after which a collision check is started (by verifying the IP)
- can also happen while a verification is already running

For IPv6, the same timing mechanism is used. A Neighbor Solicitation packet is sent instead
of an ARP request, and a Neighbor Advertisement is received as reply.


## High level design

<!-- svg file can be editted as diagram with the drawio application -->
![Diagram of high level design](doc/high_level_design.drawio.svg)

Discoping has a layered design. From bottom to top in the diagram above:

- "arp" provides sending/receiving IPv4's Address Resolution Protocol messages.
- "ndp" provides sending/receiving messages for IPv6's Neighbor Discovery Protocol.
- "implementation" implements the core logic of discoping: dealing with verifying during
  verification, detect if we must start collision check, etc.
- To make discoping easy to use, a "facade" on top provides a simple API.
  This "facade" is used by gmap-mod-ethernet-dev, which creates and deletes IP addresses of devices in
  gMap, and sets the IP status ("reachable", "unreachable", "error").

## Detailed design

<!-- svg file can be editted as diagram with the drawio application -->
![Class Diagram](doc/class_diagram.drawio.svg)



